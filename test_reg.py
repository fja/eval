import lib.parser
from lib.dfa import DFA

def is_can(dfa: str) -> bool:
    return parse(dfa).is_canonical()
    
def parse(dfa: str) -> DFA:
    return lib.parser.dfa(dfa)

def to_str(dfa: DFA) -> str:
    return lib.parser.dfa_to_str(dfa)

def test_can() -> None:
    ex1 = "init=1 (1,a)=1 (1,b)=2 (2,b)=1 (2,a)=1 final={2}"
    assert is_can(ex1)
    assert is_can("init=1 (1,b)=3 (1,a)=2 (2,b)=1 (2,a)=1 (3,a)=2 (3,b)=2 final={2}")
    assert not is_can("init=1 (1,b)=3 (1,c)=2 (2,b)=1 (2,a)=1 (3,a)=2 (3,b)=2 final={2}")
    assert not is_can("init=1 (1,a)=1 (1,b)=2 (2,b)=1 (2,b)=1 (unreach,a)=1 (unreach,b)=unreach final={2}")

    assert DFA.is_identical(parse(ex1).canonize(), parse(ex1))
    auto_can = parse("init=x (x,a)=z (x,b)=y (y,a)=z (y,b)=z (z,a)=z (z,b)=z final={z}").canonize()
    expect_can = parse("init=1 (1,a)=2 (1,b)=3 (2,a)=2 (2,b)=2 (3,a)=2 (3,b)=2 final={2}")
    assert DFA.is_identical(auto_can, expect_can), f"{to_str(auto_can)} ≠ {to_str(expect_can)}"


def test_invalid() -> None:
    try:
        lib.parser.dfa("init=A (A,ab)=A (A,c)=B (B,ab)=B final={B}")
        assert False, "should not parse (long/compound terminal)"
    except:
        pass
