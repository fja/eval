# Evaluation Tools for Formal Languages and Grammars

Primarily for use at [Faculty of Informatics, Masaryk University](www.fi.muni.cz).
This work includes:

- Library of representations and algorithms for different classes of languages.
  - *Regular Languages* (using regular grammars, and both deterministic and
    nondeterministic finite automata, and regular expressions).
  - *Context-Free Languages* (using contex-free grammars).

- Compact text representation of these formalisms and parsers for them.

- Driver for automatic evaluation of IS questionnaires with for construction of
  these formalisms.

- A web tool for comparison of regular languages.

## Technical Details

The project is written mainly in Python 3 (≥ 3.7).

- Dependencies: Python (≥ 3.7), `python-antlr4`
- Optional dependencies:
  - testing: make, `pytest`, `mypy`
  - web: python packages `flask`, `flask_wtf`, `wtforms`, and `Flask-Markdown`
