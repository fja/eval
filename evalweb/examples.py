from typing import Dict, Tuple
from lib import parser

examples: Dict[Tuple[str, str], Tuple[str, str]] = {}
convert_examples: Dict[str, str] = {}

teacher_dfa = "(0,a)=1 (1,b)=0 final={0}"
teacher_efa = r"(A,a)={B} (A,\e)={D} (B,b)={A} final={A,D}"
student_dfa = "(A,a)=B (B,b)=C (C,a)=B final={A,C}"
student_efa = r"(A,a)={B} (A,\e)={D} (B,b)={C} (C,a)={B} final={C,D}"
student_gra = "S -> aS' | \\e\nS' -> bA | b\nA -> aS'"
student_reg = "(ab)^*"

teacher_forms = [('DFA', teacher_dfa),
                 ('EFA', teacher_efa),
                 ('GRA', student_gra),
                 ('REG', student_reg)]

student_forms = [('DFA', student_dfa),
                 ('TOT', parser.dfa_to_str(parser.dfa(student_dfa)
                                           .total())),
                 ('MIN', parser.dfa_to_str(parser.dfa(student_dfa)
                                           .minimize())),
                 ('CAN', parser.dfa_to_str(parser.dfa(student_dfa)
                                           .total().canonize())),
                 ('MIC', parser.dfa_to_str(parser.dfa(student_dfa)
                                           .minimize().canonize())),
                 ('EFA', student_efa),
                 ('NFA', parser.nfa_to_str(parser.nfa(student_efa)
                                           .eliminate_epsilon())),
                 ('GRA', student_gra),
                 ('REG', student_reg)]

for tt, te in teacher_forms:
    for st, se in student_forms:
        examples[(tt, st)] = (te, se)

convert_examples = dict(student_forms)
