from flask import Blueprint, render_template, request
from flask_wtf import FlaskForm  # type: ignore
from wtforms import RadioField   # type: ignore
from lib.parser import ParsingError
from evalweb.web_checker import WebChecker, Language, CompositeException
from evalweb.examples import examples, convert_examples
from typing import Optional


bp = Blueprint('eval', __name__, url_prefix='/reg')

# radiobuttons: student task
tasks = {'DFA': 'DFA',
         'TOT': 'Totální DFA',
         'MIN': 'Minimální DFA',
         'CAN': 'Kanonický automat',
         'MIC': 'Mininální kanonický automat',
         'EFA': 'NFA (s ε-kroky)',
         'NFA': 'NFA bez ε-kroků',
         'GRA': 'Regulární gramatika',
         'REG': 'Regulární výraz'}

# radiobuttons: conversion possibilities
convs = {'DFA': 'DFA',
         'TOT': 'Totální DFA',
         'MIN': 'Minimální DFA',
         'CAN': 'Kanonický automat',
         'MIC': 'Mininální kanonický automat',
         'EFA': 'NFA (s ε-kroky)',
         'NFA': 'NFA bez ε-kroků',
         'GRA': 'Regulární gramatika'}

# radiobuttons: teacher input/formalism to convert
types = {'DFA': 'DFA',
         'EFA': 'NFA (s ε-kroky)',
         'GRA': 'Regulární gramatika',
         'REG': 'Regulární výraz'}


# forms from variants above
class TypeForm(FlaskForm):
    make = RadioField('Typ', choices=list(types.items()), default='DFA',
                      coerce=str)


class TaskForm(FlaskForm):
    make = RadioField('Task', choices=list(tasks.items()), default='DFA',
                      coerce=str)


class ConvertForm(FlaskForm):
    make = RadioField('Convert', choices=list(convs.items()), default='DFA',
                      coerce=str)


@bp.route('/compare', methods=('GET', 'POST'))
def compare() -> str:
    student_form = TaskForm(prefix='student_form')
    teacher_form = TypeForm(prefix='teacher_form')
    student_area = ""  # areas remembers user input or replace it with examples
    teacher_area = ""

    print(request.form)
    if (request.method == 'POST' and 'submit_button' in request.form) \
            or (request.method == 'GET' and 'link' in request.args):
        if request.method == 'POST':
            teacher_type = teacher_form.make.data
            teacher_string = request.form['teacher_string']
            student_type = student_form.make.data
            student_string = request.form['student_string']
        else:
            teacher_type = request.args["tt"]
            teacher_string = request.args["ta"]
            student_type = request.args["st"]
            student_string = request.args["sa"]
        student_area = student_string
        teacher_area = teacher_string

        if student_string == "" or teacher_string == "":
            def ifempty(arg: str) -> Optional[str]:
                if arg == "":
                    return "Nebyl zadán vstupní formalismus."
                return None

            return render_template('compare.html',
                                   error_asgn=ifempty(teacher_string),
                                   error_sol=ifempty(student_string),
                                   student_form=student_form,
                                   teacher_form=teacher_form,
                                   student_area=student_area,
                                   teacher_area=teacher_area)

        checker = WebChecker(student_string=student_string, task=student_type)
        try:
            result = checker.compare(teacher_string=teacher_string,
                                     teacher_type=teacher_type)
        except CompositeException as ex:
            return render_template('compare.html',
                                   error_asgn=ex.exceptions[0],
                                   error_sol=ex.exceptions[1],
                                   student_form=student_form,
                                   teacher_form=teacher_form,
                                   student_area=student_area,
                                   teacher_area=teacher_area)

        extra_word_ce, missing_word_ce, inf = None, None, None
        if not result and checker.eq is not None:  # languages not equivalent
            extra_word_ce = checker.eq.left_counterexample
            missing_word_ce = checker.eq.right_counterexample
            inf = checker.eq.inf

        assert checker.teacher is not None
        assert checker.student is not None
        return render_template('result_compare.html', compare=True, ok=result,
                               inf=inf, task_solved=checker.task_solved,
                               alphabets=checker.alphabets,
                               extra_word_ce=extra_word_ce,
                               missing_word_ce=missing_word_ce,
                               is_task_valid=checker.is_task_valid,
                               is_task=checker.is_task,
                               img_src=checker.img_src,
                               langs=checker.languages,
                               teacher=checker.teacher,
                               student=checker.student,
                               teacher_type_string=types[checker.teacher.task],
                               student_type_string=tasks[checker.student.task],
                               teacher_type=teacher_type,
                               student_type=student_type,
                               teacher_string=teacher_string,
                               student_string=student_string)

    if request.method == 'POST' and 'example_button' in request.form:
        # of which types examples should be
        teacher_type = teacher_form.make.data
        student_type = student_form.make.data
        if (teacher_type, student_type) in examples:
            teacher_area, student_area = examples[(teacher_type, student_type)]

    return render_template('compare.html',
                           student_form=student_form,
                           teacher_form=teacher_form,
                           student_area=student_area,
                           teacher_area=teacher_area)


# analogical to compare, only with just one input formalism
@bp.route('/convert', methods=('GET', 'POST'))
def convert() -> str:
    student_form = TypeForm(prefix='student_form')
    task_form = ConvertForm(prefix='task_form')
    convert_args = {"student_form": student_form,
                    "task_form": task_form,
                    "student_area": ""}

    if (request.method == 'POST' and 'submit_button' in request.form) \
            or (request.method == 'GET' and 'link' in request.args):
        if request.method == 'POST':
            student_string = request.form['student_string']
            student_type = student_form.make.data
            task = task_form.make.data
        else:
            student_string = request.args['inval']
            student_type = request.args['inty']
            task = request.args['outty']
        convert_args["student_area"] = student_string

        if student_string == "":
            return render_template('convert.html',
                                   error="Nebyl zadán vstupní formalismus.",
                                   **convert_args)

        checker = WebChecker(student_string=student_string, task=task)
        try:
            output = checker.convert(student_type=student_type)
        except ParsingError as ex:
            return render_template('convert.html', error=ex, **convert_args)

        student = Language(string=student_string, task=student_type)
        is_task_valid, is_task = student.gen_is_task(task=task)

        return render_template('result_convert.html', compare=False,
                               student_string=student_string,
                               student_type=types[student_type],
                               task=tasks[task],
                               raw_in_type=student_type,
                               raw_out_type=task,
                               output=output,
                               is_task_valid=is_task_valid,
                               is_task=is_task)

    if request.method == 'POST' and 'example_button' in request.form:
        student_type = student_form.make.data
        if student_type in convert_examples:
            convert_args["student_area"] = convert_examples[student_type]

    return render_template('convert.html', **convert_args)


@bp.route('/userref')
def userref() -> str:
    return render_template('userref.html')


@bp.route('/about')
def about() -> str:
    return render_template('about.html')
