from lib import reg, parser
from lib.parser import ParsingError
from lib.checker import transform, dfa_transform, nfa_transform, check_task, \
                        check_alphabets
from typing import List, Optional, Tuple
from enum import Enum
import enum


class Size(Enum):
    Empty = enum.auto()
    Finite = enum.auto()
    Infinite = enum.auto()
    Universal = enum.auto()


class Language:  # contains information about formalism and its language
    def __init__(self, string: str, task: str = "DFA") -> None:
        self.string = string
        self.task = task

        dfa = dfa_transform(self.string, self.task)
        self.minimal = parser.dfa_to_str(dfa.minimize().canonize())

        empty = dfa.is_empty()
        if empty:
            size = Size.Empty
        else:
            self.example = empty.counterexample
            if not empty.inf:
                size = Size.Finite
            else:
                size = Size.Infinite
                if dfa.is_universal():
                    size = Size.Universal

        name = {"Empty": "prázdný",
                "Finite": "konečný",
                "Infinite": "nekonečný",
                "Universal": "univerzální"}
        self.size = size
        self.sizename = name[size.name]
        self.dfa = dfa

    def gen_is_task(self, task: str) -> Tuple[bool, str]:
        teacher = "NFA" if self.task == "EFA" else self.task
        assert self.dfa is not None
        try:
            dfa = parser.dfa_to_str(self.dfa.canonize())
            return True, f"{teacher}-{task}:{dfa}"
        except Exception as ex:
            return False, str(ex.args[0])


class CompositeException(Exception):
    def __init__(self, *args: Optional[Exception]) -> None:
        self.exceptions = args
        super().__init__()


class WebChecker:
    def __init__(self, student_string: str, task: str) -> None:
        self.student_string = student_string
        self.task = task
        self.task_solved = ""
        self.alphabets = ""
        self.eq: Optional[reg.IsEquivalentResult] = None
        self.teacher: Optional[Language] = None
        self.student: Optional[Language] = None
        self.is_task_valid = False
        self.is_task: str = "No output!"
        self.img_src: Optional[str] = None
        self.languages: List[Optional[Language]] = [None, None, None, None]

    # nearly equivalent to dfa_task in fja_checker (for IS ROPOTs)
    def dfa_task(self, teacher_type: str, teacher_string: str,
                 task: str, student_string: str) -> bool:
        error_teach = None
        try:
            teacher = Language(string=teacher_string, task=teacher_type)
        except ParsingError as ex:
            error_teach = ex
        try:
            student = Language(string=student_string, task=self.task)
        except ParsingError as ex:
            raise CompositeException(error_teach, ex)
        if error_teach:
            raise CompositeException(error_teach, None)

        # only for task checking
        student_solution = transform(student_string, task)

        self.teacher = teacher
        self.student = student
        self.is_task_valid, self.is_task = teacher.gen_is_task(task=self.task)

        if isinstance(student_solution, reg.DFA) \
                or isinstance(student_solution, reg.NFA):
            self.task_solved = check_task(student_solution, task)

        if teacher.size == Size.Empty and student.size == Size.Empty:
            return self.task_solved == ""

        # if the alphabets aren't same, languages aren't equal (unless they are
        # both empty)
        alphabets = check_alphabets(student_alpha=student.dfa.characters,
                                    teacher_alpha=teacher.dfa.characters,
                                    task=task)
        if alphabets != "":
            self.alphabets = alphabets

            return False

        self.eq = reg.DFA.is_equivalent(student.dfa, teacher.dfa)
        assert self.eq is not None

        self.img_src = self.relation(self.eq.left_counterexample is None
                                     and self.eq.right_counterexample is None)
        if self.task_solved == "" and \
                self.eq.right_counterexample is None and \
                self.eq.left_counterexample is None:
            return True
        return False

    def compare(self, teacher_string: str, teacher_type: str) -> bool:
        return self.dfa_task(teacher_type=teacher_type,
                             teacher_string=teacher_string,
                             task=self.task,
                             student_string=self.student_string)

    def convert(self, student_type: str) -> str:
        if self.task in {"DFA", "TOT", "MIN", "CAN", "MIC"}:
            dfa = dfa_transform(self.student_string, student_type)
            # avoid confusion by making the automaton total if the input
            # formalism was not already DFA
            if student_type != "DFA":
                dfa = dfa.total()

            if self.task == "DFA":
                return parser.dfa_to_str(dfa)
            if self.task == "TOT":
                return parser.dfa_to_str(dfa.total())
            if self.task == "CAN":
                return parser.dfa_to_str(dfa.canonize())
            if self.task == "MIN":
                return parser.dfa_to_str(dfa.minimize())
            if self.task == "MIC":
                return parser.dfa_to_str(dfa.minimize().canonize())

        nfa = nfa_transform(self.student_string, student_type)
        if self.task == "EFA":
            return parser.nfa_to_str(nfa)

        nfa = nfa.eliminate_epsilon()
        if self.task == "NFA":
            return parser.nfa_to_str(nfa)

        if self.task == "GRA":
            return parser.reggrammar_to_str(nfa.nfa_to_reggrammar()
                                               .eliminate_useless())

    def relation(self, eq: bool) -> str:
        assert self.teacher is not None
        assert self.student is not None
        student = self.student.dfa
        teacher = self.teacher.dfa
        assert student is not None
        assert teacher is not None

        # language 0 on picture: complement of both
        self.languages[0] = self.language(
                              (reg.DFA.union(teacher, student)).complement())

        if not eq:
            assert self.eq is not None
            if self.eq.left_counterexample is not None:  # extra word
                if self.eq.right_counterexample is not None:  # missing word
                    intersection = reg.DFA.intersection(student, teacher)
                    if intersection.is_empty():  # disjunction
                        self.languages[1] = self.teacher
                        self.languages[2] = self.student
                        return "disjunction"
                    else:
                        self.languages[1] = self.language(reg.DFA.subtraction(
                                                            teacher, student))
                        self.languages[2] = self.language(reg.DFA.subtraction(
                                                            student, teacher))
                        self.languages[3] = self.language(reg.DFA.intersection(
                                                            teacher, student))
                        return "intersection"

                else:  # teacher is subset of student
                    self.languages[1] = self.teacher
                    self.languages[2] = self.language(reg.DFA.subtraction(
                                                        student, teacher))
                    picture = "t_in_s"
            else:  # student is subset of teacher
                self.languages[1] = self.language(reg.DFA.subtraction(
                                                    teacher, student))
                self.languages[2] = self.student
                picture = "s_in_t"

        else:
            # print(self.eq.left_counterexample, self.eq.right_counterexample)
            picture = "eq"
            self.languages[0] = self.language(teacher.complement())
            self.languages[1] = self.teacher

        # if self.teacher.size == Size.Empty \
        #         or self.student.size == Size.Universal:
        #     picture += "_empty"
        if self.teacher.size == Size.Universal \
                or self.student.size == Size.Universal:
            picture += "_universal"
            if self.teacher.size == Size.Universal:
                self.languages.pop(0)
            if self.student.size == Size.Universal:
                self.languages[0] = self.languages[2]
                self.languages[2] = None

        return picture  # name of static image file to show in feedback

    def language(self, dfa: reg.DFA) -> Language:
        return Language(parser.dfa_to_str(dfa))

    def DFA_lines(self, str_: str) -> None:
        pass
