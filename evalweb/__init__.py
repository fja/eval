import os
from flask import Flask, redirect, url_for
from . import evalweb
from flaskext.markdown import Markdown


# taken from tutorial on flask.palletsprojects.com/en/1.1.x/tutorial

def create_app(test_config=None) -> Flask:
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(SECRET_KEY='dev')
    Markdown(app, extensions=['fenced_code'])

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    @app.route('/')
    # initial page is compare mode
    def index():
        return redirect(url_for('eval.compare'))

    app.register_blueprint(evalweb.bp)
    app.add_url_rule('/compare', endpoint='index')

    return app
