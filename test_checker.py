import subprocess
import sys
import os
import os.path
import logging
from typing import Dict, Set

FilePath = str


def run_checker(task_file: FilePath, solution_file: FilePath, html: bool) \
        -> subprocess.CompletedProcess:
    """
    Executes the checker with task given in files, returns information about
    the finished process
    """
    with open(task_file) as task_handle:
        task = task_handle.read()

    args = [sys.executable,  # the Python itself
            "fja_checker/__main__.py"]
    if html:
        args.append("--html")
    return subprocess.run(args + [solution_file, f"-o{task}"],
                          capture_output=True)


def run_tests(test_dir: FilePath, html: bool) -> None:
    """
    Run all tests in given directory.
    Task are given by files of form <NAME.q>.
    Solutions are given by files of form <NAME.N.ok> or <NAME.N.nok>,
    where NAME is used to match tests with solutions and N can be used as a
    counter for multiple solutions of the same test; 'ok'/'nok' indicates
    exprected result.
    """
    files = filter(lambda x: os.path.isfile(x),
                   map(lambda x: os.path.join(test_dir, x),
                       os.listdir(test_dir)))
    tests: Dict[FilePath, Set[FilePath]] = {}
    for basename, suff in map(lambda x: x.split('.', 1), files):
        if suff.endswith("ok") or suff.endswith("nok"):
            tests.setdefault(f"{basename}.q", set()).add(f"{basename}.{suff}")
        elif suff == "q":
            tests.setdefault(f"{basename}.q", set())

    test_count = 0
    sol_count = 0
    for task, solutions in tests.items():
        test_count += 1
        assert solutions, f"Test {task} has no solutions"
        for solution in sorted(solutions):
            sol_count += 1
            res = run_checker(task, solution, html)
            assert 0 <= res.returncode < 128, \
                f"Killed with signal {res.returncode}"
            passed = (res.returncode != 0) == solution.endswith("nok")
            if not passed:
                print(f"Output:\n{res.stdout.decode('utf8')}")
                print(f"Stderr:\n{res.stderr.decode('utf8')}\n")
            assert passed, f"Test failed for task {task} solution {solution}"
    logging.getLogger() \
        .info(f"Passed {test_count} tests, with {sol_count} solutions")


# Test entry points

def test_reg_text() -> None:
    run_tests("test/checker/reg", False)


def test_reg_html() -> None:
    run_tests("test/checker/reg", True)


def test_cfl_text() -> None:
    run_tests("test/checker/cfl", False)


def test_cfl_html() -> None:
    run_tests("test/checker/cfl", True)
