from __future__ import annotations
from typing import Set, FrozenSet, List, Dict, Union, Tuple, Deque, Optional, TypeVar, Any
from copy import deepcopy
from collections import deque
from lib.common import Character, State, Eps
from lib.dfa import DFA



class NFA:
    Transition = Dict[Tuple[State, Union[Character, Eps]], Set[State]]
    type_var = TypeVar('type_var')

    def __init__(self, states: Set[State],
                 characters: Set[Character],
                 transition: Transition,
                 init: State,
                 final: Set[State]):

        self.states = states
        self.characters = characters
        self.transition = transition
        self.init = init
        self.final = final

    def determinize(self) -> DFA:
        nfa = deepcopy(self)
        if nfa.has_epsilon():
            nfa = nfa.eliminate_epsilon()

        states: Set[FrozenSet[State]] = set()
        states.add(frozenset({nfa.init}))
        transition = {}
        final = set()
        done: Set[FrozenSet[State]] = set()

        while len(states.difference(done)) > 0:
            subset = (states.difference(done)).pop()  # arbitrary element from set
            if len(subset.intersection(nfa.final)) > 0:
                final.add(subset)
            for character in nfa.characters:
                new_subset: Set[State] = set()
                for state in subset:
                    if (state, character) in nfa.transition:
                        new_subset = new_subset.union(nfa.transition[state, character])

                states.add(frozenset(new_subset))
                transition[subset, character] = frozenset(new_subset)

            done.add(subset)

        new_states: Set[State] = set()
        new_transition: Dict[Tuple[State, Character], State] = dict()
        new_final: Set[State] = set()

        for state_set in states:
            if len(state_set) > 0:
                new_states.add(nfa.unset(state_set))
        for state_set in final:
            if len(state_set) > 0:
                new_final.add(nfa.unset(state_set))
        for state_set, character in transition:
            if len(state_set) > 0 and len(transition[state_set, character]) > 0:
                new_transition[nfa.unset(state_set), character] = nfa.unset(transition[state_set, character])

        return DFA(new_states, nfa.characters, new_transition, nfa.init, new_final)

    def unset(self, states: FrozenSet[State]) -> State:
        return State('_'.join(set(map(lambda x: x.name, sorted(states, key=lambda x: x.name)))))

    def has_epsilon(self) -> bool:
        for (state, character) in self.transition:
            if isinstance(character, Eps):
                return True
        return False

    def eliminate_epsilon(self) -> NFA:
        surroundings: Dict[State, Set[State]] = {}
        for state in self.states:
            surroundings[state] = self.epsilon_surroundings(state)

        new_transition: NFA.Transition = {}
        for state in self.states:
            for character in self.characters:
                reached_states = set()
                if (state, character) in self.transition:
                    reached_states.update(self.transition[state, character])

                for eps_state in surroundings[state]:
                    if (eps_state, character) in self.transition:
                        reached_states.update(self.transition[eps_state, character])

                if len(reached_states) > 0:
                    new_transition[state, character] = deepcopy(reached_states)
                    for reached in reached_states:
                        new_transition[state, character].update(surroundings[reached])

        new_final = deepcopy(self.final)
        if surroundings[self.init].intersection(self.final):
            new_final.add(self.init)

        return NFA(self.states, self.characters, new_transition, self.init, new_final)

    def epsilon_surroundings(self, state: State) -> Set[State]:
        reached: Deque[State] = deque([state])
        reachable: Set[State] = {state}
        eps = Eps()

        while len(reached) > 0:
            actual = reached.popleft()
            if (actual, eps) in self.transition:
                for dest_state in self.transition[actual, eps]:
                    if dest_state not in reachable:
                        reached.append(dest_state)
                        reachable.add(dest_state)

        return reachable