from functools import total_ordering


@total_ordering
class Terminal:
    def __init__(self, name: str):
        self.name = name

    def __eq__(self, obj) -> bool:
        if isinstance(obj, Terminal):
            return self.name == obj.name
        return False

    def __lt__(self, obj) -> bool:
        if isinstance(obj, Terminal):
            return self.name < obj.name
        return True

    def __hash__(self):
        return hash(self.name)

    def __repr__(self) -> str:
        return f"Terminal({self.name})"


@total_ordering
class Nonterminal:
    def __init__(self, name: str):
        self.name = name

    def __eq__(self, obj) -> bool:
        if isinstance(obj, Nonterminal):
            return self.name == obj.name
        return False

    def __lt__(self, obj) -> bool:
        if isinstance(obj, Nonterminal):
            return self.name < obj.name
        return True

    def __hash__(self):
        return hash(self.name)

    def __repr__(self) -> str:
        return f"Nonterminal({self.name})"


@total_ordering
class Character:
    def __init__(self, name: str):
        self.name = name

    def __eq__(self, obj) -> bool:
        if isinstance(obj, Character):
            return self.name == obj.name
        return False

    def __lt__(self, obj) -> bool:
        if isinstance(obj, Character):
            return self.name < obj.name
        return True

    def __hash__(self):
        return hash(self.name)

    def __repr__(self) -> str:
        return f"Character({self.name})"


@total_ordering
class State:
    def __init__(self, name: str):
        self.name = name

    def __eq__(self, obj) -> bool:
        if isinstance(obj, State):
            return self.name == obj.name
        return False

    def __lt__(self, obj) -> bool:
        if isinstance(obj, State):
            return self.name < obj.name
        return True

    def __hash__(self):
        return hash(self.name)

    def __str__(self) -> str:
        return self.name

    def __repr__(self) -> str:
        return f"State({self.name})"


class Eps:
    def __init__(self):
        self.name = "ε"

    def __eq__(self, other):
        return isinstance(other, Eps)

    def __hash__(self):
        return hash("ε")

    def __iter__(self):
        return iter(())

    def __len__(self):
        return 0

    def __repr__(self) -> str:
        return "Eps()"


class Emptyset:
    def __init__(self):
        self.name = "∅"

    def __eq__(self, other):
        return isinstance(other, Eps)

    def __hash__(self):
        return hash("∅")

    def __repr__(self) -> str:
        return "Emptyset()"
