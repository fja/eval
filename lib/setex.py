from __future__ import annotations
from collections import defaultdict
from enum import Enum, auto
from lib.common import State, Character, Emptyset, Eps
from lib.nfa import NFA
from lib.dfa import DFA
from typing import Set, Tuple, Union, Optional


class BinOp(Enum):
    Concat = auto()
    Intersection = auto()
    Union = auto()
    Difference = auto()


class UnOp(Enum):
    Complement = auto()
    Iteration = auto()
    PositiveIteration = auto()


class SetEx(object):

    class AtomSet(object):
        def __init__(self, words: Set[Tuple[Character, ...]]) -> None:
            self.words = words

        def _extract_alphabet(self) -> Set[Character]:
            return set.union(*(set(w) for w in self.words))

        def to_nfa(self, alphabet: Set[Character]) -> NFA:
            ini = State("init")
            fin = State("final")
            trans = defaultdict(set)
            states = {ini, fin}
            for w, word in enumerate(self.words):
                last_st = ini
                for c, char in enumerate(word):
                    st = State(f"{w}-{c}")
                    states.add(st)
                    trans[(last_st, char)].add(st)
                    last_st = st
                trans[(last_st, Eps())].add(fin)

            return NFA(states=states, transition=dict(trans),
                       characters=alphabet, init=ini, final={fin})

        def to_dfa(self, alphabet: Set[Character]) -> DFA:
            return self.to_nfa(alphabet).determinize().minimize()

    class Bin(object):
        def __init__(self, left: Node, right: Node, op: BinOp) -> None:
            self.left = left
            self.right = right
            self.op = op

        def _extract_alphabet(self) -> Set[Character]:
            return self.left._extract_alphabet() \
                | self.right._extract_alphabet()

        def to_nfa(self, alphabet: Set[Character]) -> NFA:
            # TODO: union (and intersection) can be also done on NFA
            if self.op == BinOp.Concat:
                next_state = 0

                def state_factory() -> State:
                    nonlocal next_state
                    next_state += 1
                    return State(str(next_state))

                lstatemap = defaultdict(state_factory)
                rstatemap = defaultdict(state_factory)

                left = self.left.to_nfa(alphabet)
                right = self.right.to_nfa(alphabet)

                new_trans = defaultdict(set)
                new_init = lstatemap[left.init]
                new_st = {lstatemap[f] for f in left.states} \
                    | {rstatemap[f] for f in right.states}
                new_final = {rstatemap[f] for f in right.final}

                for (sst, ch), dsts in left.transition.items():
                    print(f"δ({sst}, {ch.name}) = {dsts}")
                    print(f"δ'({lstatemap[sst]}, {ch.name}) = {set(lstatemap[s] for s in dsts)}")
                    new_trans[(lstatemap[sst], ch)] = \
                        {lstatemap[dst] for dst in dsts}
                for f in left.final:
                    new_trans[(lstatemap[f], Eps())].add(rstatemap[right.init])

                for (sst, ch), dsts in right.transition.items():
                    new_trans[(rstatemap[sst], ch)] = \
                        {rstatemap[dst] for dst in dsts}

                return NFA(init=new_init, characters=alphabet,
                           states=new_st, transition=new_trans,
                           final=new_final)
            else:
                return self.to_dfa(alphabet).to_nfa()

        def to_dfa(self, alphabet: Set[Character]) -> DFA:
            left = self.left.to_dfa(alphabet)
            right = self.right.to_dfa(alphabet)
            if self.op == BinOp.Intersection:
                return DFA.intersection(left, right)
            if self.op == BinOp.Union:
                return DFA.union(left, right)
            if self.op == BinOp.Difference:
                return DFA.subtraction(left, right)
            if self.op == BinOp.Concat:
                return self.to_nfa().determinize().minimize()
            assert False, f"invalid operation {self.op}"

    class Un(object):
        def __init__(self, node: Node, op: UnOp) -> None:
            self.node = node
            self.op = op

        def _extract_alphabet(self) -> Set[Character]:
            return self.node._extract_alphabet()

        def to_nfa(self, alphabet: Set[Character]) -> NFA:
            pass

    def __init__(self, node: Node,
                 alphabet: Optional[Set[Character]] = None) -> None:
        self.node = node
        if alphabet is not None:
            self.alphabet = alphabet
        else:
            self.alphabet = self.node._extract_alphabet()

    def to_nfa(self) -> NFA:
        return self.node.to_nfa(self.alphabet)


Node = Union[SetEx.AtomSet, SetEx.Bin, SetEx.Un]
