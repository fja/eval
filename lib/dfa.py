from __future__ import annotations
from typing import Set, FrozenSet, List, Dict, Union, Tuple, Deque, Optional, TypeVar, Any
from enum import Enum
import enum
from copy import deepcopy
from collections import deque
from string import ascii_uppercase
from lib.common import Character, State, Terminal, Nonterminal, Eps


class Composition(Enum):
    Union = enum.auto()
    Intersection = enum.auto()
    Subtraction = enum.auto()


class IsEmptyResult:
    def __init__(self, counterexample: Optional[str] = None, inf: Optional[bool] = None):
        self.counterexample = counterexample
        self.inf = inf

    def __bool__(self):
        return self.counterexample is None


class IsEquivalentResult:
    def __init__(self, left_counterexample: Optional[str] = None,
                       right_counterexample: Optional[str] = None,
                       inf: Optional[bool] = None):
        self.left_counterexample = left_counterexample
        self.right_counterexample = right_counterexample
        self.inf = inf

    def __bool__(self):
        return self.left_counterexample is None and self.right_counterexample is None


class DFA:
    Transition = Dict[Tuple[State, Character], State]
    type_var = TypeVar('type_var')

    def __init__(self, states: Set[State],
                 characters: Set[Character],
                 transition: Transition,
                 init: State,
                 final: Set[State]):

        self.states = states
        self.characters = characters
        self.transition = transition
        self.init = init
        self.final = final
        self.check()

    def check(self):  # this control exists mainly for development reasons
        assert len(self.states) > 0, "empty automaton"

        for (state, character) in self.transition:
            assert state in self.states, "unknown state " + state.name
            assert character in self.characters, "unknown character " + character.name
            assert self.transition[state, character] in self.states, \
                "unknown state " + self.transition[state, character].name

        assert self.init in self.states, "init not in states"
        for state in self.final:
            assert state in self.states, "unknown state " + state.name

    def total(self) -> DFA:
        if self.is_total():
            return self

        dfa = deepcopy(self)
        total: DFA.Transition = {}

        hell = self.hell(dfa.states)
        dfa.states.add(hell)

        for state in dfa.states:
            for character in dfa.characters:
                if (state, character) in dfa.transition:
                    total[state, character] = dfa.transition[state, character]
                else:
                    total[state, character] = hell

        dfa.transition = total
        return dfa

    def is_total(self) -> bool:
        for state in self.states:
            for character in self.characters:
                if (state, character) not in self.transition:
                    return False
        return True

    def accepts(self, word: Union[str, List[Character], Eps]) -> bool:
        evaluate: List[Character] = []
        if isinstance(word, str):
            for letter in word:
                character = Character(letter)
                evaluate.append(character)
        elif isinstance(word, List):
            evaluate = word

        if len(evaluate) == 0:
            return self.init in self.final

        state = self.init
        for character in evaluate:
            if (state, character) not in self.transition:
                return False
            state = self.transition[state, character]
        return state in self.final

    def complement(self) -> DFA:
        dfa = deepcopy(self)
        if not dfa.is_total():
            dfa = dfa.total()

        dfa.final = dfa.states.difference(dfa.final)
        return dfa

    @staticmethod
    def state_composition(state_1: State, state_2: State) -> State:
        return State(f"{state_1.name}_{state_2.name}")

    @staticmethod
    def composition(dfa_1: DFA, dfa_2: DFA, operation: Composition) -> DFA:
        """Does not require input automata to have the same alphabet."""

        # new characters
        characters = dfa_1.characters.union(dfa_2.characters)
        dfa_1.characters = characters
        dfa_2.characters = characters

        if not dfa_1.is_total():
            dfa_1 = dfa_1.total()
        if not dfa_2.is_total():
            dfa_2 = dfa_2.total()

        # new init
        init = DFA.state_composition(dfa_1.init, dfa_2.init)

        # new states and transition
        states = {init}
        transition: Dict[Tuple[State, Character], State] = dict()
        for state_1 in dfa_1.states:
            for state_2 in dfa_2.states:
                state = DFA.state_composition(state_1, state_2)  # TODO avoid conflicts!
                states.add(state)

                # new transition
                for character in characters:
                    if (state_1, character) in dfa_1.transition and \
                            (state_2, character) in dfa_2.transition:
                        dest_state_1 = dfa_1.transition[state_1, character]
                        dest_state_2 = dfa_2.transition[state_2, character]
                        dest_state = DFA.state_composition(dest_state_1, dest_state_2)
                        transition[state, character] = dest_state

        # new final
        final: Set[State] = set()
        for state_1 in dfa_1.states:
            for state_2 in dfa_2.states:
                state = DFA.state_composition(state_1, state_2)

                if operation == Composition.Union and \
                        (state_1 in dfa_1.final or \
                        state_2 in dfa_2.final):
                    final.add(state)

                elif operation == Composition.Intersection and \
                        state_1 in dfa_1.final and \
                        state_2 in dfa_2.final:
                    final.add(state)

                elif operation == Composition.Subtraction and \
                        state_1 in dfa_1.final and \
                        state_2 not in dfa_2.final:
                    final.add(state)

        dfa = DFA(states, characters, transition, init, final)
        return dfa.eliminate_unreachable().canonize()

    @staticmethod
    def union(dfa_1: DFA, dfa_2: DFA) -> DFA:
        return DFA.composition(dfa_1, dfa_2, Composition.Union)

    @staticmethod
    def intersection(dfa_1: DFA, dfa_2: DFA) -> DFA:
        return DFA.composition(dfa_1, dfa_2, Composition.Intersection)

    @staticmethod
    def subtraction(dfa_1: DFA, dfa_2: DFA) -> DFA:
        return DFA.composition(dfa_1, dfa_2, Composition.Subtraction)

    def eliminate_unreachable(self) -> DFA:
        dfa = deepcopy(self)
        previous: Set[State] = set()
        actual = {self.init}

        while previous != actual:
            previous = deepcopy(actual)

            for state in previous:
                for character in dfa.characters:
                    if (state, character) in self.transition:
                        actual.add(self.transition[state, character])

        dfa.states = actual
        dfa.final = dfa.final.intersection(actual)
        for (state, character) in self.transition:
            if state not in actual:
                del dfa.transition[state, character]

        return dfa

    def sorted_characters(self) -> List[Character]:
        return sorted(self.characters, key=lambda x: x.name)

    def minimize(self) -> DFA:  # 26-30, section 2.1.5
        dfa = self.eliminate_unreachable().total()
        characters = self.sorted_characters()  # bc I want to iterate always in the same order

        previous: Dict[State, int] = dict()
        actual: Dict[State, int] = dict()
        for state in dfa.states:  # first classification: final or non-final
            if state in dfa.final:
                actual[state] = 1
            else:
                actual[state] = 0

        # how many classes are there now:
        classes_prev = 0
        classes_new = 1 if len(self.final) > 0 else 0     # there exist some final states
        if len(self.states.difference(self.final)) > 0:   # and there exist some non-final states
            classes_new += 1

        while classes_prev != classes_new:  # did number of classes change?
            previous = deepcopy(actual)
            patterns: Dict[Tuple[int, ...], Set[State]] = dict()
            actual = dict()

            for state in dfa.states:          # each state gets new pattern consisting of
                pattern = [previous[state]]   # previous class
                for character in characters:  # and for each transition
                    pattern.append(previous[dfa.transition[state, character]]) # previous class of dest_state

                patterns.setdefault(tuple(pattern), set()).add(state)

            classes_prev = classes_new
            classes_new = 0
            for states in patterns.values():     # according to new patterns
                for state in states:             # each state
                    actual[state] = classes_new  # gets to new class
                classes_new += 1

        new_transition: DFA.Transition = {}
        dfa.states = set()
        dfa.final = set()
        for state in actual:  # according to last classification of states
            new_state = State("new_" + str(actual[state]))
            dfa.states.add(new_state)

            for character in characters:
                if not (new_state, character) in new_transition:
                    old = dfa.transition[state, character]
                    now = State("new_" + str(actual[old]))
                    new_transition[new_state, character] = now

            if state == self.init:
                dfa.init = new_state
            if state in self.final:
                dfa.final.add(new_state)

        dfa.transition = new_transition
        return dfa.canonize()

    def rename_state(self, rename: State, name: str):
        if name == rename.name:
            return
        new_state = State(name)
        self.states.add(new_state)
        self.states.remove(rename)
        transition = deepcopy(self.transition)

        if rename == self.init:
            self.init = new_state
        if rename in self.final:
            self.final.add(new_state)
            self.final.remove(rename)

        for character in self.characters:
            if (rename, character) in self.transition:
                transition[new_state, character] = self.transition[rename, character]
                del transition[rename, character]

        for state, character in transition:
            if transition[state, character] == rename:
                transition[state, character] = new_state

        self.transition = transition

    @staticmethod
    def bijective26(num: int) -> str:
        name = ""
        if num < 26:
            return ascii_uppercase[num]
        while num > 26:
            name = ascii_uppercase[num % 26 - (num // 26 < 26)] + name
            num = num // 26
        name = ascii_uppercase[num - 1] + name
        return name

    @staticmethod
    def canonic_name_for_seq(num: int) -> str:
        # alternatively, use self.bijective26(i) for base-26 alpha. names
        return str(num)

    def _can_walk(self, state_callback: Callable[None, [State, State]]) -> Set[State]:
        seen: Set[State] = set()
        characters = self.sorted_characters()

        i = 1
        queue = deque([self.init])
        while len(queue) > 0:
            actual = queue.popleft()
            if actual in seen:
                continue
            seen.add(actual)
            state_callback(actual, State(self.canonic_name_for_seq(i)))
            i += 1

            for character in characters:
                tgt = self.transition.get((actual, character))
                if tgt is not None and tgt not in seen:
                    queue.append(tgt)
        return seen

    def canonize(self) -> DFA:
        """
        Returns a canonical automaton for ‹self›. Does NOT minimize the
        automata, therefore canonize is useful for testing isomorfism, for
        testing equivalence you should use ‹is_equivalent› or
        ‹minimize().canonize()›.
        Precondition: ‹self› has no unreachable states (exception thrown otherwise).
        """
        state_map: Dict[State, State] = {}
        def add(old: State, new: State):
            state_map[old] = new
        seen = self._can_walk(add)

        if self.states - seen:
            raise Exception("Automaton cannot be canonised, it has "
                            "unreachable states")

        return DFA(set(state_map.values()),
                   deepcopy(self.characters),
                   {(state_map[st], ch): state_map[tgt]
                    for (st, ch), tgt in self.transition.items()},
                   state_map[self.init],
                   {state_map[st] for st in self.final})

    # support function: decides from which of reachable states there is a way to final state
    def terminating_states(self, states: Set[State], pred: Dict[State, Set[State]]) -> Set[State]:
        unknown: Set[State] = states.difference(self.final)
        to_add: Set[State] = states.intersection(self.final)
        terminate: Set[State] = set()

        while len(to_add) > 0:
            # print(','.join(set(map(lambda x: x.name, unknown))))
            # print(','.join(set(map(lambda x: x.name, to_add))))
            # print(','.join(set(map(lambda x: x.name, terminate))))
            terminate.update(to_add)
            to_add = set()
            for state in terminate:
                to_add.update(pred[state].intersection(unknown))
            unknown = unknown.difference(to_add)

        return terminate

    def get_character(self, state: State, dest_state: State) -> Character:
        for character in self.characters:
            if (state, character) in self.transition and \
                    self.transition[state, character] == dest_state:
                return character
        return Character("")

    @staticmethod
    def one_of(collection: Set[type_var]) -> type_var:
        return next(iter(collection))

    def is_empty(self) -> IsEmptyResult:  # 52, 2.74, 2.76
        completed: Set[State] = set()
        stack: List[State] = [self.init]

        pred: Dict[State, Set[State]] = {self.init: set()}
        succ: Dict[State, Set[State]] = {}
        terminable: Set[State] = set()
        on_cycle: Set[State] = set()

        # DFA colouring of vertices (states)
        # white: not in stack and not in completed
        # grey: in stack and not in completed
        # black: in completed

        while len(stack) > 0:
            actual = stack[-1]
            succ.setdefault(actual, set())
            update = False
            for character in self.characters:

                if (actual, character) in self.transition:
                    dest_state = self.transition[actual, character]
                    if dest_state in succ[actual]:
                        continue  # edge already done

                    pred.setdefault(dest_state, set()).add(actual)
                    succ[actual].add(dest_state)

                    if dest_state in stack:
                        on_cycle.add(actual)
                    elif dest_state not in completed:
                        stack.append(dest_state)
                        break

            if actual == stack[-1]:
                stack.pop()
                completed.add(actual)

        if len(completed.intersection(self.final)) == 0:
            return IsEmptyResult()

        terminable = self.terminating_states(completed, pred)
        for sstate in succ:
            succ[sstate] = succ[sstate].intersection(terminable)

        if self.init in self.final:
            return IsEmptyResult('ε', len(on_cycle.intersection(terminable)) > 0)

        word: List[str] = []
        state: State = self.init
        queue: Deque = deque({self.init})
        predecessor: Dict[State, State] = {}
        finished: Set[State] = set()

        while state not in self.final:
            state = queue.popleft()
            finished.add(state)
            for successor in succ[state]:
                if successor not in queue and successor not in finished:
                    queue.append(successor)
                    predecessor[successor] = state

        while state != self.init:
            word.append(self.get_character(predecessor[state], state).name)
            state = predecessor[state]

        if len(on_cycle.intersection(terminable)) > 0:
            return IsEmptyResult(''.join(reversed(word)), True)
        return IsEmptyResult(''.join(reversed(word)), False)

    # Ha!
    def is_universal(self):  # 52, 2.74
        return self.complement().is_empty()

    # returns: True or fst_empty: what IS in first language and NOT in second, snd_empty: analogically)
    @staticmethod
    def is_equivalent(fst: DFA, snd: DFA) -> IsEquivalentResult:  # 52, 2.77
        fst_empty = DFA.intersection(fst, snd.complement()).is_empty()
        snd_empty = DFA.intersection(fst.complement(), snd).is_empty()

        if fst_empty and snd_empty:
            result = IsEquivalentResult()
        elif fst_empty.inf or snd_empty.inf:
            result = IsEquivalentResult(fst_empty.counterexample, snd_empty.counterexample, True)
        else:
            result = IsEquivalentResult(fst_empty.counterexample, snd_empty.counterexample)
        return result

    @staticmethod
    def is_identical(fst: DFA, snd: DFA) -> bool:
        """
        Check that two automata are identical, i.e. they are the same including
        names.
        """
        return fst.states == snd.states and fst.characters == snd.characters and \
               fst.transition == snd.transition and fst.init == snd.init and \
               fst.final == snd.final

    def is_minimal(self) -> bool:
        minimal = self.minimize()
        return self.is_total() and len(self.states) == len(minimal.states) and \
               len(self.transition) == len(minimal.transition)

    def is_canonical(self) -> bool:
        # note: not all DFAs have canonic form and therefore it is not possible
        # to test if automaton is canonic by canonising it and testing for
        # identity. In particular canonize() needs the automaton to have no
        # unreachable states to succeed.
        is_can = True
        def check(old: State, new: State) -> None:
            nonlocal is_can
            is_can = is_can and old.name == new.name
        seen = self._can_walk(check)
        return is_can and self.states == seen

    def hell(self, states):
        # check: consider import itertools
        hell_id = 0
        hell = State(str(hell_id))
        while hell in states:
            hell_id += 1
            hell.name = str(hell_id)
        return hell
