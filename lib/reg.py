from typing import Set
from lib.common import Terminal, Nonterminal, Eps
from lib.grammars import RegGrammar, CFG
from lib.dfa import DFA, IsEquivalentResult
from lib.nfa import NFA
from lib.regex import RegEx


class RegGrammar(RegGrammar):
    def eliminate_useless(self):
        grammar = super().eliminate_useless() # TODO ask how to do this more efficiently
        return RegGrammar(grammar.nonterminals, grammar.terminals, grammar.rules, grammar.init)
        

class Grammar(CFG):
    pass


class DFA(DFA):
    def dfa_to_nfa(self) -> NFA:
        new_transition: NFA.transition = {}
        for (state, character) in self.transition:
            new_transition[state, character] = {self.transition[state, character]}
        return NFA(self.states, self.characters, new_transition, self.init, self.final)


class NFA(NFA):
    def eliminate_epsilon(self) -> NFA:
        nfa = super().eliminate_epsilon() # TODO ask how to do this more efficiently
        return NFA(nfa.states, nfa.characters, nfa.transition, nfa.init, nfa.final)

    def nfa_to_reggrammar(self) -> RegGrammar:
        nfa: NFA = self.eliminate_epsilon()
        nonterminals: Set[Nonterminal] = set()
        terminals: Set[Terminal] = set()
        rules: RegGrammar.Rules = dict()
        init = Nonterminal(nfa.init.name)

        for state in nfa.states:
            nonterminals.add(Nonterminal(state.name))
        for character in nfa.characters:
            terminals.add(Terminal(character.name))

        for state, character in nfa.transition:
            for dest_state in nfa.transition[state, character]:
                nonterminal1 = Nonterminal(state.name)
                nonterminal2 = Nonterminal(dest_state.name)
                terminal = Terminal(character.name)
                rules.setdefault(nonterminal1, set()).add((terminal, nonterminal2))

                if dest_state in nfa.final:
                    rules[nonterminal1].add(terminal)

        if nfa.init in nfa.final:
            new_init = Nonterminal("<newinit>")
            rules[new_init] = {Eps()}
            rules[new_init].update(rules[init])
            init = new_init
            nonterminals.add(init)

        return RegGrammar(nonterminals, terminals, rules, init)

    #def nfa_to_regex(self) -> RegEx:
    #    # I don't want to have to do this really don't
    #    pass

class RegEx(RegEx):
    def regex_to_efa(self) -> NFA:
        nfa = super().regex_to_efa()  # TODO ask how to do this more efficiently
        return NFA(nfa.states, nfa.characters, nfa.transition, nfa.init, nfa.final)
