from __future__ import annotations
from copy import deepcopy
from typing import Set, Dict, Union, Tuple, TypeVar, List
from lib.common import Terminal, Nonterminal, Character, State, Eps
from lib.nfa import NFA
from lib.grammars_cfg import CFG

# Typecheck is sufficient for grammar being a grammar of certain type
# with exception of Eps only within the init rule and not use of init elsewhere.


class RegGrammar:
    Rules = Dict[Nonterminal, Set[Union[Terminal, Tuple[Terminal, Nonterminal]]]]
    type_var = TypeVar('type_var')

    def __init__(self, nonterminals: Set[Nonterminal],
                 terminals: Set[Terminal],
                 rules: Rules,
                 init: Nonterminal):
        self.nonterminals = nonterminals
        self.terminals = terminals
        self.rules = rules
        self.init = init
        self.check()

    @staticmethod
    def from_cfg(cfg: CFG) -> RegGrammar:
        cfg.check_regular(throw=True)
        reg_rules : RegGrammar.Rules = {}
        for nonterminal in cfg.rules:
            for rule in cfg.rules[nonterminal]:
                new_rule: Union[Terminal, Tuple[Terminal, Nonterminal]] = rule
                # rule of type Tuple[Terminal] becomes of type Terminal, others are OK
                if isinstance(rule, tuple) and isinstance(rule[0], Terminal) and len(rule) == 1:
                    new_rule = rule[0]
                reg_rules.setdefault(nonterminal, set()).add(new_rule)

        return RegGrammar(cfg.nonterminals, cfg.terminals, reg_rules, cfg.init)
        # exception

    def check(self) -> None:
        assert len(self.nonterminals) > 0, "empty grammar"

        has_eps_start = False
        has_start_loop = False
        for nonterminal in self.rules:
            assert nonterminal in self.nonterminals, \
                f"unknown nonterminal {nonterminal.name}"
            for rule in self.rules[nonterminal]:
                if isinstance(rule, Eps):
                    assert nonterminal == self.init, \
                        "ε can only appear for initial nonterminal, " \
                        f"appears for {nonterminal}"
                    has_eps_start = True
                elif isinstance(rule, Terminal):
                    assert rule in self.terminals, "unknown terminal " + rule.name
                else:
                    assert rule[0] in self.terminals, "unknown terminal " + rule[0].name
                    assert rule[1] in self.nonterminals, "unknown nonterminal " + rule[1].name
                    has_start_loop |= rule[1] == self.init

        assert self.init in self.nonterminals, "init not in nonterminals"
        assert not has_eps_start or not has_start_loop, \
            "since ε is present the start nonterminal must not appear on any " \
            "right-hand side of a rule"

    def reggrammar_to_nfa(self) -> NFA:
        states: Set[State] = set()
        for nonterminal in self.nonterminals:
            states.add(State(nonterminal.name))

        final = State("newfinal")  # TODO assert that name is unique, as for hell in make_total
        states.add(final)

        init = State(self.init.name)

        characters: Set[Character] = set()
        for terminal in self.terminals:
            characters.add(Character(terminal.name))

        transition: NFA.Transition = dict()
        for nonterminal in self.rules:
            state = State(nonterminal.name)
            for rule in self.rules[nonterminal]:

                # rule A -> a becomes d(A, a) = final
                if isinstance(rule, Terminal):
                    character = Character(rule.name)
                    transition.setdefault((state, character), set()).add(final)

                # rule A -> aB becomes d(A, a) = B
                elif isinstance(rule, Tuple):
                    character = Character(rule[0].name)
                    transition.setdefault((state, character), set()).add(State(rule[1].name))

        final_states = {final}
        for rule in self.rules[self.init]:
            if isinstance(rule, Eps):
                final_states.add(init)

        nfa = NFA(states, characters, transition, init, final_states)
        return nfa

    def eliminate_useless(self) -> RegGrammar:
        grammar = deepcopy(self)

        helper = Nonterminal("<helpernonterminal>")
        previous: Set[Nonterminal] = set()
        actual: Set[Nonterminal] = {helper}
        while previous != actual:
            previous = deepcopy(actual)

            for nonterminal in grammar.nonterminals:
                if nonterminal in grammar.rules:
                    for rule in grammar.rules[nonterminal]:
                        if isinstance(rule, Terminal) or isinstance(rule, Eps):
                            actual.add(nonterminal)
                        elif rule[1] in previous:
                            actual.add(nonterminal)

        actual.remove(helper)
        grammar.nonterminals = actual

        previous = set()
        actual = {self.init}

        while previous != actual:
            previous = deepcopy(actual)

            for nonterminal in previous:
                if nonterminal in grammar.rules:
                    for rule in grammar.rules[nonterminal]:
                        if isinstance(rule, tuple):
                            actual.add(rule[1])

        grammar.nonterminals = grammar.nonterminals.intersection(actual)

        for nonterminal in self.rules:
            if nonterminal not in grammar.nonterminals:
                del grammar.rules[nonterminal]
                continue
            for rule in self.rules[nonterminal]:
                if isinstance(rule, Tuple) and rule[1] not in grammar.nonterminals:
                    grammar.rules[nonterminal].remove(rule)

        return grammar
