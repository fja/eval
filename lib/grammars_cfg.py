from __future__ import annotations
from typing import Set, Dict, List, Union, Optional, Tuple, Iterable, \
    Deque, Callable, TypeVar
import typing
from typing_extensions import Final
from copy import deepcopy
from collections import deque
from lib.common import Terminal, Nonterminal, Eps
from lib.dfa import IsEquivalentResult  # TODO: to common
import random
import math

T = TypeVar("T")
TA = TypeVar("TA")
TB = TypeVar("TB")


def all_of(pred: Callable[[T], bool], it: Iterable[T]) -> bool:
    return all(map(pred, it))


def any_of(pred: Callable[[T], bool], it: Iterable[T]) -> bool:
    return any(map(pred, it))


class NotRegularException (Exception):
    def __init__(self, message: str) -> None:
        super().__init__(f"Gramatika není regulární: {message}")


class GeneratesResult:
    def __init__(self, value: bool, cnf_cfg: CFG,
                 cyk_table: Optional[List[List[Set[Nonterminal]]]] = None):
        self.value: Final = value
        self.cnf_cfg: Final = cnf_cfg
        self.cyk_table: Final = cyk_table

    def __bool__(self) -> bool:
        return self.value

    # for the sake of assertions/pytest
    def __repr__(self) -> str:
        info = ""
        if self.cyk_table is not None:
            inc = "∈" if self.value else "∉"
            info = f" ({self.cnf_cfg.init.name} {inc} " \
                   f"{set(x.name for x in self.cyk_table[-1][0])})"
        return f"{self.value}{info}"


class InfinityType:
    pass

    def __add__(self, other) -> InfinityType:
        return self

    __radd__ = __add__

    def __mul__(self, other) -> InfinityType:
        assert isinstance(other, InfinityType) or other > 0
        return self

    __rmul__ = __mul__

    def __lt__(self, other) -> bool:
        return False

    def __le__(self, other) -> bool:
        return isinstance(other, InfinityType)

    def __gt__(self, other) -> bool:
        return not isinstance(other, InfinityType)

    def __ge__(self, other) -> bool:
        return True

    def __repr__(self) -> str:
        return "Infinity"


Infinity = InfinityType()


def zip_fill(seqa: Iterable[TA], seqb: Iterable[TB], sentinel: bool = False) \
        -> Iterable[Tuple[Optional[TA], Optional[TB]]]:
    def end():
        if sentinel:
            yield (None, None)

    ita = iter(seqa)
    itb = iter(seqb)
    while True:
        try:
            a = next(ita)
        except StopIteration:
            for b in itb:
                yield (None, b)
            yield from end()
            return
        try:
            b = next(itb)
        except StopIteration:
            yield (a, None)
            for a in ita:
                yield (a, None)
            yield from end()
            return
        yield (a, b)

    yield from end()


class ChangeTracker:
    __slots__ = ["_changed"]

    def __init__(self):
        self._changed = True

    def __iter__(self) -> ChangeTracker:
        return self

    def __next__(self) -> ChangeTracker:
        if not self._changed:
            raise StopIteration
        self._changed = False
        return self

    def changed(self) -> None:
        self._changed = True


class CFG:
    Symbol = Union[Terminal, Nonterminal]
    Symbols = Tuple[Symbol, ...]
    Production = Union[Eps, Tuple[Symbol, ...]]
    Sentence = Union[Eps, Tuple[Symbol, ...]]
    Word = Tuple[Terminal, ...]
    Rules = Dict[Nonterminal, Set[Production]]

    __slots__ = ["nonterminals", "terminals", "rules", "init"]

    def __init__(self, nonterminals: Set[Nonterminal],
                 terminals: Set[Terminal],
                 rules: CFG.Rules,
                 init: Nonterminal):
        self.nonterminals: Final = deepcopy(nonterminals)
        self.terminals: Final = deepcopy(terminals)
        self.rules: Final = deepcopy(rules)
        self.init: Final = deepcopy(init)

        # normalize rules: avoid any rules leading to empty set of productions
        to_drop: Set[Nonterminal] = set()
        for src, prods in self.rules.items():
            if len(prods) == 0:
                to_drop.add(src)
        for src in to_drop:
            del self.rules[src]
        self._check()

    def is_nonterminal_used(self, nonterminal) -> bool:
        for nonterm in self.rules:
            for rule in self.rules[nonterm]:
                if not isinstance(rule, Eps) and nonterminal in rule:
                    return True
        return False

    def check_regular(self, throw: bool = False) -> bool:
        for nonterminal in self.rules:
            for rule in self.rules[nonterminal]:
                if isinstance(rule, Eps):
                    if nonterminal == self.init and not self.is_nonterminal_used(nonterminal):
                        continue
                    if throw:
                        raise NotRegularException(
                            "výskyt epsilon u neterminálu, který se "
                            "vyskytuje na pravé straně některého pravidla.")
                    return False

                if isinstance(rule, tuple):  # TODO nicer
                    if (len(rule) == 1 and isinstance(rule[0], Terminal)) or \
                       (len(rule) == 2 and isinstance(rule[0], Terminal) and isinstance(rule[1], Nonterminal)):
                        continue

                if throw:
                    raise NotRegularException(
                        "špatný tvar pravé strany některého z pravidel.")
                return False
        return True

    def productions(self) -> Iterable[Tuple[Nonterminal, CFG.Production]]:
        for src, prods in self.rules.items():
            for prod in prods:
                yield (src, prod)

    @staticmethod
    def empty(terminals: Set[Terminal]) -> CFG:
        S = Nonterminal("S")
        return CFG({S}, deepcopy(terminals), dict(), S)

    def _check(self) -> None:
        assert self.init in self.nonterminals,\
            f"Initial nonterminal {self.init.name} must be in nonterminals"
        for nterm, prod in self.productions():
            assert nterm in self.nonterminals,\
                f"A rule for {nterm.name} exists, "\
                f"but it is not in nonterminals"
            assert len(prod) != 0 or isinstance(prod, Eps), \
                f"Noncanonical epsilon representation (empty tuple) used"
            for x in prod:
                if isinstance(x, Nonterminal):
                    assert x in self.nonterminals,\
                        f"A rule containing nonterminal {x.name} found, "\
                        f"but it is not in nonterminals"
                else:
                    assert isinstance(x, Terminal),\
                        f"Neither terminal not nonterminal symbol found: "\
                        f"{x.name}"
                    assert x in self.terminals,\
                        f"A rule containing terminal {x.name} found, "\
                        f"but it is not in terminals"

    def reduced(self) -> CFG:
        return self.normalized().remove_unreachable()

    def normalized(self) -> CFG:
        normalized_nts: Set[Nonterminal] = set()
        added = True
        while added:
            added = False
            for src, prods in self.rules.items():
                for prod in prods:
                    if src not in normalized_nts and \
                            all_of(lambda x:
                                   x in normalized_nts or x in self.terminals,
                                   prod):
                        normalized_nts.add(src)
                        added = True
                        break

        return self.restrict_symbols(normalized_nts | self.terminals)

    def restrict_symbols(self, symbols: Set[Union[Terminal, Nonterminal]])\
            -> CFG:
        if self.init not in symbols:
            return CFG.empty(self.terminals & symbols)

        nonterminals = self.nonterminals & symbols
        terminals = self.terminals & symbols
        rules: CFG.Rules = dict()

        for src, prods in self.rules.items():
            if src not in symbols:
                continue
            new_prods: Set[CFG.Production] = set()
            for prod in prods:
                if all_of(lambda x: x in symbols, prod):
                    new_prods.add(prod)
            if new_prods:
                rules[src] = new_prods
        return CFG(nonterminals, terminals, rules, self.init)

    def remove_unreachable(self) -> CFG:
        if len(self.rules) == 0:
            return self

        old_reachable: Set[CFG.Symbol] = set()
        reachable: Set[CFG.Symbol] = {self.init}
        while len(old_reachable) < len(reachable):
            old_reachable = deepcopy(reachable)
            for src in old_reachable:
                if isinstance(src, Nonterminal) and src in self.rules:
                    for prod in self.rules[src]:
                        for symbol in prod:
                            reachable.add(symbol)

        return self.restrict_symbols(reachable)

    def is_empty(self) -> bool:
        norm = self.normalized()
        return norm.init not in norm.rules

    def is_infinite(self) -> bool:
        prop = self.proper()
        rec = prop._recursive_nonterminals()
        return len(rec) > 0

    def is_epsilon_normal_form(self) -> bool:
        has_eps = False
        has_non_start_eps = False
        recursive_start = False
        for src, prods in self.rules.items():
            if Eps() in prods:
                has_eps = True
                if src != self.init:
                    has_non_start_eps = True
            if any_of(lambda prod: self.init in prod, prods):
                recursive_start = True

        return not has_eps or (not has_non_start_eps and not recursive_start)

    def epsilon_normal_form(self) -> CFG:
        if self.is_epsilon_normal_form():
            return self

        erasable = set()
        added = True
        while added:
            added = False
            for src, prod in self.productions():
                if src in erasable:
                    continue
                if prod == [] or all_of(lambda x: x in erasable, prod):
                    erasable.add(src)
                    added = True
                    continue

        new_rules: CFG.Rules = dict()

        def drop(prod: CFG.Production) -> Iterable[CFG.Symbols]:
            if isinstance(prod, Eps) or len(prod) == 0:
                yield ()
                return
            head = prod[0]
            for tail in drop(prod[1:]):
                if head in erasable:
                    yield tail
                yield (head,) + tail

        for src, prod in self.productions():
            for new_prod in drop(prod):
                if new_prod:
                    new_rules.setdefault(src, set()).add(new_prod)
        if self.init in erasable:
            new_init = Nonterminal("S")
            while new_init in self.nonterminals:
                new_init.name += "'"
            new_rules[new_init] = {Eps(), (self.init,)}
            return CFG(self.nonterminals | {new_init}, self.terminals,
                       new_rules, new_init)
        return CFG(self.nonterminals, self.terminals, new_rules, self.init)

    def get_simple_to(self) -> Dict[Nonterminal, Set[Nonterminal]]:
        """
        Get a mapping from nonterminal to a set of nonterminals it can be
        *nontrivially* rewritten to (i.e., it does not include the nonterminal
        itself unless there is a rule of form X → X for it")
        """
        simple_to : Dict[Nonterminal, Set[Nonterminal]] \
            = {n: set() for n in self.nonterminals}

        for tracker in ChangeTracker():
            for src_orig in self.nonterminals:
                # make a copy to avoid error for changing during iteraton
                for src in list({src_orig} | simple_to[src_orig]):
                    for prod in self.rules.get(src, []):
                        if not isinstance(prod, Eps) and len(prod) == 1 \
                                and isinstance(prod[0], Nonterminal) \
                                and prod[0] not in simple_to[src_orig]:
                            tracker.changed()
                            simple_to[src_orig].add(prod[0])

        return simple_to

    def has_simple_rules(self) -> bool:
        return any_of(lambda v: len(v) != 0, self.get_simple_to().values())

    def remove_simple_rules(self) -> CFG:
        simple_to = self.get_simple_to()

        new_rules: CFG.Rules = dict()
        for src in self.nonterminals:
            for esrc in {src} | simple_to[src]:
                for prod in self.rules.get(esrc, []):
                    if len(prod) != 1 or typing.cast(CFG.Symbols, prod)[0] \
                            in self.terminals:
                        new_rules.setdefault(src, set()).add(prod)
        return CFG(self.nonterminals, self.terminals, new_rules, self.init)

    def proper(self) -> CFG:
        return self.epsilon_normal_form().remove_simple_rules().reduced()

    def cnf(self) -> CFG:
        prop = self.proper()
        new_nontminals = deepcopy(prop.nonterminals)
        new_rules: CFG.Rules = dict()

        def get_cnf_name(prod_part: CFG.Production) -> Nonterminal:
            nterm = Nonterminal('<' +
                                ''.join(map(lambda x: x.name.replace('>', '_')
                                            .replace('<', '_'), prod_part))
                                + '>')
            while nterm in new_nontminals:
                nterm.name += "'"
            new_nontminals.add(nterm)
            return nterm

        new_nterms: Dict[CFG.Production, Nonterminal] = dict()

        def mk_cnf_prod(src: Nonterminal, prod: CFG.Production) -> None:
            if src not in new_rules:
                new_rules[src] = set()
            if isinstance(prod, Eps) or len(prod) == 1:
                assert isinstance(prod, Eps) or prod[0] in prop.terminals
                new_rules[src].add(prod)
            else:
                half = len(prod) // 2
                tgt = []
                for x in [prod[:half], prod[half:]]:
                    if len(x) == 1 and x[0] in prop.nonterminals:
                        tgt.append(x[0])
                    elif x in new_nterms:
                        tgt.append(new_nterms[x])
                    else:
                        tgt_nt = get_cnf_name(x)
                        mk_cnf_prod(tgt_nt, x)
                        tgt.append(tgt_nt)
                        new_nterms[x] = tgt_nt
                new_rules[src].add(tuple(tgt))

        for src, prod in prop.productions():
            mk_cnf_prod(src, prod)

        return CFG(new_nontminals, prop.terminals, new_rules, prop.init)

    def is_cnf(self) -> bool:
        for src, prod in self.productions():
            # X -> ???+
            if len(prod) > 2:
                return False

            # X -> ?? with some terminal symbol
            if len(prod) == 2 and \
                    any_of(lambda x: not isinstance(x, Nonterminal), prod):
                return False

            # X -> Y
            if len(prod) == 1 and not \
                    isinstance(typing.cast(CFG.Symbols, prod)[0], Terminal):
                return False

        return self.is_epsilon_normal_form()

    def generates(self, word: Union[str, Iterable[Terminal]])\
            -> GeneratesResult:
        """
        Check if the grammar generates the given word.
        Uses the C-Y-K algorithm, so the check is in O(|CNF|^3) where CNF is
        the Chomsky normal form of the grammar.

        Returns an instance of GeneratesResult, which is convertible to bool.
        It also contains a CNF of the original grammar and a C-Y-K table
        (unless the input word was an empty word).
        """

        cnf = self if self.is_cnf() else self.cnf()
        if isinstance(word, str):
            word = [Terminal(x) for x in word]
        else:
            word = list(word)

        if cnf.init not in cnf.rules:
            return GeneratesResult(False, cnf)

        n = len(word)
        if n == 0:
            return GeneratesResult(Eps() in cnf.rules[cnf.init], cnf)

        table: List[List[Set[Nonterminal]]] = \
            [[set() for _ in range(n - i)] for i in range(n)]

        for i in range(n):
            T_i1 = table[0][i]
            for src, prod in cnf.productions():
                if len(prod) == 1 and \
                        word[i] == typing.cast(CFG.Symbols, prod)[0]:
                    T_i1.add(src)

        for j in range(2, n + 1):
            for i in range(n - j + 1):
                T_ij = table[j - 1][i]
                for k in range(1, j):
                    for src, prod in cnf.productions():
                        if len(prod) == 2:
                            prod = typing.cast(CFG.Symbols, prod)
                            if prod[0] in table[k - 1][i] and \
                                    prod[1] in table[j - k - 1][i + k]:
                                T_ij.add(src)

        return GeneratesResult(cnf.init in table[n - 1][0], cnf, table)

    def to_string(self) -> str:
        # put initial state first
        nonterms = sorted(self.nonterminals, key=lambda x: (x != self.init, x))
        out = []

        for r in nonterms:
            if r not in self.rules:
                continue
            to = sorted(map(lambda prds: "".join(map(lambda x: x.name, prds))
                                         if prds else "ε", self.rules[r]))
            out.append(f"{r.name} -> {' | '.join(to)}")

        return "\n".join(out)

    @staticmethod
    def all_terminal(sentence: CFG.Sentence) -> bool:
        return all_of(lambda x: isinstance(x, Terminal), sentence)

    def _recursive_nonterminals(self) -> Set[Nonterminal]:
        rewritable_to: Dict[Nonterminal, Set[Nonterminal]] \
            = {n: set() for n in self.nonterminals}
        for tracker in ChangeTracker():
            for src, prod in self.productions():
                for sym in prod:
                    if not isinstance(sym, Nonterminal):
                        continue
                    if sym not in rewritable_to[src]:
                        rewritable_to[src].add(sym)
                        tracker.changed()
                    for tgt in rewritable_to[sym]:
                        if tgt not in rewritable_to[src]:
                            rewritable_to[src].add(tgt)
                            tracker.changed()

        return {n for n in self.nonterminals if n in rewritable_to[n]}

    @staticmethod
    def _terminal_sequence_to_str(seq: Optional[Iterable[Terminal]]) \
            -> Optional[str]:
        if seq is None:
            return None
        out = "".join(map(lambda x: x.name, seq))
        if len(out) == 0:
            return "ε"
        return out

    @staticmethod
    def is_equivalent_test(left_ : CFG, right_ : CFG,
                           full_cmp_cnt: Optional[int] = None,
                           max_cmp_len: Optional[int] = None,
                           random_samples: int = 1000
                           ) -> IsEquivalentResult:
        left = left_.proper()
        right = right_.proper()

        left_gen = WordGenerator(left)
        right_gen = WordGenerator(right)

        left_ce: Optional[CFG.Word] = None
        right_ce: Optional[CFG.Word] = None

        def mkres() -> IsEquivalentResult:
            return IsEquivalentResult(CFG._terminal_sequence_to_str(left_ce),
                                      CFG._terminal_sequence_to_str(right_ce))

        def may_pop(words: Set[CFG.Word]) -> Optional[CFG.Word]:
            if words:
                return words.pop()
            return None

        def fill_ces(left_words: Set[CFG.Word], right_words: Set[CFG.Word]) \
                -> None:
            nonlocal left_ce
            nonlocal right_ce
            if left_ce is None:
                left_ce = may_pop(left_words - right_words)
            if right_ce is None:
                right_ce = may_pop(right_words - left_words)

        def try_word(maybe_ce: Optional[CFG.Word], rng: WordGenerator,
                     other: CachedCYK, length: int) -> Optional[CFG.Word]:
            if maybe_ce is not None:
                return maybe_ce

            word = rng.rnd_word(length)
            if word is None or other.generates(word):
                return None
            return word

        if full_cmp_cnt is None:
            full_cmp_cnt = pow(2, 16)

        if max_cmp_len is None:
            max_cmp_len = min(max(pow(2, len(left.nonterminals) + 1),
                                  pow(2, len(right.nonterminals) + 1)),
                              16)

        last_checked_len = 0
        if full_cmp_cnt > 0:
            Lenmap = Dict[int, Set[CFG.Word]]
            left_words: Lenmap = dict()
            right_words: Lenmap = dict()
            last_min_size = 0

            def total(words: Dict[int, Set[CFG.Word]]) -> int:
                return sum(len(x) for k, x in words.items()
                           if k <= last_min_size)

            def nxderivations(words: Dict[int, Set[CFG.Word]],
                              rng: WordGenerator, new_size: int) -> int:
                return total(words) + \
                    sum(rng.derivations_count(l) for l in
                        range(last_min_size + 1, new_size + 1))

            while True:
                lword = left_gen.get_and_shift()
                rword = right_gen.get_and_shift()

                if lword is not None:
                    left_words.setdefault(len(lword), set()).add(lword)
                if rword is not None:
                    right_words.setdefault(len(rword), set()).add(rword)

                min_size = min((len(w) for w in [lword, rword]
                                if w is not None),
                               default=last_min_size + 1)
                if last_min_size < min_size:
                    for sz in range(last_min_size, min_size):
                        fill_ces(left_words.get(sz, set()),
                                 right_words.get(sz, set()))
                        last_checked_len = sz
                    nxl = nxderivations(left_words, left_gen, min_size)
                    nxr = nxderivations(right_words, right_gen, min_size)

                    last_min_size = min_size

                    if max(nxl, nxr) > full_cmp_cnt or min_size > max_cmp_len:
                        break

                # produce counterexample even if it is only partial
                if left_ce is not None or right_ce is not None:
                    return mkres()

        # produce counterexample even if it is only partial
        if left_ce is not None or right_ce is not None:
            return mkres()

        left_cyk = CachedCYK(left)
        right_cyk = CachedCYK(right)

        for length in range(last_checked_len + 1, max_cmp_len + 1):
            for _ in range(random_samples):
                left_ce = try_word(left_ce, left_gen, right_cyk, length)
                right_ce = try_word(right_ce, right_gen, left_cyk, length)
                if left_ce is not None and right_ce is not None:
                    return mkres()

        return mkres()

    def __str__(self) -> str:
        return self.to_string()


# TODO: split into word counter, generator and random generator
class WordGenerator:

    CountMap = Dict[Nonterminal, int]
    ProdCountMap = Dict[CFG.Production, int]

    __slots__ = ["cfg", "_seen", "_stack", "_cur_lenght", "_next_length", "last", "counts", "prod_counts"]

    def __init__(self, cfg: CFG):
        self.cfg = cfg.proper()
        self._seen: Set[CFG.Symbols] = set()
        self._stack: List[CFG.Symbols] = [(self.cfg.init,)]
        self._cur_lenght = 1
        self._next_length: Optional[int] = 2
        self.last: Optional[CFG.Word] = None \
            if Eps() not in self.cfg.rules.get(self.cfg.init, []) \
            else ()

        # init and productions of lenght 0
        self.counts: List[WordGenerator.CountMap] \
            = [{n: 0 for n in self.cfg.nonterminals},
               {n: 0 for n in self.cfg.nonterminals}]
        if Eps() in self.cfg.rules.get(self.cfg.init, []):
            self.counts[0][self.cfg.init] = 1
        self.prod_counts: List[WordGenerator.ProdCountMap] = [dict(), dict()]

        for src, prod in self.cfg.productions():
            if len(prod) == 1:  # No A -> B (siple) rules
                self.counts[1][src] += 1

    def get_and_shift(self) -> Optional[CFG.Word]:
        """
        Generates all words of the grammar in ascending order, or random words
        of given length.
        No words are repeated.
        """
        word = self.last if self.last is not None else self._next()
        self.last = None
        return word

    def get(self) -> Optional[CFG.Word]:
        if self.last is None:
            return self._next()
        return self.last

    def _next(self) -> Optional[CFG.Word]:
        # Iterative-Deepening pseudo DFS (as a generator)
        while self._stack or self._next_length is not None:
            while self._stack:
                sentence = self._stack.pop()

                if CFG.all_terminal(sentence):
                    if len(sentence) == self._cur_lenght:
                        self.last = typing.cast(CFG.Word, sentence)
                        return self.last
                    continue

                for i in range(len(sentence)):
                    if isinstance(sentence[i], Nonterminal):
                        for p in self.cfg.rules.get(
                                typing.cast(Nonterminal, sentence[i]), []):
                            if isinstance(p, Eps):
                                continue
                            new_sentence = sentence[:i] + p + sentence[i + 1:]
                            nlen = len(new_sentence)

                            # prune seen and too long
                            if new_sentence not in self._seen \
                                    and nlen <= self._cur_lenght:
                                self._seen.add(new_sentence)
                                self._stack.append(new_sentence)  # recursion
                            elif nlen > self._cur_lenght \
                                    and (self._next_length is None
                                         or nlen < self._next_length):
                                self._next_length = nlen
                        break  # it suffices to perform left derivations

            if self._next_length is not None \
                    and self._next_length > self._cur_lenght:
                self._cur_lenght = self._next_length
                self._next_length = None
                self._stack = [(self.cfg.init,)]
                self._seen = set()
                self._materialize(self._cur_lenght)

        return None

    def derivations_count(self, length: int,
                          nterm: Optional[Nonterminal] = None) -> int:
        self._materialize(length)
        if nterm is None:
            nterm = self.cfg.init
        return self.counts[length][nterm]

    def _materialize(self, length: int) -> None:
        if len(self.counts) > length:
            return
        for l in range(len(self.counts), length):
            self._materialize(l)

        self.counts.append({n: 0 for n in self.cfg.nonterminals})

        for src, prod in self.cfg.productions():
            count = self._materialize_prod_count(prod, length)
            self.counts[length][src] += count

    def _materialize_prod_count(self, prod: CFG.Production,
                                length: int, prefix="") -> int:
        """Assumes smaller length are already computed"""

        if isinstance(prod, Eps):
            return Eps() in self.cfg.rules.get(self.cfg.init, [])

        if len(prod) == 1:
            if isinstance(prod[0], Terminal):
                return int(length == 1)
            return self.counts[length][prod[0]]

        if CFG.all_terminal(prod):
            return len(prod) == length

        assert len(self.prod_counts) >= length, \
            "smaller production lengths must be already computed"

        if len(self.prod_counts) == length:
            self.prod_counts.append(dict())

        if prod in self.prod_counts[length]:
            return self.prod_counts[length][prod]

        # for N -> γ to get number of words of lenght l
        # consider split γ = αβ such that |α| = 1
        # and all lenght a, b. a + b = l such that words of lenght a are
        # derived from α and words of lenght b are derived from β
        count = 0
        alpha = prod[:1]
        beta = prod[1:]
        for a in range(1, length):  # end is exclusive
            b = length - a
            cnt_alpha = self._materialize_prod_count(alpha, a, f"{prefix}    ")
            if cnt_alpha == 0:
                continue
            cnt_beta = self._materialize_prod_count(beta, b, f"{prefix}    ")
            count += cnt_alpha * cnt_beta

        self.prod_counts[length][prod] = count
        return count

    def rnd_word(self, length: int) -> Optional[CFG.Word]:
        """
        Returns a random word uniformly selected from all the derivations of
        given length or None if shuch a word does not exist.
        """
        if self.derivations_count(length) == 0:
            return None

        if length == 0:
            return None \
                if Eps() not in self.cfg.rules.get(self.cfg.init, []) \
                else ()

        sentence: CFG.Symbols = (self.cfg.init,)
        while not CFG.all_terminal(sentence):
            for i in range(len(sentence)):
                sym = sentence[i]
                if not isinstance(sym, Nonterminal):
                    continue
                break  # left derivations only

            candidates: List[CFG.Symbols] = []
            weights: List[int] = []

            assert isinstance(sym, Nonterminal)
            for prod in self.cfg.rules[sym]:
                suff = tuple(prod) + sentence[i + 1:]
                # note: no nonterminals before i ~> the weight of the whole
                # candidate is the same as the weight of its suffix starting at
                # position i
                w = self._materialize_prod_count(suff, length - i)
                if w > 0:
                    cand = sentence[:i] + suff
                    candidates.append(cand)
                    weights.append(w)

            sentence = random.choices(candidates, weights=weights)[0]
        return typing.cast(CFG.Word, sentence)


class CachedCYK:

    __slots__ = ["cfg", "cache"]

    def __init__(self, cfg: CFG):
        self.cfg = cfg.cnf()
        self.cache: Dict[CFG.Word, Set[Nonterminal]] = dict()
        for src, dst in self.cfg.productions():
            if len(dst) <= 1:
                dst = typing.cast(CFG.Word, dst)
                self.cache.setdefault(dst, set()).add(src)

    def generates(self, word: Union[str, Iterable[Terminal]]) -> bool:
        if isinstance(word, str):
            word = tuple(Terminal(x) for x in word)
        else:
            word = tuple(word)
        return self.cfg.init in self._generates(word)

    def _generates(self, word: CFG.Word) -> Set[Nonterminal]:
        if word in self.cache:
            return self.cache[word]

        out: Set[Nonterminal] = set()
        for i in range(1, len(word)):
            alpha = word[:i]
            beta = word[i:]
            alpha_nterms = self._generates(alpha)
            if len(alpha_nterms) == 0:
                continue
            beta_nterms = self._generates(beta)
            if len(beta_nterms) == 0:
                continue
            for src, dst in self.cfg.productions():
                if len(dst) != 2:
                    continue
                dst = typing.cast(CFG.Symbols, dst)
                if dst[0] in alpha_nterms and dst[1] in beta_nterms:
                    out.add(src)

        self.cache[word] = out
        return out
