from __future__ import annotations
from typing import Set, Dict, Union, Tuple
from lib.nfa import NFA
from lib.common import State, Character, Eps, Emptyset
from enum import Enum
import enum


class Bin(Enum):
    Union = enum.auto()
    Concat = enum.auto()


class Iter(Enum):
    Iteration = enum.auto()
    Positive = enum.auto()


class AST(object):
    def astprint(self) -> str:
        pass


class BinOp(AST):
    def __init__(self, left: AST, op: Bin, right: AST):
        self.left = left
        self.right = right
        self.token = op

    def __eq__(self, other):
        if isinstance(other, BinOp):
            return other.token == self.token and other.left == self.left and other.right == self.right
        return False

    def __hash__(self):
        return hash((self.token, self.left, self.right))

    def astprint(self):
        if self.token == Bin.Union:
            return f"({self.left.astprint()} + {self.right.astprint()})"
        else:
            return f"{self.left.astprint()}.{self.right.astprint()}"


class IterOp(AST):
    def __init__(self, node: AST, op: Iter):
        self.node = node
        self.token = op

    def __eq__(self, other):
        if isinstance(other, IterOp):
            return other.token == self.token and other.node == self.node
        return False

    def __hash__(self):
        return hash((self.token, self.node))

    def astprint(self):
        # If we want to print positive iterations (a^+) with asterisk (a.a^*), just uncomment this:
        # positive = self.node.astprint() + "." if self.token == Iter.Positive else ""
        # return positive + self.node.astprint() + "^*"

        sign = "^*" if self.token == Iter.Iteration else "^+"
        return self.node.astprint() + sign


class CharNode(AST):
    def __init__(self, token: Union[Character, Eps, Emptyset]):
        self.token = token

    def __eq__(self, other):
        if isinstance(other, CharNode):
            return other.token == self.token
        return False

    def __hash__(self):
        return hash(self.token)

    def astprint(self):
        return self.token.name

class RegEx:
    Transition = Dict[Tuple[State, AST], Set[State]]

    def __init__(self, characters: Set[Character], expression: AST):
        self.characters = characters
        self.expression = expression

    def regex_to_efa(self) -> NFA:
        init = State("init_state")
        final = State("final_state")
        states: Set[State] = {init, final}
        transition: RegEx.Transition = dict()
        transition[init, self.expression] = {final}

        new_name = 0
        while self.has_ast(transition):
            changed = False
            for (state, ast) in transition:
                for dest_state in transition[state, ast]:

                    if type(ast) == BinOp:
                        if ast.token == Bin.Union:
                            self.transition_add(transition, state, ast.left, dest_state)
                            self.transition_add(transition, state, ast.right, dest_state)
                            changed = True

                        elif ast.token == Bin.Concat:
                            if ast.left.token != Emptyset() and ast.right.token != Emptyset():
                                new_state = State(str(new_name))
                                states.add(new_state)
                                new_name += 1
                                self.transition_add(transition, state, ast.left, new_state)
                                self.transition_add(transition, new_state, ast.right, dest_state)
                            changed = True

                    elif type(ast) == IterOp:
                        if ast.node == Emptyset():
                            if ast.token == Iter.Iteration:
                                self.transition_add(transition, state, CharNode(Eps()), dest_state)
                        else:
                            new_state = State(str(new_name))
                            states.add(new_state)
                            new_name += 1
                            incoming = ast.node if ast.token == Iter.Positive else CharNode(Eps())
                            self.transition_add(transition, state, incoming,  new_state)
                            self.transition_add(transition, new_state, ast.node,  new_state)
                            self.transition_add(transition, new_state, CharNode(Eps()),  dest_state)
                        changed = True

                if changed:
                    del transition[state, ast]
                    break

        new_transition: NFA.Transition = dict()
        for (state, char) in transition:
            if type(char.token) != Emptyset:
                new_transition[state, char.token] = transition[state, char]

        return NFA(states, self.characters, new_transition, init, {final})

    def transition_add(self, transition, state, ast, dest_state):
        if ast.token == Emptyset():
            return

        transition.setdefault((state, ast), set()).add(dest_state)

    def has_ast(self, transition: RegEx.Transition):
        for (state, ast) in transition:
            if type(ast) != CharNode:
                return True
        return False

    # implementation only - to see how transition function with ASTs changes
    def transprint(self, transition):
        print("TRANSITION")
        for state, ast in transition:
            print("KEY", state.name, ast.astprint(), end=': {')
            for dest in transition[state, ast]:
                print(dest.name, end=' ')
            print("}")

