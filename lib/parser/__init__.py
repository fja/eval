from typing import List, Tuple, Union, Set, Any
from copy import deepcopy
from lib.common import State, Character, Eps, Terminal, Nonterminal, Emptyset
from lib.reg import DFA, NFA, RegGrammar
from lib.grammars_cfg import CFG
from lib.regex import RegEx, AST, Bin, Iter, BinOp, IterOp, CharNode
import antlr4  # type: ignore
from antlr4.error.ErrorListener import ErrorListener
from lib.parser.DFALexer import DFALexer
from lib.parser.DFAParser import DFAParser
from lib.parser.DFAListener import DFAListener
from lib.parser.NFALexer import NFALexer
from lib.parser.NFAParser import NFAParser
from lib.parser.NFAListener import NFAListener
from lib.parser.RegExLexer import RegExLexer
from lib.parser.RegExParser import RegExParser
from lib.parser.RegExVisitor import RegExVisitor
from lib.parser.CFGLexer import CFGLexer
from lib.parser.CFGParser import CFGParser
from lib.parser.CFGListener import CFGListener
from lib.parser.RegGLexer import RegGLexer
from lib.parser.RegGParser import RegGParser
from lib.parser.RegGListener import RegGListener


class ParsingError(Exception):
    def __init__(self, args):
        self.args = args


# This is needed because antlr is too smart and parse at least something
# possible even when input formalism and given type don't match. This way it
# aborts on any parsing problem.
class ErrorShouter(ErrorListener):
    def syntaxError(self, recognizer: Any, offendingSymbol: Any, line: int,
                    column: int, msg: str, e: Any) -> None:
        raise Exception(
            f"ERROR: when parsing line {line} column {column}: {msg}")


def _names_to_str(collection: Union[Set[State], Set[Character],
                  Set[Terminal], Set[Nonterminal]]) -> str:
    return "{" + ','.join(set(map(lambda x: x.name, collection))) + "}"


def _rules_to_str(rules: Union[CFG.Rules, RegGrammar.Rules],
                  nonterminals: List[Nonterminal]) -> str:
    out = ""
    for nonterminal in nonterminals:
        if nonterminal not in rules:
            continue
        rewritten = ' | '.join(set(map(lambda x:
                                       _rewrite_variant(x),
                                       rules[nonterminal])))
        out += f"{nonterminal.name} -> {rewritten}\n"
    return out[:-1]


def _rewrite_variant(variant: Union[Eps, Terminal,
                     Tuple[Union[Terminal, Nonterminal], ...]]) -> str:
    if isinstance(variant, Tuple):
        return ''.join(map(lambda x: x.name, variant))
    return variant.name


def _unquote(txt: str) -> str:
    assert txt[0] == '"'
    assert txt[-1] == '"'
    return txt[1:-1]


def dfa_to_str(dfa: DFA, full: bool = False) -> str:
    transition = ""
    for key, dest_state in sorted(dfa.transition.items()):
        state_1, character = key
        transition += f"({state_1.name},{character.name})={dest_state.name} "

    init = f"init={dfa.init.name}"
    final = f"final={_names_to_str(dfa.final)}"

    # full - verbose description of DFA - only for development, dismiss later
    if full:
        return f"DFA = ({_names_to_str(dfa.states)}, " \
               f"{_names_to_str(dfa.characters)}, " \
               f"d, {init}, {final})\n{transition}"
    return f"{init} {transition} {final}"


def reggrammar_to_str(reg: RegGrammar, full: bool = False) -> str:
    nonterminals = deepcopy(reg.nonterminals).difference({reg.init})
    nonterminals = [reg.init] + list(nonterminals)
    rules = _rules_to_str(reg.rules, nonterminals)
    if not full:
        return rules

    # full - verbose description of DFA - only for development, dismiss later
    nonterminals_names = _names_to_str(reg.nonterminals)
    terminals = _names_to_str(reg.terminals)
    return f"Grammar: ({nonterminals_names}, {terminals}, P, " \
           f"{reg.init.name})\n{_rules_to_str(reg.rules, nonterminals)}"


def cfg_to_str(gra: CFG, full: bool = False) -> str:
    nonterminals = deepcopy(gra.nonterminals).difference({gra.init})
    nonterminals = [gra.init] + list(nonterminals)
    rules = _rules_to_str(gra.rules, nonterminals)
    if not full:
        return rules

    # full - verbose description of DFA - only for development, dismiss later
    nonterminals_names = _names_to_str(gra.nonterminals)
    terminals = _names_to_str(gra.terminals)
    return f"Grammar: ({nonterminals_names}, {terminals}, P, " \
           f"{gra.init.name})\n{_rules_to_str(gra.rules, nonterminals)}"


def nfa_to_str(nfa: NFA, full: bool = False) -> str:
    transition = ""
    for key, set_states in sorted(nfa.transition.items()):
        state, character = key
        dest_states = nfa.transition[state, character]
        transition += f"({state.name},{character.name})=" \
            f"{_names_to_str(dest_states)} "

    init = f"init={nfa.init.name}"
    final = f"final={_names_to_str(nfa.final)}"

    if full:
        return f"NFA = ({_names_to_str(nfa.states)}, " \
               f"{_names_to_str(nfa.characters)}, " \
               f"d, {init}, {final})\n{transition}"
    return f"{init} {transition} {final}"


def regex_to_str(reg: RegEx) -> str:
    return reg.expression.astprint()


def _common_parse(string: str, given_lexer: Any, given_parser: Any,
                  given_builder: Any) -> Any:
    error_listener = ErrorShouter()
    chars = antlr4.InputStream(string)
    lexer = given_lexer(chars)
    lexer.addErrorListener(error_listener)
    tokens = antlr4.CommonTokenStream(lexer)
    parser = given_parser(tokens)
    parser.addErrorListener(error_listener)

    tree = parser.start()
    builder = given_builder()
    walker = antlr4.ParseTreeWalker()
    walker.walk(builder, tree)
    return builder


def cfg(string: str) -> CFG:
    try:
        builder = _common_parse(string, CFGLexer, CFGParser, CFGBuilder)
        return CFG(builder.nonterminals, builder.terminals, builder.rules,
                   builder.init)

    except Exception as e:
        raise ParsingError(e.args)


def reggrammar(string: str) -> RegGrammar:
    try:
        builder = _common_parse(string, RegGLexer, RegGParser, RegGBuilder)
        return RegGrammar(builder.nonterminals, builder.terminals,
                          builder.rules, builder.init)

    except Exception as e:
        raise ParsingError(e.args)


def dfa(string: str) -> DFA:
    try:
        builder = _common_parse(string, DFALexer, DFAParser, DFABuilder)

        if builder.init is None:
            builder.init = builder.first_state
            if builder.init is None:
                raise ParsingError(
                    "Automat musí obsahovat alespoň jeden stav.")

        dfa = DFA(builder.states, builder.characters, builder.transition,
                  builder.init, builder.final)
        return dfa

    except Exception as e:
        raise ParsingError(e.args)


def nfa(string: str) -> NFA:
    try:
        builder = _common_parse(string, NFALexer, NFAParser, NFABuilder)

        if builder.init is None:
            builder.init = builder.first_state
            if builder.init is None:
                raise ParsingError(
                    "Automat musí obsahovat alespoň jeden stav.")

        return NFA(builder.states, builder.characters, builder.transition,
                   builder.init, builder.final)

    except Exception as e:
        raise ParsingError(e.args)


def regex(string: str) -> RegEx:
    try:
        error_listener = ErrorShouter()
        chars = antlr4.InputStream(string)
        lexer = RegExLexer(chars)
        lexer.addErrorListener(error_listener)
        tokens = antlr4.CommonTokenStream(lexer)
        parser = RegExParser(tokens)
        parser.addErrorListener(error_listener)

        tree = parser.start()
        ast = RegExBuilder()
        ast.visitStart(tree)

        return RegEx(ast.characters, ast.expression)

    except Exception as e:
        raise ParsingError(e.args)


class CommonFABuilder(object):

    def visitStatename(self, ctx: Any) -> str:
        if ctx.QUOTE():
            return _unquote(ctx.getText())
        return ctx.getText()

    def exitInit(self, ctx: Any) -> None:
        if ctx.statename() is not None:
            state = State(self.visitStatename(ctx.statename()))
            self.init = state
            self.states.add(state)

    def exitFinal(self, ctx: Any) -> None:
        for stctx in ctx.stateset().statename():
            state = State(self.visitStatename(stctx))
            self.states.add(state)
            self.final.add(state)

    def exitComment(self, ctx: Any) -> None:
        pass


# The order of inheritance is important – we need the ‹exit*› functions from
# ‹CommonFABuilder› to precede the empty versions from ‹DFAListener›
class DFABuilder(CommonFABuilder, DFAListener):

    def __init__(self) -> None:
        self.states = set()
        self.characters = set()
        self.transition = {}
        self.init = None
        self.first_state = None
        self.final = set()

    def exitProduction(self, ctx: Any) -> None:
        state = State(self.visitStatename(ctx.statename(0)))
        character = Character(self.visitStatename(ctx.statename(1)))
        dest_state = State(self.visitStatename(ctx.statename(2)))
        self.states.add(state)
        self.states.add(dest_state)
        self.characters.add(character)

        if (state, character) in self.transition:
            print("Upozornění: v textovém zápisu se objevilo více přechodů "
                  f"pro stejnou dvojici ({state.name}, {character.name}).")
        self.transition[state, character] = dest_state

        if self.first_state is None:
            self.first_state = state


class NFABuilder(CommonFABuilder, NFAListener):

    def __init__(self) -> None:
        self.states = set()
        self.characters = set()
        self.transition = {}
        self.init = None
        self.first_state = None
        self.final = set()
        self.efa = False

    def exitProduction(self, ctx: Any) -> None:
        state = State(self.visitStatename(ctx.statename(0)))
        self.states.add(state)
        dest_states = set()
        for stctx in ctx.stateset().statename():
            dest_state = State(self.visitStatename(stctx))
            self.states.add(dest_state)
            dest_states.add(dest_state)

        if ctx.EPSILON():
            if (state, Eps()) in self.transition:
                print("Upozornění: v textovém zápisu se objevilo více "
                      f"přechodů pro stejnou dvojici ({state.name}, ε).")
            self.transition[state, Eps()] = dest_states
            self.efa = True
        else:
            character = Character(self.visitStatename(ctx.statename(1)))
            self.characters.add(character)
            if (state, character) in self.transition:
                print("Upozornění: v textovém zápisu se objevilo více "
                      f"přechodů pro stejnou dvojici ({state.name}, "
                      f"{character.name}).")
            self.transition[state, character] = dest_states

        if self.first_state is None:
            self.first_state = state


class RegExBuilder(RegExVisitor):

    def __init__(self) -> None:
        self.characters: Set[Character] = set()
        self.expression: AST

    def visitStart(self, ctx: Any) -> None:
        self.expression = self.visitExpr(ctx.expr())

    def visitExpr(self, ctx: Any) -> None:
        # Binary operation: union or explicit concatenation
        if ctx.UNION() or ctx.CONCAT():
            op = Bin.Union if ctx.UNION() is not None else Bin.Concat
            return BinOp(self.visitExpr(ctx.expr(0)), op,
                         self.visitExpr(ctx.expr(1)))

        # Implicit concatenation of (iterated) symbols or expressions in
        # parentheses
        expressions = list(map(lambda x:
                               self.visitConcatenable(x), ctx.concatenated()))
        return self.implicit_concat(expressions)

    def visitConcatenable(self, ctx: Any) -> None:
        if ctx.symbol():
            return self.visitSymbol(ctx.symbol())

        elif ctx.iterable():
            if ctx.ITER():
                return self.visitIterable(ctx.iterable())

            elif ctx.POS_ITER():
                return self.visitIterable(ctx.iterable(), True)

        elif ctx.parentheses():
            return self.visitParentheses(ctx.parentheses())

    def visitSymbol(self, ctx: Any) -> None:
        if ctx.ALPHABET():
            self.characters.add(Character(str(ctx.ALPHABET())))
            return CharNode(Character(str(ctx.ALPHABET())))

        elif ctx.EPSILON():
            return CharNode(Eps())

        elif ctx.EMPTYSET():
            return CharNode(Emptyset())

        elif ctx.QUOTE():
            char = Character(_unquote(self.getText()))
            self.characters.add(char)
            return CharNode(char)

    def visitParentheses(self, ctx: Any) -> None:
        return self.visitExpr(ctx.expr())

    def visitIterable(self, ctx: Any, positive: bool = False) -> None:
        if ctx.symbol():
            expression = self.visitSymbol(ctx.symbol())

        elif ctx.parentheses():
            expression = self.visitParentheses(ctx.parentheses())

        return IterOp(expression, Iter.Positive) if positive \
            else IterOp(expression, Iter.Iteration)

    def implicit_concat(self, to_concat: Any) -> None:
        ast = to_concat[0]
        if len(to_concat) > 1:
            for expression in to_concat[1:]:
                ast = BinOp(ast, Bin.Concat, expression)
        return ast

    # for future support of comments
    def exitComment(self, ctx: Any) -> None:
        return None


class CommonGrammarBuilder(object):
    def __init__(self) -> None:
        self.terminals = set()
        self.nonterminals = set()
        self.rules = {}
        self.init = None

    def visitSymbol(self, ctx: Any) -> None:
        if ctx.TERMINAL():
            return str(ctx.TERMINAL())
        elif ctx.CAPS():
            return str(ctx.CAPS())
        else:
            return '_'

    def visitNonterm(self, ctx: Any) -> None:
        if ctx.CAPS():
            name = str(ctx.CAPS())
        elif ctx.LEFT_ANGLE():
            name = '<' + ''.join(map(lambda x: self.visitSymbol(x),
                                     ctx.symbol())) + '>'
            if ctx.APOSTROPHE():
                name = name + len(ctx.APOSTROPHE()) * "'"
        elif ctx.APOSTROPHE():
            name = self.visitSymbol(ctx.symbol(0)) \
                + len(ctx.APOSTROPHE()) * "'"

        nonterminal = Nonterminal(name)
        self.nonterminals.add(nonterminal)
        return nonterminal

    def visitTerm(self, term_ctx: Any) -> None:
        if term_ctx.TERMINAL():
            name = str(term_ctx.TERMINAL())
        elif term_ctx.QUOTE():
            name = _unquote(term_ctx.getText())

        terminal = Terminal(name)
        self.terminals.add(terminal)
        return terminal

    def exitOnerule(self, ctx: Any) -> None:
        nonterminal = self.visitNonterm(ctx.nonterm())
        self.nonterminals.add(nonterminal)
        if self.init is None:
            self.init = nonterminal

        # multiple lines for one nonterminal are possible this way
        if nonterminal not in self.rules:
            self.rules[nonterminal] = set()

        for subctx in ctx.rewrite():
            self.rules[nonterminal].add(self.visitRewrite(subctx))

    # for future support of comments
    def exitComment(self, ctx: Any) -> None:
        return None


class CFGBuilder(CommonGrammarBuilder, CFGListener):

    def visitRewrite(self, ctx: Any) \
            -> Union[Eps, Tuple[Union[Terminal, Nonterminal], ...]]:
        if ctx.EPSILON():
            return Eps()
        sequence = []
        for subctx in ctx.term_or_nonterm():
            if subctx.term():
                sequence.append(self.visitTerm(subctx.term()))
            else:
                sequence.append(self.visitNonterm(subctx.nonterm()))

        return tuple(sequence)


class RegGBuilder(CommonGrammarBuilder, RegGListener):

    def visitRewrite(self, ctx: Any) -> None:
        if ctx.EPSILON():
            return Eps()
        assert ctx.term() is not None
        term = self.visitTerm(ctx.term())
        if ctx.nonterm():
            return tuple([term, self.visitNonterm(ctx.nonterm())])
        return term
