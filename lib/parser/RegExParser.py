# Generated from lib/parser/RegEx.g4 by ANTLR 4.9.2
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\17")
        buf.write("P\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b")
        buf.write("\t\b\4\t\t\t\3\2\3\2\5\2\25\n\2\3\3\3\3\6\3\31\n\3\r\3")
        buf.write("\16\3\32\3\3\3\3\3\3\3\3\3\3\3\3\7\3#\n\3\f\3\16\3&\13")
        buf.write("\3\3\4\3\4\3\4\3\4\3\4\5\4-\n\4\3\5\3\5\5\5\61\n\5\3\6")
        buf.write("\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\6\7<\n\7\r\7\16\7=\3")
        buf.write("\7\3\7\5\7B\n\7\3\b\3\b\7\bF\n\b\f\b\16\bI\13\b\3\b\5")
        buf.write("\bL\n\b\3\t\3\t\3\t\2\3\4\n\2\4\6\b\n\f\16\20\2\4\3\2")
        buf.write("\5\6\4\2\3\f\17\17\2T\2\24\3\2\2\2\4\26\3\2\2\2\6,\3\2")
        buf.write("\2\2\b\60\3\2\2\2\n\62\3\2\2\2\fA\3\2\2\2\16K\3\2\2\2")
        buf.write("\20M\3\2\2\2\22\25\5\4\3\2\23\25\5\16\b\2\24\22\3\2\2")
        buf.write("\2\24\23\3\2\2\2\25\3\3\2\2\2\26\30\b\3\1\2\27\31\5\6")
        buf.write("\4\2\30\27\3\2\2\2\31\32\3\2\2\2\32\30\3\2\2\2\32\33\3")
        buf.write("\2\2\2\33$\3\2\2\2\34\35\f\4\2\2\35\36\7\7\2\2\36#\5\4")
        buf.write("\3\5\37 \f\3\2\2 !\7\b\2\2!#\5\4\3\4\"\34\3\2\2\2\"\37")
        buf.write("\3\2\2\2#&\3\2\2\2$\"\3\2\2\2$%\3\2\2\2%\5\3\2\2\2&$\3")
        buf.write("\2\2\2\'-\5\f\7\2()\5\b\5\2)*\t\2\2\2*-\3\2\2\2+-\5\n")
        buf.write("\6\2,\'\3\2\2\2,(\3\2\2\2,+\3\2\2\2-\7\3\2\2\2.\61\5\f")
        buf.write("\7\2/\61\5\n\6\2\60.\3\2\2\2\60/\3\2\2\2\61\t\3\2\2\2")
        buf.write("\62\63\7\3\2\2\63\64\5\4\3\2\64\65\7\4\2\2\65\13\3\2\2")
        buf.write("\2\66B\7\t\2\2\67B\7\n\2\28B\7\13\2\29;\7\r\2\2:<\5\20")
        buf.write("\t\2;:\3\2\2\2<=\3\2\2\2=;\3\2\2\2=>\3\2\2\2>?\3\2\2\2")
        buf.write("?@\7\r\2\2@B\3\2\2\2A\66\3\2\2\2A\67\3\2\2\2A8\3\2\2\2")
        buf.write("A9\3\2\2\2B\r\3\2\2\2CG\7\f\2\2DF\5\20\t\2ED\3\2\2\2F")
        buf.write("I\3\2\2\2GE\3\2\2\2GH\3\2\2\2HL\3\2\2\2IG\3\2\2\2JL\3")
        buf.write("\2\2\2KC\3\2\2\2KJ\3\2\2\2L\17\3\2\2\2MN\t\3\2\2N\21\3")
        buf.write("\2\2\2\f\24\32\"$,\60=AGK")
        return buf.getvalue()


class RegExParser ( Parser ):

    grammarFileName = "RegEx.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'('", "')'", "<INVALID>", "'^+'", "'.'", 
                     "'+'", "<INVALID>", "<INVALID>", "<INVALID>", "'#'", 
                     "'\"'" ]

    symbolicNames = [ "<INVALID>", "LEFT_PAR", "RIGHT_PAR", "ITER", "POS_ITER", 
                      "CONCAT", "UNION", "ALPHABET", "EPSILON", "EMPTYSET", 
                      "HASH", "QUOTE", "WS", "ANYCHAR" ]

    RULE_start = 0
    RULE_expr = 1
    RULE_concatenated = 2
    RULE_iterable = 3
    RULE_parentheses = 4
    RULE_symbol = 5
    RULE_comment = 6
    RULE_anyvalue = 7

    ruleNames =  [ "start", "expr", "concatenated", "iterable", "parentheses", 
                   "symbol", "comment", "anyvalue" ]

    EOF = Token.EOF
    LEFT_PAR=1
    RIGHT_PAR=2
    ITER=3
    POS_ITER=4
    CONCAT=5
    UNION=6
    ALPHABET=7
    EPSILON=8
    EMPTYSET=9
    HASH=10
    QUOTE=11
    WS=12
    ANYCHAR=13

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class StartContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expr(self):
            return self.getTypedRuleContext(RegExParser.ExprContext,0)


        def comment(self):
            return self.getTypedRuleContext(RegExParser.CommentContext,0)


        def getRuleIndex(self):
            return RegExParser.RULE_start

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStart" ):
                listener.enterStart(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStart" ):
                listener.exitStart(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStart" ):
                return visitor.visitStart(self)
            else:
                return visitor.visitChildren(self)




    def start(self):

        localctx = RegExParser.StartContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_start)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 18
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [RegExParser.LEFT_PAR, RegExParser.ALPHABET, RegExParser.EPSILON, RegExParser.EMPTYSET, RegExParser.QUOTE]:
                self.state = 16
                self.expr(0)
                pass
            elif token in [RegExParser.EOF, RegExParser.HASH]:
                self.state = 17
                self.comment()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExprContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def concatenated(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RegExParser.ConcatenatedContext)
            else:
                return self.getTypedRuleContext(RegExParser.ConcatenatedContext,i)


        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RegExParser.ExprContext)
            else:
                return self.getTypedRuleContext(RegExParser.ExprContext,i)


        def CONCAT(self):
            return self.getToken(RegExParser.CONCAT, 0)

        def UNION(self):
            return self.getToken(RegExParser.UNION, 0)

        def getRuleIndex(self):
            return RegExParser.RULE_expr

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpr" ):
                listener.enterExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpr" ):
                listener.exitExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpr" ):
                return visitor.visitExpr(self)
            else:
                return visitor.visitChildren(self)



    def expr(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = RegExParser.ExprContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 2
        self.enterRecursionRule(localctx, 2, self.RULE_expr, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 22 
            self._errHandler.sync(self)
            _alt = 1
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt == 1:
                    self.state = 21
                    self.concatenated()

                else:
                    raise NoViableAltException(self)
                self.state = 24 
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,1,self._ctx)

            self._ctx.stop = self._input.LT(-1)
            self.state = 34
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,3,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 32
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,2,self._ctx)
                    if la_ == 1:
                        localctx = RegExParser.ExprContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 26
                        if not self.precpred(self._ctx, 2):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                        self.state = 27
                        self.match(RegExParser.CONCAT)
                        self.state = 28
                        self.expr(3)
                        pass

                    elif la_ == 2:
                        localctx = RegExParser.ExprContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 29
                        if not self.precpred(self._ctx, 1):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 1)")
                        self.state = 30
                        self.match(RegExParser.UNION)
                        self.state = 31
                        self.expr(2)
                        pass

             
                self.state = 36
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,3,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class ConcatenatedContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def symbol(self):
            return self.getTypedRuleContext(RegExParser.SymbolContext,0)


        def parentheses(self):
            return self.getTypedRuleContext(RegExParser.ParenthesesContext,0)


        def iterable(self):
            return self.getTypedRuleContext(RegExParser.IterableContext,0)


        def ITER(self):
            return self.getToken(RegExParser.ITER, 0)

        def POS_ITER(self):
            return self.getToken(RegExParser.POS_ITER, 0)

        def getRuleIndex(self):
            return RegExParser.RULE_concatenated

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterConcatenated" ):
                listener.enterConcatenated(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitConcatenated" ):
                listener.exitConcatenated(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitConcatenated" ):
                return visitor.visitConcatenated(self)
            else:
                return visitor.visitChildren(self)




    def concatenated(self):

        localctx = RegExParser.ConcatenatedContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_concatenated)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 42
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,4,self._ctx)
            if la_ == 1:
                self.state = 37
                self.symbol()
                pass

            elif la_ == 2:
                self.state = 38
                self.iterable()
                self.state = 39
                _la = self._input.LA(1)
                if not(_la==RegExParser.ITER or _la==RegExParser.POS_ITER):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                pass

            elif la_ == 3:
                self.state = 41
                self.parentheses()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class IterableContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def symbol(self):
            return self.getTypedRuleContext(RegExParser.SymbolContext,0)


        def parentheses(self):
            return self.getTypedRuleContext(RegExParser.ParenthesesContext,0)


        def getRuleIndex(self):
            return RegExParser.RULE_iterable

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterIterable" ):
                listener.enterIterable(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitIterable" ):
                listener.exitIterable(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitIterable" ):
                return visitor.visitIterable(self)
            else:
                return visitor.visitChildren(self)




    def iterable(self):

        localctx = RegExParser.IterableContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_iterable)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 46
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [RegExParser.ALPHABET, RegExParser.EPSILON, RegExParser.EMPTYSET, RegExParser.QUOTE]:
                self.state = 44
                self.symbol()
                pass
            elif token in [RegExParser.LEFT_PAR]:
                self.state = 45
                self.parentheses()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ParenthesesContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LEFT_PAR(self):
            return self.getToken(RegExParser.LEFT_PAR, 0)

        def expr(self):
            return self.getTypedRuleContext(RegExParser.ExprContext,0)


        def RIGHT_PAR(self):
            return self.getToken(RegExParser.RIGHT_PAR, 0)

        def getRuleIndex(self):
            return RegExParser.RULE_parentheses

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterParentheses" ):
                listener.enterParentheses(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitParentheses" ):
                listener.exitParentheses(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitParentheses" ):
                return visitor.visitParentheses(self)
            else:
                return visitor.visitChildren(self)




    def parentheses(self):

        localctx = RegExParser.ParenthesesContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_parentheses)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 48
            self.match(RegExParser.LEFT_PAR)
            self.state = 49
            self.expr(0)
            self.state = 50
            self.match(RegExParser.RIGHT_PAR)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class SymbolContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ALPHABET(self):
            return self.getToken(RegExParser.ALPHABET, 0)

        def EPSILON(self):
            return self.getToken(RegExParser.EPSILON, 0)

        def EMPTYSET(self):
            return self.getToken(RegExParser.EMPTYSET, 0)

        def QUOTE(self, i:int=None):
            if i is None:
                return self.getTokens(RegExParser.QUOTE)
            else:
                return self.getToken(RegExParser.QUOTE, i)

        def anyvalue(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RegExParser.AnyvalueContext)
            else:
                return self.getTypedRuleContext(RegExParser.AnyvalueContext,i)


        def getRuleIndex(self):
            return RegExParser.RULE_symbol

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSymbol" ):
                listener.enterSymbol(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSymbol" ):
                listener.exitSymbol(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSymbol" ):
                return visitor.visitSymbol(self)
            else:
                return visitor.visitChildren(self)




    def symbol(self):

        localctx = RegExParser.SymbolContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_symbol)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 63
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [RegExParser.ALPHABET]:
                self.state = 52
                self.match(RegExParser.ALPHABET)
                pass
            elif token in [RegExParser.EPSILON]:
                self.state = 53
                self.match(RegExParser.EPSILON)
                pass
            elif token in [RegExParser.EMPTYSET]:
                self.state = 54
                self.match(RegExParser.EMPTYSET)
                pass
            elif token in [RegExParser.QUOTE]:
                self.state = 55
                self.match(RegExParser.QUOTE)
                self.state = 57 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 56
                    self.anyvalue()
                    self.state = 59 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << RegExParser.LEFT_PAR) | (1 << RegExParser.RIGHT_PAR) | (1 << RegExParser.ITER) | (1 << RegExParser.POS_ITER) | (1 << RegExParser.CONCAT) | (1 << RegExParser.UNION) | (1 << RegExParser.ALPHABET) | (1 << RegExParser.EPSILON) | (1 << RegExParser.EMPTYSET) | (1 << RegExParser.HASH) | (1 << RegExParser.ANYCHAR))) != 0)):
                        break

                self.state = 61
                self.match(RegExParser.QUOTE)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class CommentContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def HASH(self):
            return self.getToken(RegExParser.HASH, 0)

        def anyvalue(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RegExParser.AnyvalueContext)
            else:
                return self.getTypedRuleContext(RegExParser.AnyvalueContext,i)


        def getRuleIndex(self):
            return RegExParser.RULE_comment

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterComment" ):
                listener.enterComment(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitComment" ):
                listener.exitComment(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitComment" ):
                return visitor.visitComment(self)
            else:
                return visitor.visitChildren(self)




    def comment(self):

        localctx = RegExParser.CommentContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_comment)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 73
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [RegExParser.HASH]:
                self.state = 65
                self.match(RegExParser.HASH)
                self.state = 69
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << RegExParser.LEFT_PAR) | (1 << RegExParser.RIGHT_PAR) | (1 << RegExParser.ITER) | (1 << RegExParser.POS_ITER) | (1 << RegExParser.CONCAT) | (1 << RegExParser.UNION) | (1 << RegExParser.ALPHABET) | (1 << RegExParser.EPSILON) | (1 << RegExParser.EMPTYSET) | (1 << RegExParser.HASH) | (1 << RegExParser.ANYCHAR))) != 0):
                    self.state = 66
                    self.anyvalue()
                    self.state = 71
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                pass
            elif token in [RegExParser.EOF]:
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AnyvalueContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LEFT_PAR(self):
            return self.getToken(RegExParser.LEFT_PAR, 0)

        def RIGHT_PAR(self):
            return self.getToken(RegExParser.RIGHT_PAR, 0)

        def ITER(self):
            return self.getToken(RegExParser.ITER, 0)

        def POS_ITER(self):
            return self.getToken(RegExParser.POS_ITER, 0)

        def CONCAT(self):
            return self.getToken(RegExParser.CONCAT, 0)

        def UNION(self):
            return self.getToken(RegExParser.UNION, 0)

        def ALPHABET(self):
            return self.getToken(RegExParser.ALPHABET, 0)

        def EPSILON(self):
            return self.getToken(RegExParser.EPSILON, 0)

        def EMPTYSET(self):
            return self.getToken(RegExParser.EMPTYSET, 0)

        def ANYCHAR(self):
            return self.getToken(RegExParser.ANYCHAR, 0)

        def HASH(self):
            return self.getToken(RegExParser.HASH, 0)

        def getRuleIndex(self):
            return RegExParser.RULE_anyvalue

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAnyvalue" ):
                listener.enterAnyvalue(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAnyvalue" ):
                listener.exitAnyvalue(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAnyvalue" ):
                return visitor.visitAnyvalue(self)
            else:
                return visitor.visitChildren(self)




    def anyvalue(self):

        localctx = RegExParser.AnyvalueContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_anyvalue)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 75
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << RegExParser.LEFT_PAR) | (1 << RegExParser.RIGHT_PAR) | (1 << RegExParser.ITER) | (1 << RegExParser.POS_ITER) | (1 << RegExParser.CONCAT) | (1 << RegExParser.UNION) | (1 << RegExParser.ALPHABET) | (1 << RegExParser.EPSILON) | (1 << RegExParser.EMPTYSET) | (1 << RegExParser.HASH) | (1 << RegExParser.ANYCHAR))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[1] = self.expr_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def expr_sempred(self, localctx:ExprContext, predIndex:int):
            if predIndex == 0:
                return self.precpred(self._ctx, 2)
         

            if predIndex == 1:
                return self.precpred(self._ctx, 1)
         




