# Generated from lib/parser/NFA.g4 by ANTLR 4.9.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .NFAParser import NFAParser
else:
    from NFAParser import NFAParser

# This class defines a complete listener for a parse tree produced by NFAParser.
class NFAListener(ParseTreeListener):

    # Enter a parse tree produced by NFAParser#production.
    def enterProduction(self, ctx:NFAParser.ProductionContext):
        pass

    # Exit a parse tree produced by NFAParser#production.
    def exitProduction(self, ctx:NFAParser.ProductionContext):
        pass


    # Enter a parse tree produced by NFAParser#start.
    def enterStart(self, ctx:NFAParser.StartContext):
        pass

    # Exit a parse tree produced by NFAParser#start.
    def exitStart(self, ctx:NFAParser.StartContext):
        pass


    # Enter a parse tree produced by NFAParser#init.
    def enterInit(self, ctx:NFAParser.InitContext):
        pass

    # Exit a parse tree produced by NFAParser#init.
    def exitInit(self, ctx:NFAParser.InitContext):
        pass


    # Enter a parse tree produced by NFAParser#stateset.
    def enterStateset(self, ctx:NFAParser.StatesetContext):
        pass

    # Exit a parse tree produced by NFAParser#stateset.
    def exitStateset(self, ctx:NFAParser.StatesetContext):
        pass


    # Enter a parse tree produced by NFAParser#final.
    def enterFinal(self, ctx:NFAParser.FinalContext):
        pass

    # Exit a parse tree produced by NFAParser#final.
    def exitFinal(self, ctx:NFAParser.FinalContext):
        pass


    # Enter a parse tree produced by NFAParser#statename.
    def enterStatename(self, ctx:NFAParser.StatenameContext):
        pass

    # Exit a parse tree produced by NFAParser#statename.
    def exitStatename(self, ctx:NFAParser.StatenameContext):
        pass


    # Enter a parse tree produced by NFAParser#comment.
    def enterComment(self, ctx:NFAParser.CommentContext):
        pass

    # Exit a parse tree produced by NFAParser#comment.
    def exitComment(self, ctx:NFAParser.CommentContext):
        pass


    # Enter a parse tree produced by NFAParser#anyvalue.
    def enterAnyvalue(self, ctx:NFAParser.AnyvalueContext):
        pass

    # Exit a parse tree produced by NFAParser#anyvalue.
    def exitAnyvalue(self, ctx:NFAParser.AnyvalueContext):
        pass



del NFAParser