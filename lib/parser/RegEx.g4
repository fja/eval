grammar RegEx;

/* Parser Rules */

start:        (expr | comment <EOF>);

expr:         concatenated+
              | expr CONCAT expr
              | expr UNION expr;

concatenated: (symbol | (iterable (ITER | POS_ITER)) | parentheses);

iterable:     (symbol | parentheses);

parentheses:  LEFT_PAR expr RIGHT_PAR;

symbol :      (ALPHABET | EPSILON | EMPTYSET | QUOTE anyvalue+ QUOTE);

comment:      (HASH anyvalue* | );

anyvalue:     LEFT_PAR | RIGHT_PAR | ITER | POS_ITER | CONCAT| UNION | ALPHABET | EPSILON | EMPTYSET | ANYCHAR | HASH;

/* Lexer Rules */

/* Tokens */

LEFT_PAR : '(';
RIGHT_PAR: ')';
ITER     : ('*' | '^*');
POS_ITER : '^+';
CONCAT   : '.';
UNION    : '+';
ALPHABET : [a-zA-Z0-9];
EPSILON  : ('ε' | '\\''e');
EMPTYSET : ('∅' | '\\''0');
HASH     : '#';
QUOTE    : '"';

/* Characters to be ignored */
WS : [ \r\t\n]+ -> skip ;


ANYCHAR  : .;
