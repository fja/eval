# Generated from lib/parser/RegG.g4 by ANTLR 4.9.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .RegGParser import RegGParser
else:
    from RegGParser import RegGParser

# This class defines a complete generic visitor for a parse tree produced by RegGParser.

class RegGVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by RegGParser#rewrite.
    def visitRewrite(self, ctx:RegGParser.RewriteContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RegGParser#start.
    def visitStart(self, ctx:RegGParser.StartContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RegGParser#onerule.
    def visitOnerule(self, ctx:RegGParser.OneruleContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RegGParser#term_or_nonterm.
    def visitTerm_or_nonterm(self, ctx:RegGParser.Term_or_nontermContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RegGParser#term.
    def visitTerm(self, ctx:RegGParser.TermContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RegGParser#nonterm.
    def visitNonterm(self, ctx:RegGParser.NontermContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RegGParser#symbol.
    def visitSymbol(self, ctx:RegGParser.SymbolContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RegGParser#comment.
    def visitComment(self, ctx:RegGParser.CommentContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RegGParser#anyvalue.
    def visitAnyvalue(self, ctx:RegGParser.AnyvalueContext):
        return self.visitChildren(ctx)



del RegGParser