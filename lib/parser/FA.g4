grammar FA;

start:       init production* final comment;

init:        (INIT EQUALS statename | );

stateset:    LEFT_BRACKET (statename (COMMA statename)* | ) RIGHT_BRACKET;

final:       FINAL EQUALS stateset;

statename:   (INIT | FINAL | STATE | QUOTE anyvalue+ QUOTE);

comment:     (HASH (anyvalue | QUOTE)* | );

anyvalue:    INIT | EQUALS | LEFT_PARENTHESIS | RIGHT_PARENTHESIS | LEFT_BRACKET | RIGHT_BRACKET | COMMA | FINAL | EPSILON | STATE | HASH | ANYCHAR;

INIT              : 'init';
EQUALS            : '=';
LEFT_PARENTHESIS  : '(';
RIGHT_PARENTHESIS : ')';
LEFT_BRACKET      : '{';
RIGHT_BRACKET     : '}';
COMMA             : ',';
FINAL             : 'final';
EPSILON           : ('ε' | '\\''e');
STATE             : ([a-zA-Z0-9] | '_' | '-' | '\'' | '<' | '>' )+;
HASH              : '#';
QUOTE             : '"';

/* Characters to be ignored */
WS : [ \r\t\n]+ -> skip ;

ANYCHAR           : .;

/*
vim: ft=antlr
*/
