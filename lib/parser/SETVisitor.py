# Generated from lib/parser/SET.g4 by ANTLR 4.9.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .SETParser import SETParser
else:
    from SETParser import SETParser

# This class defines a complete generic visitor for a parse tree produced by SETParser.

class SETVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by SETParser#start.
    def visitStart(self, ctx:SETParser.StartContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SETParser#alph.
    def visitAlph(self, ctx:SETParser.AlphContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SETParser#expr.
    def visitExpr(self, ctx:SETParser.ExprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SETParser#unop.
    def visitUnop(self, ctx:SETParser.UnopContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SETParser#parentheses.
    def visitParentheses(self, ctx:SETParser.ParenthesesContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SETParser#atom_set.
    def visitAtom_set(self, ctx:SETParser.Atom_setContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SETParser#symbol.
    def visitSymbol(self, ctx:SETParser.SymbolContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SETParser#comment.
    def visitComment(self, ctx:SETParser.CommentContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SETParser#anyvalue.
    def visitAnyvalue(self, ctx:SETParser.AnyvalueContext):
        return self.visitChildren(ctx)



del SETParser