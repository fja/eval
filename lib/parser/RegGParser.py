# Generated from lib/parser/RegG.g4 by ANTLR 4.9.2
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\20")
        buf.write("q\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b")
        buf.write("\t\b\4\t\t\t\4\n\t\n\3\2\3\2\3\2\3\2\3\2\5\2\32\n\2\3")
        buf.write("\3\3\3\6\3\36\n\3\r\3\16\3\37\7\3\"\n\3\f\3\16\3%\13\3")
        buf.write("\3\3\3\3\6\3)\n\3\r\3\16\3*\3\3\5\3.\n\3\3\3\3\3\3\4\3")
        buf.write("\4\3\4\3\4\3\4\7\4\67\n\4\f\4\16\4:\13\4\3\4\3\4\3\5\3")
        buf.write("\5\5\5@\n\5\3\6\3\6\3\6\6\6E\n\6\r\6\16\6F\3\6\3\6\5\6")
        buf.write("K\n\6\3\7\3\7\3\7\6\7P\n\7\r\7\16\7Q\3\7\3\7\7\7V\n\7")
        buf.write("\f\7\16\7Y\13\7\3\7\3\7\6\7]\n\7\r\7\16\7^\5\7a\n\7\3")
        buf.write("\b\3\b\3\t\3\t\7\tg\n\t\f\t\16\tj\13\t\3\t\5\tm\n\t\3")
        buf.write("\n\3\n\3\n\2\2\13\2\4\6\b\n\f\16\20\22\2\4\3\2\6\b\5\2")
        buf.write("\3\13\r\r\20\20\2x\2\31\3\2\2\2\4#\3\2\2\2\6\61\3\2\2")
        buf.write("\2\b?\3\2\2\2\nJ\3\2\2\2\f`\3\2\2\2\16b\3\2\2\2\20l\3")
        buf.write("\2\2\2\22n\3\2\2\2\24\25\5\n\6\2\25\26\5\f\7\2\26\32\3")
        buf.write("\2\2\2\27\32\5\n\6\2\30\32\7\n\2\2\31\24\3\2\2\2\31\27")
        buf.write("\3\2\2\2\31\30\3\2\2\2\32\3\3\2\2\2\33\35\5\6\4\2\34\36")
        buf.write("\7\f\2\2\35\34\3\2\2\2\36\37\3\2\2\2\37\35\3\2\2\2\37")
        buf.write(" \3\2\2\2 \"\3\2\2\2!\33\3\2\2\2\"%\3\2\2\2#!\3\2\2\2")
        buf.write("#$\3\2\2\2$&\3\2\2\2%#\3\2\2\2&-\5\6\4\2\')\7\f\2\2(\'")
        buf.write("\3\2\2\2)*\3\2\2\2*(\3\2\2\2*+\3\2\2\2+.\3\2\2\2,.\3\2")
        buf.write("\2\2-(\3\2\2\2-,\3\2\2\2./\3\2\2\2/\60\5\20\t\2\60\5\3")
        buf.write("\2\2\2\61\62\5\f\7\2\628\7\t\2\2\63\64\5\2\2\2\64\65\7")
        buf.write("\13\2\2\65\67\3\2\2\2\66\63\3\2\2\2\67:\3\2\2\28\66\3")
        buf.write("\2\2\289\3\2\2\29;\3\2\2\2:8\3\2\2\2;<\5\2\2\2<\7\3\2")
        buf.write("\2\2=@\5\n\6\2>@\5\f\7\2?=\3\2\2\2?>\3\2\2\2@\t\3\2\2")
        buf.write("\2AK\7\7\2\2BD\7\16\2\2CE\5\22\n\2DC\3\2\2\2EF\3\2\2\2")
        buf.write("FD\3\2\2\2FG\3\2\2\2GH\3\2\2\2HI\7\16\2\2IK\3\2\2\2JA")
        buf.write("\3\2\2\2JB\3\2\2\2K\13\3\2\2\2La\7\b\2\2MO\7\3\2\2NP\5")
        buf.write("\16\b\2ON\3\2\2\2PQ\3\2\2\2QO\3\2\2\2QR\3\2\2\2RS\3\2")
        buf.write("\2\2SW\7\4\2\2TV\7\5\2\2UT\3\2\2\2VY\3\2\2\2WU\3\2\2\2")
        buf.write("WX\3\2\2\2Xa\3\2\2\2YW\3\2\2\2Z\\\5\16\b\2[]\7\5\2\2\\")
        buf.write("[\3\2\2\2]^\3\2\2\2^\\\3\2\2\2^_\3\2\2\2_a\3\2\2\2`L\3")
        buf.write("\2\2\2`M\3\2\2\2`Z\3\2\2\2a\r\3\2\2\2bc\t\2\2\2c\17\3")
        buf.write("\2\2\2dh\7\r\2\2eg\5\22\n\2fe\3\2\2\2gj\3\2\2\2hf\3\2")
        buf.write("\2\2hi\3\2\2\2im\3\2\2\2jh\3\2\2\2km\3\2\2\2ld\3\2\2\2")
        buf.write("lk\3\2\2\2m\21\3\2\2\2no\t\3\2\2o\23\3\2\2\2\21\31\37")
        buf.write("#*-8?FJQW^`hl")
        return buf.getvalue()


class RegGParser ( Parser ):

    grammarFileName = "RegG.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'<'", "'>'", "'''", "'_'", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "'|'", "<INVALID>", 
                     "'#'", "'\"'" ]

    symbolicNames = [ "<INVALID>", "LEFT_ANGLE", "RIGHT_ANGLE", "APOSTROPHE", 
                      "UNDERSCORE", "TERMINAL", "CAPS", "ARROW", "EPSILON", 
                      "DELIMITER", "NEWLINE", "HASH", "QUOTE", "WS", "ANYCHAR" ]

    RULE_rewrite = 0
    RULE_start = 1
    RULE_onerule = 2
    RULE_term_or_nonterm = 3
    RULE_term = 4
    RULE_nonterm = 5
    RULE_symbol = 6
    RULE_comment = 7
    RULE_anyvalue = 8

    ruleNames =  [ "rewrite", "start", "onerule", "term_or_nonterm", "term", 
                   "nonterm", "symbol", "comment", "anyvalue" ]

    EOF = Token.EOF
    LEFT_ANGLE=1
    RIGHT_ANGLE=2
    APOSTROPHE=3
    UNDERSCORE=4
    TERMINAL=5
    CAPS=6
    ARROW=7
    EPSILON=8
    DELIMITER=9
    NEWLINE=10
    HASH=11
    QUOTE=12
    WS=13
    ANYCHAR=14

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class RewriteContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def term(self):
            return self.getTypedRuleContext(RegGParser.TermContext,0)


        def nonterm(self):
            return self.getTypedRuleContext(RegGParser.NontermContext,0)


        def EPSILON(self):
            return self.getToken(RegGParser.EPSILON, 0)

        def getRuleIndex(self):
            return RegGParser.RULE_rewrite

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRewrite" ):
                listener.enterRewrite(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRewrite" ):
                listener.exitRewrite(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRewrite" ):
                return visitor.visitRewrite(self)
            else:
                return visitor.visitChildren(self)




    def rewrite(self):

        localctx = RegGParser.RewriteContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_rewrite)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 23
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,0,self._ctx)
            if la_ == 1:
                self.state = 18
                self.term()
                self.state = 19
                self.nonterm()
                pass

            elif la_ == 2:
                self.state = 21
                self.term()
                pass

            elif la_ == 3:
                self.state = 22
                self.match(RegGParser.EPSILON)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StartContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def onerule(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RegGParser.OneruleContext)
            else:
                return self.getTypedRuleContext(RegGParser.OneruleContext,i)


        def comment(self):
            return self.getTypedRuleContext(RegGParser.CommentContext,0)


        def NEWLINE(self, i:int=None):
            if i is None:
                return self.getTokens(RegGParser.NEWLINE)
            else:
                return self.getToken(RegGParser.NEWLINE, i)

        def getRuleIndex(self):
            return RegGParser.RULE_start

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStart" ):
                listener.enterStart(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStart" ):
                listener.exitStart(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStart" ):
                return visitor.visitStart(self)
            else:
                return visitor.visitChildren(self)




    def start(self):

        localctx = RegGParser.StartContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_start)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 33
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,2,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 25
                    self.onerule()
                    self.state = 27 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while True:
                        self.state = 26
                        self.match(RegGParser.NEWLINE)
                        self.state = 29 
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)
                        if not (_la==RegGParser.NEWLINE):
                            break
             
                self.state = 35
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,2,self._ctx)

            self.state = 36
            self.onerule()
            self.state = 43
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [RegGParser.NEWLINE]:
                self.state = 38 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 37
                    self.match(RegGParser.NEWLINE)
                    self.state = 40 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not (_la==RegGParser.NEWLINE):
                        break

                pass
            elif token in [RegGParser.EOF, RegGParser.HASH]:
                pass
            else:
                raise NoViableAltException(self)

            self.state = 45
            self.comment()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class OneruleContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def nonterm(self):
            return self.getTypedRuleContext(RegGParser.NontermContext,0)


        def ARROW(self):
            return self.getToken(RegGParser.ARROW, 0)

        def rewrite(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RegGParser.RewriteContext)
            else:
                return self.getTypedRuleContext(RegGParser.RewriteContext,i)


        def DELIMITER(self, i:int=None):
            if i is None:
                return self.getTokens(RegGParser.DELIMITER)
            else:
                return self.getToken(RegGParser.DELIMITER, i)

        def getRuleIndex(self):
            return RegGParser.RULE_onerule

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterOnerule" ):
                listener.enterOnerule(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitOnerule" ):
                listener.exitOnerule(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitOnerule" ):
                return visitor.visitOnerule(self)
            else:
                return visitor.visitChildren(self)




    def onerule(self):

        localctx = RegGParser.OneruleContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_onerule)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 47
            self.nonterm()
            self.state = 48
            self.match(RegGParser.ARROW)
            self.state = 54
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,5,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 49
                    self.rewrite()
                    self.state = 50
                    self.match(RegGParser.DELIMITER) 
                self.state = 56
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,5,self._ctx)

            self.state = 57
            self.rewrite()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Term_or_nontermContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def term(self):
            return self.getTypedRuleContext(RegGParser.TermContext,0)


        def nonterm(self):
            return self.getTypedRuleContext(RegGParser.NontermContext,0)


        def getRuleIndex(self):
            return RegGParser.RULE_term_or_nonterm

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTerm_or_nonterm" ):
                listener.enterTerm_or_nonterm(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTerm_or_nonterm" ):
                listener.exitTerm_or_nonterm(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTerm_or_nonterm" ):
                return visitor.visitTerm_or_nonterm(self)
            else:
                return visitor.visitChildren(self)




    def term_or_nonterm(self):

        localctx = RegGParser.Term_or_nontermContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_term_or_nonterm)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 61
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,6,self._ctx)
            if la_ == 1:
                self.state = 59
                self.term()
                pass

            elif la_ == 2:
                self.state = 60
                self.nonterm()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TermContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def TERMINAL(self):
            return self.getToken(RegGParser.TERMINAL, 0)

        def QUOTE(self, i:int=None):
            if i is None:
                return self.getTokens(RegGParser.QUOTE)
            else:
                return self.getToken(RegGParser.QUOTE, i)

        def anyvalue(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RegGParser.AnyvalueContext)
            else:
                return self.getTypedRuleContext(RegGParser.AnyvalueContext,i)


        def getRuleIndex(self):
            return RegGParser.RULE_term

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTerm" ):
                listener.enterTerm(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTerm" ):
                listener.exitTerm(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTerm" ):
                return visitor.visitTerm(self)
            else:
                return visitor.visitChildren(self)




    def term(self):

        localctx = RegGParser.TermContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_term)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 72
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [RegGParser.TERMINAL]:
                self.state = 63
                self.match(RegGParser.TERMINAL)
                pass
            elif token in [RegGParser.QUOTE]:
                self.state = 64
                self.match(RegGParser.QUOTE)
                self.state = 66 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 65
                    self.anyvalue()
                    self.state = 68 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << RegGParser.LEFT_ANGLE) | (1 << RegGParser.RIGHT_ANGLE) | (1 << RegGParser.APOSTROPHE) | (1 << RegGParser.UNDERSCORE) | (1 << RegGParser.TERMINAL) | (1 << RegGParser.CAPS) | (1 << RegGParser.ARROW) | (1 << RegGParser.EPSILON) | (1 << RegGParser.DELIMITER) | (1 << RegGParser.HASH) | (1 << RegGParser.ANYCHAR))) != 0)):
                        break

                self.state = 70
                self.match(RegGParser.QUOTE)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class NontermContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def CAPS(self):
            return self.getToken(RegGParser.CAPS, 0)

        def LEFT_ANGLE(self):
            return self.getToken(RegGParser.LEFT_ANGLE, 0)

        def RIGHT_ANGLE(self):
            return self.getToken(RegGParser.RIGHT_ANGLE, 0)

        def symbol(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RegGParser.SymbolContext)
            else:
                return self.getTypedRuleContext(RegGParser.SymbolContext,i)


        def APOSTROPHE(self, i:int=None):
            if i is None:
                return self.getTokens(RegGParser.APOSTROPHE)
            else:
                return self.getToken(RegGParser.APOSTROPHE, i)

        def getRuleIndex(self):
            return RegGParser.RULE_nonterm

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNonterm" ):
                listener.enterNonterm(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNonterm" ):
                listener.exitNonterm(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitNonterm" ):
                return visitor.visitNonterm(self)
            else:
                return visitor.visitChildren(self)




    def nonterm(self):

        localctx = RegGParser.NontermContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_nonterm)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 94
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,12,self._ctx)
            if la_ == 1:
                self.state = 74
                self.match(RegGParser.CAPS)
                pass

            elif la_ == 2:
                self.state = 75
                self.match(RegGParser.LEFT_ANGLE)
                self.state = 77 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 76
                    self.symbol()
                    self.state = 79 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << RegGParser.UNDERSCORE) | (1 << RegGParser.TERMINAL) | (1 << RegGParser.CAPS))) != 0)):
                        break

                self.state = 81
                self.match(RegGParser.RIGHT_ANGLE)

                self.state = 85
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==RegGParser.APOSTROPHE:
                    self.state = 82
                    self.match(RegGParser.APOSTROPHE)
                    self.state = 87
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                pass

            elif la_ == 3:
                self.state = 88
                self.symbol()
                self.state = 90 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 89
                    self.match(RegGParser.APOSTROPHE)
                    self.state = 92 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not (_la==RegGParser.APOSTROPHE):
                        break

                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class SymbolContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def TERMINAL(self):
            return self.getToken(RegGParser.TERMINAL, 0)

        def CAPS(self):
            return self.getToken(RegGParser.CAPS, 0)

        def UNDERSCORE(self):
            return self.getToken(RegGParser.UNDERSCORE, 0)

        def getRuleIndex(self):
            return RegGParser.RULE_symbol

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSymbol" ):
                listener.enterSymbol(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSymbol" ):
                listener.exitSymbol(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSymbol" ):
                return visitor.visitSymbol(self)
            else:
                return visitor.visitChildren(self)




    def symbol(self):

        localctx = RegGParser.SymbolContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_symbol)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 96
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << RegGParser.UNDERSCORE) | (1 << RegGParser.TERMINAL) | (1 << RegGParser.CAPS))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class CommentContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def HASH(self):
            return self.getToken(RegGParser.HASH, 0)

        def anyvalue(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RegGParser.AnyvalueContext)
            else:
                return self.getTypedRuleContext(RegGParser.AnyvalueContext,i)


        def getRuleIndex(self):
            return RegGParser.RULE_comment

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterComment" ):
                listener.enterComment(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitComment" ):
                listener.exitComment(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitComment" ):
                return visitor.visitComment(self)
            else:
                return visitor.visitChildren(self)




    def comment(self):

        localctx = RegGParser.CommentContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_comment)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 106
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [RegGParser.HASH]:
                self.state = 98
                self.match(RegGParser.HASH)
                self.state = 102
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << RegGParser.LEFT_ANGLE) | (1 << RegGParser.RIGHT_ANGLE) | (1 << RegGParser.APOSTROPHE) | (1 << RegGParser.UNDERSCORE) | (1 << RegGParser.TERMINAL) | (1 << RegGParser.CAPS) | (1 << RegGParser.ARROW) | (1 << RegGParser.EPSILON) | (1 << RegGParser.DELIMITER) | (1 << RegGParser.HASH) | (1 << RegGParser.ANYCHAR))) != 0):
                    self.state = 99
                    self.anyvalue()
                    self.state = 104
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                pass
            elif token in [RegGParser.EOF]:
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AnyvalueContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LEFT_ANGLE(self):
            return self.getToken(RegGParser.LEFT_ANGLE, 0)

        def RIGHT_ANGLE(self):
            return self.getToken(RegGParser.RIGHT_ANGLE, 0)

        def APOSTROPHE(self):
            return self.getToken(RegGParser.APOSTROPHE, 0)

        def UNDERSCORE(self):
            return self.getToken(RegGParser.UNDERSCORE, 0)

        def TERMINAL(self):
            return self.getToken(RegGParser.TERMINAL, 0)

        def CAPS(self):
            return self.getToken(RegGParser.CAPS, 0)

        def ARROW(self):
            return self.getToken(RegGParser.ARROW, 0)

        def EPSILON(self):
            return self.getToken(RegGParser.EPSILON, 0)

        def DELIMITER(self):
            return self.getToken(RegGParser.DELIMITER, 0)

        def ANYCHAR(self):
            return self.getToken(RegGParser.ANYCHAR, 0)

        def HASH(self):
            return self.getToken(RegGParser.HASH, 0)

        def getRuleIndex(self):
            return RegGParser.RULE_anyvalue

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAnyvalue" ):
                listener.enterAnyvalue(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAnyvalue" ):
                listener.exitAnyvalue(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAnyvalue" ):
                return visitor.visitAnyvalue(self)
            else:
                return visitor.visitChildren(self)




    def anyvalue(self):

        localctx = RegGParser.AnyvalueContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_anyvalue)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 108
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << RegGParser.LEFT_ANGLE) | (1 << RegGParser.RIGHT_ANGLE) | (1 << RegGParser.APOSTROPHE) | (1 << RegGParser.UNDERSCORE) | (1 << RegGParser.TERMINAL) | (1 << RegGParser.CAPS) | (1 << RegGParser.ARROW) | (1 << RegGParser.EPSILON) | (1 << RegGParser.DELIMITER) | (1 << RegGParser.HASH) | (1 << RegGParser.ANYCHAR))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





