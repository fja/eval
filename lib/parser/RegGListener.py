# Generated from lib/parser/RegG.g4 by ANTLR 4.9.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .RegGParser import RegGParser
else:
    from RegGParser import RegGParser

# This class defines a complete listener for a parse tree produced by RegGParser.
class RegGListener(ParseTreeListener):

    # Enter a parse tree produced by RegGParser#rewrite.
    def enterRewrite(self, ctx:RegGParser.RewriteContext):
        pass

    # Exit a parse tree produced by RegGParser#rewrite.
    def exitRewrite(self, ctx:RegGParser.RewriteContext):
        pass


    # Enter a parse tree produced by RegGParser#start.
    def enterStart(self, ctx:RegGParser.StartContext):
        pass

    # Exit a parse tree produced by RegGParser#start.
    def exitStart(self, ctx:RegGParser.StartContext):
        pass


    # Enter a parse tree produced by RegGParser#onerule.
    def enterOnerule(self, ctx:RegGParser.OneruleContext):
        pass

    # Exit a parse tree produced by RegGParser#onerule.
    def exitOnerule(self, ctx:RegGParser.OneruleContext):
        pass


    # Enter a parse tree produced by RegGParser#term_or_nonterm.
    def enterTerm_or_nonterm(self, ctx:RegGParser.Term_or_nontermContext):
        pass

    # Exit a parse tree produced by RegGParser#term_or_nonterm.
    def exitTerm_or_nonterm(self, ctx:RegGParser.Term_or_nontermContext):
        pass


    # Enter a parse tree produced by RegGParser#term.
    def enterTerm(self, ctx:RegGParser.TermContext):
        pass

    # Exit a parse tree produced by RegGParser#term.
    def exitTerm(self, ctx:RegGParser.TermContext):
        pass


    # Enter a parse tree produced by RegGParser#nonterm.
    def enterNonterm(self, ctx:RegGParser.NontermContext):
        pass

    # Exit a parse tree produced by RegGParser#nonterm.
    def exitNonterm(self, ctx:RegGParser.NontermContext):
        pass


    # Enter a parse tree produced by RegGParser#symbol.
    def enterSymbol(self, ctx:RegGParser.SymbolContext):
        pass

    # Exit a parse tree produced by RegGParser#symbol.
    def exitSymbol(self, ctx:RegGParser.SymbolContext):
        pass


    # Enter a parse tree produced by RegGParser#comment.
    def enterComment(self, ctx:RegGParser.CommentContext):
        pass

    # Exit a parse tree produced by RegGParser#comment.
    def exitComment(self, ctx:RegGParser.CommentContext):
        pass


    # Enter a parse tree produced by RegGParser#anyvalue.
    def enterAnyvalue(self, ctx:RegGParser.AnyvalueContext):
        pass

    # Exit a parse tree produced by RegGParser#anyvalue.
    def exitAnyvalue(self, ctx:RegGParser.AnyvalueContext):
        pass



del RegGParser