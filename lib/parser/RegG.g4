grammar RegG;

import CFG;

rewrite:         (term nonterm | term | EPSILON);

/*
vim: ft=antlr
*/
