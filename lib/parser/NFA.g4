grammar NFA;

import FA;

production:  LEFT_PARENTHESIS statename COMMA (statename | EPSILON) RIGHT_PARENTHESIS EQUALS stateset;
