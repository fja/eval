# Generated from lib/parser/SET.g4 by ANTLR 4.9.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .SETParser import SETParser
else:
    from SETParser import SETParser

# This class defines a complete listener for a parse tree produced by SETParser.
class SETListener(ParseTreeListener):

    # Enter a parse tree produced by SETParser#start.
    def enterStart(self, ctx:SETParser.StartContext):
        pass

    # Exit a parse tree produced by SETParser#start.
    def exitStart(self, ctx:SETParser.StartContext):
        pass


    # Enter a parse tree produced by SETParser#alph.
    def enterAlph(self, ctx:SETParser.AlphContext):
        pass

    # Exit a parse tree produced by SETParser#alph.
    def exitAlph(self, ctx:SETParser.AlphContext):
        pass


    # Enter a parse tree produced by SETParser#expr.
    def enterExpr(self, ctx:SETParser.ExprContext):
        pass

    # Exit a parse tree produced by SETParser#expr.
    def exitExpr(self, ctx:SETParser.ExprContext):
        pass


    # Enter a parse tree produced by SETParser#unop.
    def enterUnop(self, ctx:SETParser.UnopContext):
        pass

    # Exit a parse tree produced by SETParser#unop.
    def exitUnop(self, ctx:SETParser.UnopContext):
        pass


    # Enter a parse tree produced by SETParser#parentheses.
    def enterParentheses(self, ctx:SETParser.ParenthesesContext):
        pass

    # Exit a parse tree produced by SETParser#parentheses.
    def exitParentheses(self, ctx:SETParser.ParenthesesContext):
        pass


    # Enter a parse tree produced by SETParser#atom_set.
    def enterAtom_set(self, ctx:SETParser.Atom_setContext):
        pass

    # Exit a parse tree produced by SETParser#atom_set.
    def exitAtom_set(self, ctx:SETParser.Atom_setContext):
        pass


    # Enter a parse tree produced by SETParser#symbol.
    def enterSymbol(self, ctx:SETParser.SymbolContext):
        pass

    # Exit a parse tree produced by SETParser#symbol.
    def exitSymbol(self, ctx:SETParser.SymbolContext):
        pass


    # Enter a parse tree produced by SETParser#comment.
    def enterComment(self, ctx:SETParser.CommentContext):
        pass

    # Exit a parse tree produced by SETParser#comment.
    def exitComment(self, ctx:SETParser.CommentContext):
        pass


    # Enter a parse tree produced by SETParser#anyvalue.
    def enterAnyvalue(self, ctx:SETParser.AnyvalueContext):
        pass

    # Exit a parse tree produced by SETParser#anyvalue.
    def exitAnyvalue(self, ctx:SETParser.AnyvalueContext):
        pass



del SETParser