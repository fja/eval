# Generated from lib/parser/CFG.g4 by ANTLR 4.9.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .CFGParser import CFGParser
else:
    from CFGParser import CFGParser

# This class defines a complete listener for a parse tree produced by CFGParser.
class CFGListener(ParseTreeListener):

    # Enter a parse tree produced by CFGParser#start.
    def enterStart(self, ctx:CFGParser.StartContext):
        pass

    # Exit a parse tree produced by CFGParser#start.
    def exitStart(self, ctx:CFGParser.StartContext):
        pass


    # Enter a parse tree produced by CFGParser#onerule.
    def enterOnerule(self, ctx:CFGParser.OneruleContext):
        pass

    # Exit a parse tree produced by CFGParser#onerule.
    def exitOnerule(self, ctx:CFGParser.OneruleContext):
        pass


    # Enter a parse tree produced by CFGParser#rewrite.
    def enterRewrite(self, ctx:CFGParser.RewriteContext):
        pass

    # Exit a parse tree produced by CFGParser#rewrite.
    def exitRewrite(self, ctx:CFGParser.RewriteContext):
        pass


    # Enter a parse tree produced by CFGParser#term_or_nonterm.
    def enterTerm_or_nonterm(self, ctx:CFGParser.Term_or_nontermContext):
        pass

    # Exit a parse tree produced by CFGParser#term_or_nonterm.
    def exitTerm_or_nonterm(self, ctx:CFGParser.Term_or_nontermContext):
        pass


    # Enter a parse tree produced by CFGParser#term.
    def enterTerm(self, ctx:CFGParser.TermContext):
        pass

    # Exit a parse tree produced by CFGParser#term.
    def exitTerm(self, ctx:CFGParser.TermContext):
        pass


    # Enter a parse tree produced by CFGParser#nonterm.
    def enterNonterm(self, ctx:CFGParser.NontermContext):
        pass

    # Exit a parse tree produced by CFGParser#nonterm.
    def exitNonterm(self, ctx:CFGParser.NontermContext):
        pass


    # Enter a parse tree produced by CFGParser#symbol.
    def enterSymbol(self, ctx:CFGParser.SymbolContext):
        pass

    # Exit a parse tree produced by CFGParser#symbol.
    def exitSymbol(self, ctx:CFGParser.SymbolContext):
        pass


    # Enter a parse tree produced by CFGParser#comment.
    def enterComment(self, ctx:CFGParser.CommentContext):
        pass

    # Exit a parse tree produced by CFGParser#comment.
    def exitComment(self, ctx:CFGParser.CommentContext):
        pass


    # Enter a parse tree produced by CFGParser#anyvalue.
    def enterAnyvalue(self, ctx:CFGParser.AnyvalueContext):
        pass

    # Exit a parse tree produced by CFGParser#anyvalue.
    def exitAnyvalue(self, ctx:CFGParser.AnyvalueContext):
        pass



del CFGParser