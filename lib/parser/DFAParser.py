# Generated from lib/parser/DFA.g4 by ANTLR 4.9.2
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\20")
        buf.write("W\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b")
        buf.write("\t\b\4\t\t\t\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\3\3\3\7")
        buf.write("\3\35\n\3\f\3\16\3 \13\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\5")
        buf.write("\4)\n\4\3\5\3\5\3\5\3\5\7\5/\n\5\f\5\16\5\62\13\5\3\5")
        buf.write("\5\5\65\n\5\3\5\3\5\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3")
        buf.write("\7\6\7B\n\7\r\7\16\7C\3\7\3\7\5\7H\n\7\3\b\3\b\3\b\7\b")
        buf.write("M\n\b\f\b\16\bP\13\b\3\b\5\bS\n\b\3\t\3\t\3\t\2\2\n\2")
        buf.write("\4\6\b\n\f\16\20\2\3\4\2\3\r\20\20\2Y\2\22\3\2\2\2\4\32")
        buf.write("\3\2\2\2\6(\3\2\2\2\b*\3\2\2\2\n8\3\2\2\2\fG\3\2\2\2\16")
        buf.write("R\3\2\2\2\20T\3\2\2\2\22\23\7\5\2\2\23\24\5\f\7\2\24\25")
        buf.write("\7\t\2\2\25\26\5\f\7\2\26\27\7\6\2\2\27\30\7\4\2\2\30")
        buf.write("\31\5\f\7\2\31\3\3\2\2\2\32\36\5\6\4\2\33\35\5\2\2\2\34")
        buf.write("\33\3\2\2\2\35 \3\2\2\2\36\34\3\2\2\2\36\37\3\2\2\2\37")
        buf.write("!\3\2\2\2 \36\3\2\2\2!\"\5\n\6\2\"#\5\16\b\2#\5\3\2\2")
        buf.write("\2$%\7\3\2\2%&\7\4\2\2&)\5\f\7\2\')\3\2\2\2($\3\2\2\2")
        buf.write("(\'\3\2\2\2)\7\3\2\2\2*\64\7\7\2\2+\60\5\f\7\2,-\7\t\2")
        buf.write("\2-/\5\f\7\2.,\3\2\2\2/\62\3\2\2\2\60.\3\2\2\2\60\61\3")
        buf.write("\2\2\2\61\65\3\2\2\2\62\60\3\2\2\2\63\65\3\2\2\2\64+\3")
        buf.write("\2\2\2\64\63\3\2\2\2\65\66\3\2\2\2\66\67\7\b\2\2\67\t")
        buf.write("\3\2\2\289\7\n\2\29:\7\4\2\2:;\5\b\5\2;\13\3\2\2\2<H\7")
        buf.write("\3\2\2=H\7\n\2\2>H\7\f\2\2?A\7\16\2\2@B\5\20\t\2A@\3\2")
        buf.write("\2\2BC\3\2\2\2CA\3\2\2\2CD\3\2\2\2DE\3\2\2\2EF\7\16\2")
        buf.write("\2FH\3\2\2\2G<\3\2\2\2G=\3\2\2\2G>\3\2\2\2G?\3\2\2\2H")
        buf.write("\r\3\2\2\2IN\7\r\2\2JM\5\20\t\2KM\7\16\2\2LJ\3\2\2\2L")
        buf.write("K\3\2\2\2MP\3\2\2\2NL\3\2\2\2NO\3\2\2\2OS\3\2\2\2PN\3")
        buf.write("\2\2\2QS\3\2\2\2RI\3\2\2\2RQ\3\2\2\2S\17\3\2\2\2TU\t\2")
        buf.write("\2\2U\21\3\2\2\2\13\36(\60\64CGLNR")
        return buf.getvalue()


class DFAParser ( Parser ):

    grammarFileName = "DFA.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'init'", "'='", "'('", "')'", "'{'", 
                     "'}'", "','", "'final'", "<INVALID>", "<INVALID>", 
                     "'#'", "'\"'" ]

    symbolicNames = [ "<INVALID>", "INIT", "EQUALS", "LEFT_PARENTHESIS", 
                      "RIGHT_PARENTHESIS", "LEFT_BRACKET", "RIGHT_BRACKET", 
                      "COMMA", "FINAL", "EPSILON", "STATE", "HASH", "QUOTE", 
                      "WS", "ANYCHAR" ]

    RULE_production = 0
    RULE_start = 1
    RULE_init = 2
    RULE_stateset = 3
    RULE_final = 4
    RULE_statename = 5
    RULE_comment = 6
    RULE_anyvalue = 7

    ruleNames =  [ "production", "start", "init", "stateset", "final", "statename", 
                   "comment", "anyvalue" ]

    EOF = Token.EOF
    INIT=1
    EQUALS=2
    LEFT_PARENTHESIS=3
    RIGHT_PARENTHESIS=4
    LEFT_BRACKET=5
    RIGHT_BRACKET=6
    COMMA=7
    FINAL=8
    EPSILON=9
    STATE=10
    HASH=11
    QUOTE=12
    WS=13
    ANYCHAR=14

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class ProductionContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LEFT_PARENTHESIS(self):
            return self.getToken(DFAParser.LEFT_PARENTHESIS, 0)

        def statename(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(DFAParser.StatenameContext)
            else:
                return self.getTypedRuleContext(DFAParser.StatenameContext,i)


        def COMMA(self):
            return self.getToken(DFAParser.COMMA, 0)

        def RIGHT_PARENTHESIS(self):
            return self.getToken(DFAParser.RIGHT_PARENTHESIS, 0)

        def EQUALS(self):
            return self.getToken(DFAParser.EQUALS, 0)

        def getRuleIndex(self):
            return DFAParser.RULE_production

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterProduction" ):
                listener.enterProduction(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitProduction" ):
                listener.exitProduction(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitProduction" ):
                return visitor.visitProduction(self)
            else:
                return visitor.visitChildren(self)




    def production(self):

        localctx = DFAParser.ProductionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_production)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 16
            self.match(DFAParser.LEFT_PARENTHESIS)
            self.state = 17
            self.statename()
            self.state = 18
            self.match(DFAParser.COMMA)
            self.state = 19
            self.statename()
            self.state = 20
            self.match(DFAParser.RIGHT_PARENTHESIS)
            self.state = 21
            self.match(DFAParser.EQUALS)
            self.state = 22
            self.statename()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StartContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def init(self):
            return self.getTypedRuleContext(DFAParser.InitContext,0)


        def final(self):
            return self.getTypedRuleContext(DFAParser.FinalContext,0)


        def comment(self):
            return self.getTypedRuleContext(DFAParser.CommentContext,0)


        def production(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(DFAParser.ProductionContext)
            else:
                return self.getTypedRuleContext(DFAParser.ProductionContext,i)


        def getRuleIndex(self):
            return DFAParser.RULE_start

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStart" ):
                listener.enterStart(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStart" ):
                listener.exitStart(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStart" ):
                return visitor.visitStart(self)
            else:
                return visitor.visitChildren(self)




    def start(self):

        localctx = DFAParser.StartContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_start)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 24
            self.init()
            self.state = 28
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==DFAParser.LEFT_PARENTHESIS:
                self.state = 25
                self.production()
                self.state = 30
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 31
            self.final()
            self.state = 32
            self.comment()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class InitContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INIT(self):
            return self.getToken(DFAParser.INIT, 0)

        def EQUALS(self):
            return self.getToken(DFAParser.EQUALS, 0)

        def statename(self):
            return self.getTypedRuleContext(DFAParser.StatenameContext,0)


        def getRuleIndex(self):
            return DFAParser.RULE_init

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInit" ):
                listener.enterInit(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInit" ):
                listener.exitInit(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInit" ):
                return visitor.visitInit(self)
            else:
                return visitor.visitChildren(self)




    def init(self):

        localctx = DFAParser.InitContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_init)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 38
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [DFAParser.INIT]:
                self.state = 34
                self.match(DFAParser.INIT)
                self.state = 35
                self.match(DFAParser.EQUALS)
                self.state = 36
                self.statename()
                pass
            elif token in [DFAParser.LEFT_PARENTHESIS, DFAParser.FINAL]:
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StatesetContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LEFT_BRACKET(self):
            return self.getToken(DFAParser.LEFT_BRACKET, 0)

        def RIGHT_BRACKET(self):
            return self.getToken(DFAParser.RIGHT_BRACKET, 0)

        def statename(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(DFAParser.StatenameContext)
            else:
                return self.getTypedRuleContext(DFAParser.StatenameContext,i)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(DFAParser.COMMA)
            else:
                return self.getToken(DFAParser.COMMA, i)

        def getRuleIndex(self):
            return DFAParser.RULE_stateset

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStateset" ):
                listener.enterStateset(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStateset" ):
                listener.exitStateset(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStateset" ):
                return visitor.visitStateset(self)
            else:
                return visitor.visitChildren(self)




    def stateset(self):

        localctx = DFAParser.StatesetContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_stateset)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 40
            self.match(DFAParser.LEFT_BRACKET)
            self.state = 50
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [DFAParser.INIT, DFAParser.FINAL, DFAParser.STATE, DFAParser.QUOTE]:
                self.state = 41
                self.statename()
                self.state = 46
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==DFAParser.COMMA:
                    self.state = 42
                    self.match(DFAParser.COMMA)
                    self.state = 43
                    self.statename()
                    self.state = 48
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                pass
            elif token in [DFAParser.RIGHT_BRACKET]:
                pass
            else:
                raise NoViableAltException(self)

            self.state = 52
            self.match(DFAParser.RIGHT_BRACKET)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FinalContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def FINAL(self):
            return self.getToken(DFAParser.FINAL, 0)

        def EQUALS(self):
            return self.getToken(DFAParser.EQUALS, 0)

        def stateset(self):
            return self.getTypedRuleContext(DFAParser.StatesetContext,0)


        def getRuleIndex(self):
            return DFAParser.RULE_final

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFinal" ):
                listener.enterFinal(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFinal" ):
                listener.exitFinal(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFinal" ):
                return visitor.visitFinal(self)
            else:
                return visitor.visitChildren(self)




    def final(self):

        localctx = DFAParser.FinalContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_final)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 54
            self.match(DFAParser.FINAL)
            self.state = 55
            self.match(DFAParser.EQUALS)
            self.state = 56
            self.stateset()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StatenameContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INIT(self):
            return self.getToken(DFAParser.INIT, 0)

        def FINAL(self):
            return self.getToken(DFAParser.FINAL, 0)

        def STATE(self):
            return self.getToken(DFAParser.STATE, 0)

        def QUOTE(self, i:int=None):
            if i is None:
                return self.getTokens(DFAParser.QUOTE)
            else:
                return self.getToken(DFAParser.QUOTE, i)

        def anyvalue(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(DFAParser.AnyvalueContext)
            else:
                return self.getTypedRuleContext(DFAParser.AnyvalueContext,i)


        def getRuleIndex(self):
            return DFAParser.RULE_statename

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStatename" ):
                listener.enterStatename(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStatename" ):
                listener.exitStatename(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStatename" ):
                return visitor.visitStatename(self)
            else:
                return visitor.visitChildren(self)




    def statename(self):

        localctx = DFAParser.StatenameContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_statename)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 69
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [DFAParser.INIT]:
                self.state = 58
                self.match(DFAParser.INIT)
                pass
            elif token in [DFAParser.FINAL]:
                self.state = 59
                self.match(DFAParser.FINAL)
                pass
            elif token in [DFAParser.STATE]:
                self.state = 60
                self.match(DFAParser.STATE)
                pass
            elif token in [DFAParser.QUOTE]:
                self.state = 61
                self.match(DFAParser.QUOTE)
                self.state = 63 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 62
                    self.anyvalue()
                    self.state = 65 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << DFAParser.INIT) | (1 << DFAParser.EQUALS) | (1 << DFAParser.LEFT_PARENTHESIS) | (1 << DFAParser.RIGHT_PARENTHESIS) | (1 << DFAParser.LEFT_BRACKET) | (1 << DFAParser.RIGHT_BRACKET) | (1 << DFAParser.COMMA) | (1 << DFAParser.FINAL) | (1 << DFAParser.EPSILON) | (1 << DFAParser.STATE) | (1 << DFAParser.HASH) | (1 << DFAParser.ANYCHAR))) != 0)):
                        break

                self.state = 67
                self.match(DFAParser.QUOTE)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class CommentContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def HASH(self):
            return self.getToken(DFAParser.HASH, 0)

        def anyvalue(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(DFAParser.AnyvalueContext)
            else:
                return self.getTypedRuleContext(DFAParser.AnyvalueContext,i)


        def QUOTE(self, i:int=None):
            if i is None:
                return self.getTokens(DFAParser.QUOTE)
            else:
                return self.getToken(DFAParser.QUOTE, i)

        def getRuleIndex(self):
            return DFAParser.RULE_comment

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterComment" ):
                listener.enterComment(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitComment" ):
                listener.exitComment(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitComment" ):
                return visitor.visitComment(self)
            else:
                return visitor.visitChildren(self)




    def comment(self):

        localctx = DFAParser.CommentContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_comment)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 80
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [DFAParser.HASH]:
                self.state = 71
                self.match(DFAParser.HASH)
                self.state = 76
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << DFAParser.INIT) | (1 << DFAParser.EQUALS) | (1 << DFAParser.LEFT_PARENTHESIS) | (1 << DFAParser.RIGHT_PARENTHESIS) | (1 << DFAParser.LEFT_BRACKET) | (1 << DFAParser.RIGHT_BRACKET) | (1 << DFAParser.COMMA) | (1 << DFAParser.FINAL) | (1 << DFAParser.EPSILON) | (1 << DFAParser.STATE) | (1 << DFAParser.HASH) | (1 << DFAParser.QUOTE) | (1 << DFAParser.ANYCHAR))) != 0):
                    self.state = 74
                    self._errHandler.sync(self)
                    token = self._input.LA(1)
                    if token in [DFAParser.INIT, DFAParser.EQUALS, DFAParser.LEFT_PARENTHESIS, DFAParser.RIGHT_PARENTHESIS, DFAParser.LEFT_BRACKET, DFAParser.RIGHT_BRACKET, DFAParser.COMMA, DFAParser.FINAL, DFAParser.EPSILON, DFAParser.STATE, DFAParser.HASH, DFAParser.ANYCHAR]:
                        self.state = 72
                        self.anyvalue()
                        pass
                    elif token in [DFAParser.QUOTE]:
                        self.state = 73
                        self.match(DFAParser.QUOTE)
                        pass
                    else:
                        raise NoViableAltException(self)

                    self.state = 78
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                pass
            elif token in [DFAParser.EOF]:
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AnyvalueContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INIT(self):
            return self.getToken(DFAParser.INIT, 0)

        def EQUALS(self):
            return self.getToken(DFAParser.EQUALS, 0)

        def LEFT_PARENTHESIS(self):
            return self.getToken(DFAParser.LEFT_PARENTHESIS, 0)

        def RIGHT_PARENTHESIS(self):
            return self.getToken(DFAParser.RIGHT_PARENTHESIS, 0)

        def LEFT_BRACKET(self):
            return self.getToken(DFAParser.LEFT_BRACKET, 0)

        def RIGHT_BRACKET(self):
            return self.getToken(DFAParser.RIGHT_BRACKET, 0)

        def COMMA(self):
            return self.getToken(DFAParser.COMMA, 0)

        def FINAL(self):
            return self.getToken(DFAParser.FINAL, 0)

        def EPSILON(self):
            return self.getToken(DFAParser.EPSILON, 0)

        def STATE(self):
            return self.getToken(DFAParser.STATE, 0)

        def HASH(self):
            return self.getToken(DFAParser.HASH, 0)

        def ANYCHAR(self):
            return self.getToken(DFAParser.ANYCHAR, 0)

        def getRuleIndex(self):
            return DFAParser.RULE_anyvalue

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAnyvalue" ):
                listener.enterAnyvalue(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAnyvalue" ):
                listener.exitAnyvalue(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAnyvalue" ):
                return visitor.visitAnyvalue(self)
            else:
                return visitor.visitChildren(self)




    def anyvalue(self):

        localctx = DFAParser.AnyvalueContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_anyvalue)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 82
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << DFAParser.INIT) | (1 << DFAParser.EQUALS) | (1 << DFAParser.LEFT_PARENTHESIS) | (1 << DFAParser.RIGHT_PARENTHESIS) | (1 << DFAParser.LEFT_BRACKET) | (1 << DFAParser.RIGHT_BRACKET) | (1 << DFAParser.COMMA) | (1 << DFAParser.FINAL) | (1 << DFAParser.EPSILON) | (1 << DFAParser.STATE) | (1 << DFAParser.HASH) | (1 << DFAParser.ANYCHAR))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





