# Generated from lib/parser/RegEx.g4 by ANTLR 4.9.2
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
    from typing import TextIO
else:
    from typing.io import TextIO



def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\17")
        buf.write("F\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\3\2\3\2\3\3\3\3\3\4\3\4\3\4\5\4%\n\4\3\5\3\5\3\5")
        buf.write("\3\6\3\6\3\7\3\7\3\b\3\b\3\t\3\t\3\t\5\t\63\n\t\3\n\3")
        buf.write("\n\3\n\5\n8\n\n\3\13\3\13\3\f\3\f\3\r\6\r?\n\r\r\r\16")
        buf.write("\r@\3\r\3\r\3\16\3\16\2\2\17\3\3\5\4\7\5\t\6\13\7\r\b")
        buf.write("\17\t\21\n\23\13\25\f\27\r\31\16\33\17\3\2\4\5\2\62;C")
        buf.write("\\c|\5\2\13\f\17\17\"\"\2I\2\3\3\2\2\2\2\5\3\2\2\2\2\7")
        buf.write("\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2")
        buf.write("\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2")
        buf.write("\2\2\31\3\2\2\2\2\33\3\2\2\2\3\35\3\2\2\2\5\37\3\2\2\2")
        buf.write("\7$\3\2\2\2\t&\3\2\2\2\13)\3\2\2\2\r+\3\2\2\2\17-\3\2")
        buf.write("\2\2\21\62\3\2\2\2\23\67\3\2\2\2\259\3\2\2\2\27;\3\2\2")
        buf.write("\2\31>\3\2\2\2\33D\3\2\2\2\35\36\7*\2\2\36\4\3\2\2\2\37")
        buf.write(" \7+\2\2 \6\3\2\2\2!%\7,\2\2\"#\7`\2\2#%\7,\2\2$!\3\2")
        buf.write("\2\2$\"\3\2\2\2%\b\3\2\2\2&\'\7`\2\2\'(\7-\2\2(\n\3\2")
        buf.write("\2\2)*\7\60\2\2*\f\3\2\2\2+,\7-\2\2,\16\3\2\2\2-.\t\2")
        buf.write("\2\2.\20\3\2\2\2/\63\7\u03b7\2\2\60\61\7^\2\2\61\63\7")
        buf.write("g\2\2\62/\3\2\2\2\62\60\3\2\2\2\63\22\3\2\2\2\648\7\u2207")
        buf.write("\2\2\65\66\7^\2\2\668\7\62\2\2\67\64\3\2\2\2\67\65\3\2")
        buf.write("\2\28\24\3\2\2\29:\7%\2\2:\26\3\2\2\2;<\7$\2\2<\30\3\2")
        buf.write("\2\2=?\t\3\2\2>=\3\2\2\2?@\3\2\2\2@>\3\2\2\2@A\3\2\2\2")
        buf.write("AB\3\2\2\2BC\b\r\2\2C\32\3\2\2\2DE\13\2\2\2E\34\3\2\2")
        buf.write("\2\7\2$\62\67@\3\b\2\2")
        return buf.getvalue()


class RegExLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    LEFT_PAR = 1
    RIGHT_PAR = 2
    ITER = 3
    POS_ITER = 4
    CONCAT = 5
    UNION = 6
    ALPHABET = 7
    EPSILON = 8
    EMPTYSET = 9
    HASH = 10
    QUOTE = 11
    WS = 12
    ANYCHAR = 13

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'('", "')'", "'^+'", "'.'", "'+'", "'#'", "'\"'" ]

    symbolicNames = [ "<INVALID>",
            "LEFT_PAR", "RIGHT_PAR", "ITER", "POS_ITER", "CONCAT", "UNION", 
            "ALPHABET", "EPSILON", "EMPTYSET", "HASH", "QUOTE", "WS", "ANYCHAR" ]

    ruleNames = [ "LEFT_PAR", "RIGHT_PAR", "ITER", "POS_ITER", "CONCAT", 
                  "UNION", "ALPHABET", "EPSILON", "EMPTYSET", "HASH", "QUOTE", 
                  "WS", "ANYCHAR" ]

    grammarFileName = "RegEx.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


