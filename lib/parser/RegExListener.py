# Generated from lib/parser/RegEx.g4 by ANTLR 4.9.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .RegExParser import RegExParser
else:
    from RegExParser import RegExParser

# This class defines a complete listener for a parse tree produced by RegExParser.
class RegExListener(ParseTreeListener):

    # Enter a parse tree produced by RegExParser#start.
    def enterStart(self, ctx:RegExParser.StartContext):
        pass

    # Exit a parse tree produced by RegExParser#start.
    def exitStart(self, ctx:RegExParser.StartContext):
        pass


    # Enter a parse tree produced by RegExParser#expr.
    def enterExpr(self, ctx:RegExParser.ExprContext):
        pass

    # Exit a parse tree produced by RegExParser#expr.
    def exitExpr(self, ctx:RegExParser.ExprContext):
        pass


    # Enter a parse tree produced by RegExParser#concatenated.
    def enterConcatenated(self, ctx:RegExParser.ConcatenatedContext):
        pass

    # Exit a parse tree produced by RegExParser#concatenated.
    def exitConcatenated(self, ctx:RegExParser.ConcatenatedContext):
        pass


    # Enter a parse tree produced by RegExParser#iterable.
    def enterIterable(self, ctx:RegExParser.IterableContext):
        pass

    # Exit a parse tree produced by RegExParser#iterable.
    def exitIterable(self, ctx:RegExParser.IterableContext):
        pass


    # Enter a parse tree produced by RegExParser#parentheses.
    def enterParentheses(self, ctx:RegExParser.ParenthesesContext):
        pass

    # Exit a parse tree produced by RegExParser#parentheses.
    def exitParentheses(self, ctx:RegExParser.ParenthesesContext):
        pass


    # Enter a parse tree produced by RegExParser#symbol.
    def enterSymbol(self, ctx:RegExParser.SymbolContext):
        pass

    # Exit a parse tree produced by RegExParser#symbol.
    def exitSymbol(self, ctx:RegExParser.SymbolContext):
        pass


    # Enter a parse tree produced by RegExParser#comment.
    def enterComment(self, ctx:RegExParser.CommentContext):
        pass

    # Exit a parse tree produced by RegExParser#comment.
    def exitComment(self, ctx:RegExParser.CommentContext):
        pass


    # Enter a parse tree produced by RegExParser#anyvalue.
    def enterAnyvalue(self, ctx:RegExParser.AnyvalueContext):
        pass

    # Exit a parse tree produced by RegExParser#anyvalue.
    def exitAnyvalue(self, ctx:RegExParser.AnyvalueContext):
        pass



del RegExParser