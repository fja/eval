grammar SET;

/* Parser Rules */

start: expr[0] (alph |) comment <EOF>;

alph : SIGSEP SIGMA EQUALS atom_set;

expr[int pr]
  : unop
  | unop ( {5 >= $pr}? CONCAT expr[6]
	 | {4 >= $pr}? INTERS expr[5]
	 | {3 >= $pr}? UNION expr[4]
	 | {2 >= $pr}? DIFF expr[3])
  ;

unop
  : parentheses
  | atom_set
  | COMPL unop
  | unop POS_COMPL
  | unop ITER
  | unop POS_ITER
  ;

parentheses:  LEFT_PAR expr[0] RIGHT_PAR;

atom_set 
  : LEFT_BR ( | symbol | symbol (COMMA symbol) ) RIGHT_BR
  | EMPTYSET
  ;

symbol : (ALPHABET | EPSILON | QUOTE anyvalue+ QUOTE);

comment: (HASH anyvalue* | );

anyvalue : LEFT_PAR | RIGHT_PAR | LEFT_BR | RIGHT_BR | COMMA | ITER | POS_ITER | CONCAT | UNION | INTERS | COMPL | POS_COMPL | ALPHABET | EPSILON | EMPTYSET | QUOTE | SIGMA | SIGSEP | EQUALS | DIFF | HASH | ANYCHAR;

LEFT_PAR : '(';
RIGHT_PAR: ')';
LEFT_BR  : '{';
RIGHT_BR : '}';
COMMA    : ',';
ITER     : ('*' | '^*');
POS_ITER : ('+' | '^+');
CONCAT   : ('.' | '·'); 
UNION    : ('υ' | '\\u' | '\\union' | '\\cup');
INTERS   : ('∩' | '\\i' | '\\intersection' | '\\cap');
COMPL    : ('\\co''-'? | '¬' | '!');
POS_COMPL: ('^c' | '\'');
ALPHABET : [a-zA-Z0-9];
EPSILON  : ('ε' | '\\''e');
EMPTYSET : ('∅' | '\\''0');
SIGMA    : ('Σ' | '\\Sigma');
SIGSEP   : ('\n' | ';');
EQUALS   : '=';
/* lower priority then the rest of \\ uses **/
DIFF     : ('∖' | '\\'  | '\\setminus');
HASH     : '#';
QUOTE    : '"';

/* Characters to be ignored */
WS : [ \r\t\n]+ -> skip ;


ANYCHAR  : .;

/*
vim: syntax=antlr
*/
