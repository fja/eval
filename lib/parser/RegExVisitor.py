# Generated from lib/parser/RegEx.g4 by ANTLR 4.9.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .RegExParser import RegExParser
else:
    from RegExParser import RegExParser

# This class defines a complete generic visitor for a parse tree produced by RegExParser.

class RegExVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by RegExParser#start.
    def visitStart(self, ctx:RegExParser.StartContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RegExParser#expr.
    def visitExpr(self, ctx:RegExParser.ExprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RegExParser#concatenated.
    def visitConcatenated(self, ctx:RegExParser.ConcatenatedContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RegExParser#iterable.
    def visitIterable(self, ctx:RegExParser.IterableContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RegExParser#parentheses.
    def visitParentheses(self, ctx:RegExParser.ParenthesesContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RegExParser#symbol.
    def visitSymbol(self, ctx:RegExParser.SymbolContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RegExParser#comment.
    def visitComment(self, ctx:RegExParser.CommentContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RegExParser#anyvalue.
    def visitAnyvalue(self, ctx:RegExParser.AnyvalueContext):
        return self.visitChildren(ctx)



del RegExParser