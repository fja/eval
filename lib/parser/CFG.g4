grammar CFG;

/* Parser Rules */

start:           (onerule NEWLINE+)* onerule (NEWLINE+ | ) comment;

onerule:         nonterm ARROW (rewrite DELIMITER)* rewrite;

rewrite:         (term_or_nonterm+ | EPSILON);

term_or_nonterm: (term | nonterm);

term:		 (TERMINAL | QUOTE anyvalue+ QUOTE);

nonterm:	 (CAPS | (LEFT_ANGLE symbol+ RIGHT_ANGLE (APOSTROPHE*)) | (symbol APOSTROPHE+));

symbol:          (TERMINAL | CAPS | UNDERSCORE);

comment:         (HASH anyvalue* | );

anyvalue:        LEFT_ANGLE | RIGHT_ANGLE | APOSTROPHE | UNDERSCORE | TERMINAL | CAPS | ARROW | EPSILON | DELIMITER | ANYCHAR | HASH;

/* Lexer Rules */

/* Tokens */

LEFT_ANGLE  : '<';
RIGHT_ANGLE : '>';
APOSTROPHE  : '\'';
UNDERSCORE  : '_';
TERMINAL    : [-~=a-z0-9];
CAPS        : [A-Z];
ARROW       : ('→' | '->');
EPSILON     : ('ε' | '\\''e');
DELIMITER   : '|';
NEWLINE     : ('\n' | ';' | ',');
HASH        : '#';
QUOTE       : '"';

/* Characters to be ignored */
WS : [ \r\t]+ -> skip ;

ANYCHAR     : .;

/*
vim: ft=antlr
*/
