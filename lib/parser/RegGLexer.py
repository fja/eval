# Generated from lib/parser/RegG.g4 by ANTLR 4.9.2
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
    from typing import TextIO
else:
    from typing.io import TextIO



def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\20")
        buf.write("F\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\3\2\3\2\3\3\3\3\3\4\3\4\3\5\3\5\3\6\3")
        buf.write("\6\3\7\3\7\3\b\3\b\3\b\5\b/\n\b\3\t\3\t\3\t\5\t\64\n\t")
        buf.write("\3\n\3\n\3\13\3\13\3\f\3\f\3\r\3\r\3\16\6\16?\n\16\r\16")
        buf.write("\16\16@\3\16\3\16\3\17\3\17\2\2\20\3\3\5\4\7\5\t\6\13")
        buf.write("\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20\3\2")
        buf.write("\6\7\2//\62;??c|\u0080\u0080\3\2C\\\5\2\f\f..==\5\2\13")
        buf.write("\13\17\17\"\"\2H\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2")
        buf.write("\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21")
        buf.write("\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3")
        buf.write("\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\3\37\3\2\2\2\5!\3\2\2")
        buf.write("\2\7#\3\2\2\2\t%\3\2\2\2\13\'\3\2\2\2\r)\3\2\2\2\17.\3")
        buf.write("\2\2\2\21\63\3\2\2\2\23\65\3\2\2\2\25\67\3\2\2\2\279\3")
        buf.write("\2\2\2\31;\3\2\2\2\33>\3\2\2\2\35D\3\2\2\2\37 \7>\2\2")
        buf.write(" \4\3\2\2\2!\"\7@\2\2\"\6\3\2\2\2#$\7)\2\2$\b\3\2\2\2")
        buf.write("%&\7a\2\2&\n\3\2\2\2\'(\t\2\2\2(\f\3\2\2\2)*\t\3\2\2*")
        buf.write("\16\3\2\2\2+/\7\u2194\2\2,-\7/\2\2-/\7@\2\2.+\3\2\2\2")
        buf.write(".,\3\2\2\2/\20\3\2\2\2\60\64\7\u03b7\2\2\61\62\7^\2\2")
        buf.write("\62\64\7g\2\2\63\60\3\2\2\2\63\61\3\2\2\2\64\22\3\2\2")
        buf.write("\2\65\66\7~\2\2\66\24\3\2\2\2\678\t\4\2\28\26\3\2\2\2")
        buf.write("9:\7%\2\2:\30\3\2\2\2;<\7$\2\2<\32\3\2\2\2=?\t\5\2\2>")
        buf.write("=\3\2\2\2?@\3\2\2\2@>\3\2\2\2@A\3\2\2\2AB\3\2\2\2BC\b")
        buf.write("\16\2\2C\34\3\2\2\2DE\13\2\2\2E\36\3\2\2\2\6\2.\63@\3")
        buf.write("\b\2\2")
        return buf.getvalue()


class RegGLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    LEFT_ANGLE = 1
    RIGHT_ANGLE = 2
    APOSTROPHE = 3
    UNDERSCORE = 4
    TERMINAL = 5
    CAPS = 6
    ARROW = 7
    EPSILON = 8
    DELIMITER = 9
    NEWLINE = 10
    HASH = 11
    QUOTE = 12
    WS = 13
    ANYCHAR = 14

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'<'", "'>'", "'''", "'_'", "'|'", "'#'", "'\"'" ]

    symbolicNames = [ "<INVALID>",
            "LEFT_ANGLE", "RIGHT_ANGLE", "APOSTROPHE", "UNDERSCORE", "TERMINAL", 
            "CAPS", "ARROW", "EPSILON", "DELIMITER", "NEWLINE", "HASH", 
            "QUOTE", "WS", "ANYCHAR" ]

    ruleNames = [ "LEFT_ANGLE", "RIGHT_ANGLE", "APOSTROPHE", "UNDERSCORE", 
                  "TERMINAL", "CAPS", "ARROW", "EPSILON", "DELIMITER", "NEWLINE", 
                  "HASH", "QUOTE", "WS", "ANYCHAR" ]

    grammarFileName = "RegG.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


