# Generated from lib/parser/NFA.g4 by ANTLR 4.9.2
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\20")
        buf.write("Z\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b")
        buf.write("\t\b\4\t\t\t\3\2\3\2\3\2\3\2\3\2\5\2\30\n\2\3\2\3\2\3")
        buf.write("\2\3\2\3\3\3\3\7\3 \n\3\f\3\16\3#\13\3\3\3\3\3\3\3\3\4")
        buf.write("\3\4\3\4\3\4\5\4,\n\4\3\5\3\5\3\5\3\5\7\5\62\n\5\f\5\16")
        buf.write("\5\65\13\5\3\5\5\58\n\5\3\5\3\5\3\6\3\6\3\6\3\6\3\7\3")
        buf.write("\7\3\7\3\7\3\7\6\7E\n\7\r\7\16\7F\3\7\3\7\5\7K\n\7\3\b")
        buf.write("\3\b\3\b\7\bP\n\b\f\b\16\bS\13\b\3\b\5\bV\n\b\3\t\3\t")
        buf.write("\3\t\2\2\n\2\4\6\b\n\f\16\20\2\3\4\2\3\r\20\20\2]\2\22")
        buf.write("\3\2\2\2\4\35\3\2\2\2\6+\3\2\2\2\b-\3\2\2\2\n;\3\2\2\2")
        buf.write("\fJ\3\2\2\2\16U\3\2\2\2\20W\3\2\2\2\22\23\7\5\2\2\23\24")
        buf.write("\5\f\7\2\24\27\7\t\2\2\25\30\5\f\7\2\26\30\7\13\2\2\27")
        buf.write("\25\3\2\2\2\27\26\3\2\2\2\30\31\3\2\2\2\31\32\7\6\2\2")
        buf.write("\32\33\7\4\2\2\33\34\5\b\5\2\34\3\3\2\2\2\35!\5\6\4\2")
        buf.write("\36 \5\2\2\2\37\36\3\2\2\2 #\3\2\2\2!\37\3\2\2\2!\"\3")
        buf.write("\2\2\2\"$\3\2\2\2#!\3\2\2\2$%\5\n\6\2%&\5\16\b\2&\5\3")
        buf.write("\2\2\2\'(\7\3\2\2()\7\4\2\2),\5\f\7\2*,\3\2\2\2+\'\3\2")
        buf.write("\2\2+*\3\2\2\2,\7\3\2\2\2-\67\7\7\2\2.\63\5\f\7\2/\60")
        buf.write("\7\t\2\2\60\62\5\f\7\2\61/\3\2\2\2\62\65\3\2\2\2\63\61")
        buf.write("\3\2\2\2\63\64\3\2\2\2\648\3\2\2\2\65\63\3\2\2\2\668\3")
        buf.write("\2\2\2\67.\3\2\2\2\67\66\3\2\2\289\3\2\2\29:\7\b\2\2:")
        buf.write("\t\3\2\2\2;<\7\n\2\2<=\7\4\2\2=>\5\b\5\2>\13\3\2\2\2?")
        buf.write("K\7\3\2\2@K\7\n\2\2AK\7\f\2\2BD\7\16\2\2CE\5\20\t\2DC")
        buf.write("\3\2\2\2EF\3\2\2\2FD\3\2\2\2FG\3\2\2\2GH\3\2\2\2HI\7\16")
        buf.write("\2\2IK\3\2\2\2J?\3\2\2\2J@\3\2\2\2JA\3\2\2\2JB\3\2\2\2")
        buf.write("K\r\3\2\2\2LQ\7\r\2\2MP\5\20\t\2NP\7\16\2\2OM\3\2\2\2")
        buf.write("ON\3\2\2\2PS\3\2\2\2QO\3\2\2\2QR\3\2\2\2RV\3\2\2\2SQ\3")
        buf.write("\2\2\2TV\3\2\2\2UL\3\2\2\2UT\3\2\2\2V\17\3\2\2\2WX\t\2")
        buf.write("\2\2X\21\3\2\2\2\f\27!+\63\67FJOQU")
        return buf.getvalue()


class NFAParser ( Parser ):

    grammarFileName = "NFA.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'init'", "'='", "'('", "')'", "'{'", 
                     "'}'", "','", "'final'", "<INVALID>", "<INVALID>", 
                     "'#'", "'\"'" ]

    symbolicNames = [ "<INVALID>", "INIT", "EQUALS", "LEFT_PARENTHESIS", 
                      "RIGHT_PARENTHESIS", "LEFT_BRACKET", "RIGHT_BRACKET", 
                      "COMMA", "FINAL", "EPSILON", "STATE", "HASH", "QUOTE", 
                      "WS", "ANYCHAR" ]

    RULE_production = 0
    RULE_start = 1
    RULE_init = 2
    RULE_stateset = 3
    RULE_final = 4
    RULE_statename = 5
    RULE_comment = 6
    RULE_anyvalue = 7

    ruleNames =  [ "production", "start", "init", "stateset", "final", "statename", 
                   "comment", "anyvalue" ]

    EOF = Token.EOF
    INIT=1
    EQUALS=2
    LEFT_PARENTHESIS=3
    RIGHT_PARENTHESIS=4
    LEFT_BRACKET=5
    RIGHT_BRACKET=6
    COMMA=7
    FINAL=8
    EPSILON=9
    STATE=10
    HASH=11
    QUOTE=12
    WS=13
    ANYCHAR=14

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class ProductionContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LEFT_PARENTHESIS(self):
            return self.getToken(NFAParser.LEFT_PARENTHESIS, 0)

        def statename(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(NFAParser.StatenameContext)
            else:
                return self.getTypedRuleContext(NFAParser.StatenameContext,i)


        def COMMA(self):
            return self.getToken(NFAParser.COMMA, 0)

        def RIGHT_PARENTHESIS(self):
            return self.getToken(NFAParser.RIGHT_PARENTHESIS, 0)

        def EQUALS(self):
            return self.getToken(NFAParser.EQUALS, 0)

        def stateset(self):
            return self.getTypedRuleContext(NFAParser.StatesetContext,0)


        def EPSILON(self):
            return self.getToken(NFAParser.EPSILON, 0)

        def getRuleIndex(self):
            return NFAParser.RULE_production

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterProduction" ):
                listener.enterProduction(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitProduction" ):
                listener.exitProduction(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitProduction" ):
                return visitor.visitProduction(self)
            else:
                return visitor.visitChildren(self)




    def production(self):

        localctx = NFAParser.ProductionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_production)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 16
            self.match(NFAParser.LEFT_PARENTHESIS)
            self.state = 17
            self.statename()
            self.state = 18
            self.match(NFAParser.COMMA)
            self.state = 21
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [NFAParser.INIT, NFAParser.FINAL, NFAParser.STATE, NFAParser.QUOTE]:
                self.state = 19
                self.statename()
                pass
            elif token in [NFAParser.EPSILON]:
                self.state = 20
                self.match(NFAParser.EPSILON)
                pass
            else:
                raise NoViableAltException(self)

            self.state = 23
            self.match(NFAParser.RIGHT_PARENTHESIS)
            self.state = 24
            self.match(NFAParser.EQUALS)
            self.state = 25
            self.stateset()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StartContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def init(self):
            return self.getTypedRuleContext(NFAParser.InitContext,0)


        def final(self):
            return self.getTypedRuleContext(NFAParser.FinalContext,0)


        def comment(self):
            return self.getTypedRuleContext(NFAParser.CommentContext,0)


        def production(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(NFAParser.ProductionContext)
            else:
                return self.getTypedRuleContext(NFAParser.ProductionContext,i)


        def getRuleIndex(self):
            return NFAParser.RULE_start

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStart" ):
                listener.enterStart(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStart" ):
                listener.exitStart(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStart" ):
                return visitor.visitStart(self)
            else:
                return visitor.visitChildren(self)




    def start(self):

        localctx = NFAParser.StartContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_start)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 27
            self.init()
            self.state = 31
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==NFAParser.LEFT_PARENTHESIS:
                self.state = 28
                self.production()
                self.state = 33
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 34
            self.final()
            self.state = 35
            self.comment()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class InitContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INIT(self):
            return self.getToken(NFAParser.INIT, 0)

        def EQUALS(self):
            return self.getToken(NFAParser.EQUALS, 0)

        def statename(self):
            return self.getTypedRuleContext(NFAParser.StatenameContext,0)


        def getRuleIndex(self):
            return NFAParser.RULE_init

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInit" ):
                listener.enterInit(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInit" ):
                listener.exitInit(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInit" ):
                return visitor.visitInit(self)
            else:
                return visitor.visitChildren(self)




    def init(self):

        localctx = NFAParser.InitContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_init)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 41
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [NFAParser.INIT]:
                self.state = 37
                self.match(NFAParser.INIT)
                self.state = 38
                self.match(NFAParser.EQUALS)
                self.state = 39
                self.statename()
                pass
            elif token in [NFAParser.LEFT_PARENTHESIS, NFAParser.FINAL]:
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StatesetContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LEFT_BRACKET(self):
            return self.getToken(NFAParser.LEFT_BRACKET, 0)

        def RIGHT_BRACKET(self):
            return self.getToken(NFAParser.RIGHT_BRACKET, 0)

        def statename(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(NFAParser.StatenameContext)
            else:
                return self.getTypedRuleContext(NFAParser.StatenameContext,i)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(NFAParser.COMMA)
            else:
                return self.getToken(NFAParser.COMMA, i)

        def getRuleIndex(self):
            return NFAParser.RULE_stateset

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStateset" ):
                listener.enterStateset(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStateset" ):
                listener.exitStateset(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStateset" ):
                return visitor.visitStateset(self)
            else:
                return visitor.visitChildren(self)




    def stateset(self):

        localctx = NFAParser.StatesetContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_stateset)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 43
            self.match(NFAParser.LEFT_BRACKET)
            self.state = 53
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [NFAParser.INIT, NFAParser.FINAL, NFAParser.STATE, NFAParser.QUOTE]:
                self.state = 44
                self.statename()
                self.state = 49
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==NFAParser.COMMA:
                    self.state = 45
                    self.match(NFAParser.COMMA)
                    self.state = 46
                    self.statename()
                    self.state = 51
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                pass
            elif token in [NFAParser.RIGHT_BRACKET]:
                pass
            else:
                raise NoViableAltException(self)

            self.state = 55
            self.match(NFAParser.RIGHT_BRACKET)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FinalContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def FINAL(self):
            return self.getToken(NFAParser.FINAL, 0)

        def EQUALS(self):
            return self.getToken(NFAParser.EQUALS, 0)

        def stateset(self):
            return self.getTypedRuleContext(NFAParser.StatesetContext,0)


        def getRuleIndex(self):
            return NFAParser.RULE_final

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFinal" ):
                listener.enterFinal(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFinal" ):
                listener.exitFinal(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFinal" ):
                return visitor.visitFinal(self)
            else:
                return visitor.visitChildren(self)




    def final(self):

        localctx = NFAParser.FinalContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_final)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 57
            self.match(NFAParser.FINAL)
            self.state = 58
            self.match(NFAParser.EQUALS)
            self.state = 59
            self.stateset()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StatenameContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INIT(self):
            return self.getToken(NFAParser.INIT, 0)

        def FINAL(self):
            return self.getToken(NFAParser.FINAL, 0)

        def STATE(self):
            return self.getToken(NFAParser.STATE, 0)

        def QUOTE(self, i:int=None):
            if i is None:
                return self.getTokens(NFAParser.QUOTE)
            else:
                return self.getToken(NFAParser.QUOTE, i)

        def anyvalue(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(NFAParser.AnyvalueContext)
            else:
                return self.getTypedRuleContext(NFAParser.AnyvalueContext,i)


        def getRuleIndex(self):
            return NFAParser.RULE_statename

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStatename" ):
                listener.enterStatename(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStatename" ):
                listener.exitStatename(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStatename" ):
                return visitor.visitStatename(self)
            else:
                return visitor.visitChildren(self)




    def statename(self):

        localctx = NFAParser.StatenameContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_statename)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 72
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [NFAParser.INIT]:
                self.state = 61
                self.match(NFAParser.INIT)
                pass
            elif token in [NFAParser.FINAL]:
                self.state = 62
                self.match(NFAParser.FINAL)
                pass
            elif token in [NFAParser.STATE]:
                self.state = 63
                self.match(NFAParser.STATE)
                pass
            elif token in [NFAParser.QUOTE]:
                self.state = 64
                self.match(NFAParser.QUOTE)
                self.state = 66 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 65
                    self.anyvalue()
                    self.state = 68 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << NFAParser.INIT) | (1 << NFAParser.EQUALS) | (1 << NFAParser.LEFT_PARENTHESIS) | (1 << NFAParser.RIGHT_PARENTHESIS) | (1 << NFAParser.LEFT_BRACKET) | (1 << NFAParser.RIGHT_BRACKET) | (1 << NFAParser.COMMA) | (1 << NFAParser.FINAL) | (1 << NFAParser.EPSILON) | (1 << NFAParser.STATE) | (1 << NFAParser.HASH) | (1 << NFAParser.ANYCHAR))) != 0)):
                        break

                self.state = 70
                self.match(NFAParser.QUOTE)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class CommentContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def HASH(self):
            return self.getToken(NFAParser.HASH, 0)

        def anyvalue(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(NFAParser.AnyvalueContext)
            else:
                return self.getTypedRuleContext(NFAParser.AnyvalueContext,i)


        def QUOTE(self, i:int=None):
            if i is None:
                return self.getTokens(NFAParser.QUOTE)
            else:
                return self.getToken(NFAParser.QUOTE, i)

        def getRuleIndex(self):
            return NFAParser.RULE_comment

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterComment" ):
                listener.enterComment(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitComment" ):
                listener.exitComment(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitComment" ):
                return visitor.visitComment(self)
            else:
                return visitor.visitChildren(self)




    def comment(self):

        localctx = NFAParser.CommentContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_comment)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 83
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [NFAParser.HASH]:
                self.state = 74
                self.match(NFAParser.HASH)
                self.state = 79
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << NFAParser.INIT) | (1 << NFAParser.EQUALS) | (1 << NFAParser.LEFT_PARENTHESIS) | (1 << NFAParser.RIGHT_PARENTHESIS) | (1 << NFAParser.LEFT_BRACKET) | (1 << NFAParser.RIGHT_BRACKET) | (1 << NFAParser.COMMA) | (1 << NFAParser.FINAL) | (1 << NFAParser.EPSILON) | (1 << NFAParser.STATE) | (1 << NFAParser.HASH) | (1 << NFAParser.QUOTE) | (1 << NFAParser.ANYCHAR))) != 0):
                    self.state = 77
                    self._errHandler.sync(self)
                    token = self._input.LA(1)
                    if token in [NFAParser.INIT, NFAParser.EQUALS, NFAParser.LEFT_PARENTHESIS, NFAParser.RIGHT_PARENTHESIS, NFAParser.LEFT_BRACKET, NFAParser.RIGHT_BRACKET, NFAParser.COMMA, NFAParser.FINAL, NFAParser.EPSILON, NFAParser.STATE, NFAParser.HASH, NFAParser.ANYCHAR]:
                        self.state = 75
                        self.anyvalue()
                        pass
                    elif token in [NFAParser.QUOTE]:
                        self.state = 76
                        self.match(NFAParser.QUOTE)
                        pass
                    else:
                        raise NoViableAltException(self)

                    self.state = 81
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                pass
            elif token in [NFAParser.EOF]:
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AnyvalueContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INIT(self):
            return self.getToken(NFAParser.INIT, 0)

        def EQUALS(self):
            return self.getToken(NFAParser.EQUALS, 0)

        def LEFT_PARENTHESIS(self):
            return self.getToken(NFAParser.LEFT_PARENTHESIS, 0)

        def RIGHT_PARENTHESIS(self):
            return self.getToken(NFAParser.RIGHT_PARENTHESIS, 0)

        def LEFT_BRACKET(self):
            return self.getToken(NFAParser.LEFT_BRACKET, 0)

        def RIGHT_BRACKET(self):
            return self.getToken(NFAParser.RIGHT_BRACKET, 0)

        def COMMA(self):
            return self.getToken(NFAParser.COMMA, 0)

        def FINAL(self):
            return self.getToken(NFAParser.FINAL, 0)

        def EPSILON(self):
            return self.getToken(NFAParser.EPSILON, 0)

        def STATE(self):
            return self.getToken(NFAParser.STATE, 0)

        def HASH(self):
            return self.getToken(NFAParser.HASH, 0)

        def ANYCHAR(self):
            return self.getToken(NFAParser.ANYCHAR, 0)

        def getRuleIndex(self):
            return NFAParser.RULE_anyvalue

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAnyvalue" ):
                listener.enterAnyvalue(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAnyvalue" ):
                listener.exitAnyvalue(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAnyvalue" ):
                return visitor.visitAnyvalue(self)
            else:
                return visitor.visitChildren(self)




    def anyvalue(self):

        localctx = NFAParser.AnyvalueContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_anyvalue)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 85
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << NFAParser.INIT) | (1 << NFAParser.EQUALS) | (1 << NFAParser.LEFT_PARENTHESIS) | (1 << NFAParser.RIGHT_PARENTHESIS) | (1 << NFAParser.LEFT_BRACKET) | (1 << NFAParser.RIGHT_BRACKET) | (1 << NFAParser.COMMA) | (1 << NFAParser.FINAL) | (1 << NFAParser.EPSILON) | (1 << NFAParser.STATE) | (1 << NFAParser.HASH) | (1 << NFAParser.ANYCHAR))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





