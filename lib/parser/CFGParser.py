# Generated from lib/parser/CFG.g4 by ANTLR 4.9.2
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\20")
        buf.write("r\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b")
        buf.write("\t\b\4\t\t\t\4\n\t\n\3\2\3\2\6\2\27\n\2\r\2\16\2\30\7")
        buf.write("\2\33\n\2\f\2\16\2\36\13\2\3\2\3\2\6\2\"\n\2\r\2\16\2")
        buf.write("#\3\2\5\2\'\n\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\7\3\60\n\3")
        buf.write("\f\3\16\3\63\13\3\3\3\3\3\3\4\6\48\n\4\r\4\16\49\3\4\5")
        buf.write("\4=\n\4\3\5\3\5\5\5A\n\5\3\6\3\6\3\6\6\6F\n\6\r\6\16\6")
        buf.write("G\3\6\3\6\5\6L\n\6\3\7\3\7\3\7\6\7Q\n\7\r\7\16\7R\3\7")
        buf.write("\3\7\7\7W\n\7\f\7\16\7Z\13\7\3\7\3\7\6\7^\n\7\r\7\16\7")
        buf.write("_\5\7b\n\7\3\b\3\b\3\t\3\t\7\th\n\t\f\t\16\tk\13\t\3\t")
        buf.write("\5\tn\n\t\3\n\3\n\3\n\2\2\13\2\4\6\b\n\f\16\20\22\2\4")
        buf.write("\3\2\6\b\5\2\3\13\r\r\20\20\2y\2\34\3\2\2\2\4*\3\2\2\2")
        buf.write("\6<\3\2\2\2\b@\3\2\2\2\nK\3\2\2\2\fa\3\2\2\2\16c\3\2\2")
        buf.write("\2\20m\3\2\2\2\22o\3\2\2\2\24\26\5\4\3\2\25\27\7\f\2\2")
        buf.write("\26\25\3\2\2\2\27\30\3\2\2\2\30\26\3\2\2\2\30\31\3\2\2")
        buf.write("\2\31\33\3\2\2\2\32\24\3\2\2\2\33\36\3\2\2\2\34\32\3\2")
        buf.write("\2\2\34\35\3\2\2\2\35\37\3\2\2\2\36\34\3\2\2\2\37&\5\4")
        buf.write("\3\2 \"\7\f\2\2! \3\2\2\2\"#\3\2\2\2#!\3\2\2\2#$\3\2\2")
        buf.write("\2$\'\3\2\2\2%\'\3\2\2\2&!\3\2\2\2&%\3\2\2\2\'(\3\2\2")
        buf.write("\2()\5\20\t\2)\3\3\2\2\2*+\5\f\7\2+\61\7\t\2\2,-\5\6\4")
        buf.write("\2-.\7\13\2\2.\60\3\2\2\2/,\3\2\2\2\60\63\3\2\2\2\61/")
        buf.write("\3\2\2\2\61\62\3\2\2\2\62\64\3\2\2\2\63\61\3\2\2\2\64")
        buf.write("\65\5\6\4\2\65\5\3\2\2\2\668\5\b\5\2\67\66\3\2\2\289\3")
        buf.write("\2\2\29\67\3\2\2\29:\3\2\2\2:=\3\2\2\2;=\7\n\2\2<\67\3")
        buf.write("\2\2\2<;\3\2\2\2=\7\3\2\2\2>A\5\n\6\2?A\5\f\7\2@>\3\2")
        buf.write("\2\2@?\3\2\2\2A\t\3\2\2\2BL\7\7\2\2CE\7\16\2\2DF\5\22")
        buf.write("\n\2ED\3\2\2\2FG\3\2\2\2GE\3\2\2\2GH\3\2\2\2HI\3\2\2\2")
        buf.write("IJ\7\16\2\2JL\3\2\2\2KB\3\2\2\2KC\3\2\2\2L\13\3\2\2\2")
        buf.write("Mb\7\b\2\2NP\7\3\2\2OQ\5\16\b\2PO\3\2\2\2QR\3\2\2\2RP")
        buf.write("\3\2\2\2RS\3\2\2\2ST\3\2\2\2TX\7\4\2\2UW\7\5\2\2VU\3\2")
        buf.write("\2\2WZ\3\2\2\2XV\3\2\2\2XY\3\2\2\2Yb\3\2\2\2ZX\3\2\2\2")
        buf.write("[]\5\16\b\2\\^\7\5\2\2]\\\3\2\2\2^_\3\2\2\2_]\3\2\2\2")
        buf.write("_`\3\2\2\2`b\3\2\2\2aM\3\2\2\2aN\3\2\2\2a[\3\2\2\2b\r")
        buf.write("\3\2\2\2cd\t\2\2\2d\17\3\2\2\2ei\7\r\2\2fh\5\22\n\2gf")
        buf.write("\3\2\2\2hk\3\2\2\2ig\3\2\2\2ij\3\2\2\2jn\3\2\2\2ki\3\2")
        buf.write("\2\2ln\3\2\2\2me\3\2\2\2ml\3\2\2\2n\21\3\2\2\2op\t\3\2")
        buf.write("\2p\23\3\2\2\2\22\30\34#&\619<@GKRX_aim")
        return buf.getvalue()


class CFGParser ( Parser ):

    grammarFileName = "CFG.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'<'", "'>'", "'''", "'_'", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "'|'", "<INVALID>", 
                     "'#'", "'\"'" ]

    symbolicNames = [ "<INVALID>", "LEFT_ANGLE", "RIGHT_ANGLE", "APOSTROPHE", 
                      "UNDERSCORE", "TERMINAL", "CAPS", "ARROW", "EPSILON", 
                      "DELIMITER", "NEWLINE", "HASH", "QUOTE", "WS", "ANYCHAR" ]

    RULE_start = 0
    RULE_onerule = 1
    RULE_rewrite = 2
    RULE_term_or_nonterm = 3
    RULE_term = 4
    RULE_nonterm = 5
    RULE_symbol = 6
    RULE_comment = 7
    RULE_anyvalue = 8

    ruleNames =  [ "start", "onerule", "rewrite", "term_or_nonterm", "term", 
                   "nonterm", "symbol", "comment", "anyvalue" ]

    EOF = Token.EOF
    LEFT_ANGLE=1
    RIGHT_ANGLE=2
    APOSTROPHE=3
    UNDERSCORE=4
    TERMINAL=5
    CAPS=6
    ARROW=7
    EPSILON=8
    DELIMITER=9
    NEWLINE=10
    HASH=11
    QUOTE=12
    WS=13
    ANYCHAR=14

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class StartContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def onerule(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CFGParser.OneruleContext)
            else:
                return self.getTypedRuleContext(CFGParser.OneruleContext,i)


        def comment(self):
            return self.getTypedRuleContext(CFGParser.CommentContext,0)


        def NEWLINE(self, i:int=None):
            if i is None:
                return self.getTokens(CFGParser.NEWLINE)
            else:
                return self.getToken(CFGParser.NEWLINE, i)

        def getRuleIndex(self):
            return CFGParser.RULE_start

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStart" ):
                listener.enterStart(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStart" ):
                listener.exitStart(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStart" ):
                return visitor.visitStart(self)
            else:
                return visitor.visitChildren(self)




    def start(self):

        localctx = CFGParser.StartContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_start)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 26
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,1,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 18
                    self.onerule()
                    self.state = 20 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while True:
                        self.state = 19
                        self.match(CFGParser.NEWLINE)
                        self.state = 22 
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)
                        if not (_la==CFGParser.NEWLINE):
                            break
             
                self.state = 28
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,1,self._ctx)

            self.state = 29
            self.onerule()
            self.state = 36
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [CFGParser.NEWLINE]:
                self.state = 31 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 30
                    self.match(CFGParser.NEWLINE)
                    self.state = 33 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not (_la==CFGParser.NEWLINE):
                        break

                pass
            elif token in [CFGParser.EOF, CFGParser.HASH]:
                pass
            else:
                raise NoViableAltException(self)

            self.state = 38
            self.comment()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class OneruleContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def nonterm(self):
            return self.getTypedRuleContext(CFGParser.NontermContext,0)


        def ARROW(self):
            return self.getToken(CFGParser.ARROW, 0)

        def rewrite(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CFGParser.RewriteContext)
            else:
                return self.getTypedRuleContext(CFGParser.RewriteContext,i)


        def DELIMITER(self, i:int=None):
            if i is None:
                return self.getTokens(CFGParser.DELIMITER)
            else:
                return self.getToken(CFGParser.DELIMITER, i)

        def getRuleIndex(self):
            return CFGParser.RULE_onerule

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterOnerule" ):
                listener.enterOnerule(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitOnerule" ):
                listener.exitOnerule(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitOnerule" ):
                return visitor.visitOnerule(self)
            else:
                return visitor.visitChildren(self)




    def onerule(self):

        localctx = CFGParser.OneruleContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_onerule)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 40
            self.nonterm()
            self.state = 41
            self.match(CFGParser.ARROW)
            self.state = 47
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,4,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 42
                    self.rewrite()
                    self.state = 43
                    self.match(CFGParser.DELIMITER) 
                self.state = 49
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,4,self._ctx)

            self.state = 50
            self.rewrite()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class RewriteContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def EPSILON(self):
            return self.getToken(CFGParser.EPSILON, 0)

        def term_or_nonterm(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CFGParser.Term_or_nontermContext)
            else:
                return self.getTypedRuleContext(CFGParser.Term_or_nontermContext,i)


        def getRuleIndex(self):
            return CFGParser.RULE_rewrite

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRewrite" ):
                listener.enterRewrite(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRewrite" ):
                listener.exitRewrite(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRewrite" ):
                return visitor.visitRewrite(self)
            else:
                return visitor.visitChildren(self)




    def rewrite(self):

        localctx = CFGParser.RewriteContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_rewrite)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 58
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [CFGParser.LEFT_ANGLE, CFGParser.UNDERSCORE, CFGParser.TERMINAL, CFGParser.CAPS, CFGParser.QUOTE]:
                self.state = 53 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 52
                    self.term_or_nonterm()
                    self.state = 55 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << CFGParser.LEFT_ANGLE) | (1 << CFGParser.UNDERSCORE) | (1 << CFGParser.TERMINAL) | (1 << CFGParser.CAPS) | (1 << CFGParser.QUOTE))) != 0)):
                        break

                pass
            elif token in [CFGParser.EPSILON]:
                self.state = 57
                self.match(CFGParser.EPSILON)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Term_or_nontermContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def term(self):
            return self.getTypedRuleContext(CFGParser.TermContext,0)


        def nonterm(self):
            return self.getTypedRuleContext(CFGParser.NontermContext,0)


        def getRuleIndex(self):
            return CFGParser.RULE_term_or_nonterm

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTerm_or_nonterm" ):
                listener.enterTerm_or_nonterm(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTerm_or_nonterm" ):
                listener.exitTerm_or_nonterm(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTerm_or_nonterm" ):
                return visitor.visitTerm_or_nonterm(self)
            else:
                return visitor.visitChildren(self)




    def term_or_nonterm(self):

        localctx = CFGParser.Term_or_nontermContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_term_or_nonterm)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 62
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,7,self._ctx)
            if la_ == 1:
                self.state = 60
                self.term()
                pass

            elif la_ == 2:
                self.state = 61
                self.nonterm()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TermContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def TERMINAL(self):
            return self.getToken(CFGParser.TERMINAL, 0)

        def QUOTE(self, i:int=None):
            if i is None:
                return self.getTokens(CFGParser.QUOTE)
            else:
                return self.getToken(CFGParser.QUOTE, i)

        def anyvalue(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CFGParser.AnyvalueContext)
            else:
                return self.getTypedRuleContext(CFGParser.AnyvalueContext,i)


        def getRuleIndex(self):
            return CFGParser.RULE_term

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTerm" ):
                listener.enterTerm(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTerm" ):
                listener.exitTerm(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTerm" ):
                return visitor.visitTerm(self)
            else:
                return visitor.visitChildren(self)




    def term(self):

        localctx = CFGParser.TermContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_term)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 73
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [CFGParser.TERMINAL]:
                self.state = 64
                self.match(CFGParser.TERMINAL)
                pass
            elif token in [CFGParser.QUOTE]:
                self.state = 65
                self.match(CFGParser.QUOTE)
                self.state = 67 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 66
                    self.anyvalue()
                    self.state = 69 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << CFGParser.LEFT_ANGLE) | (1 << CFGParser.RIGHT_ANGLE) | (1 << CFGParser.APOSTROPHE) | (1 << CFGParser.UNDERSCORE) | (1 << CFGParser.TERMINAL) | (1 << CFGParser.CAPS) | (1 << CFGParser.ARROW) | (1 << CFGParser.EPSILON) | (1 << CFGParser.DELIMITER) | (1 << CFGParser.HASH) | (1 << CFGParser.ANYCHAR))) != 0)):
                        break

                self.state = 71
                self.match(CFGParser.QUOTE)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class NontermContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def CAPS(self):
            return self.getToken(CFGParser.CAPS, 0)

        def LEFT_ANGLE(self):
            return self.getToken(CFGParser.LEFT_ANGLE, 0)

        def RIGHT_ANGLE(self):
            return self.getToken(CFGParser.RIGHT_ANGLE, 0)

        def symbol(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CFGParser.SymbolContext)
            else:
                return self.getTypedRuleContext(CFGParser.SymbolContext,i)


        def APOSTROPHE(self, i:int=None):
            if i is None:
                return self.getTokens(CFGParser.APOSTROPHE)
            else:
                return self.getToken(CFGParser.APOSTROPHE, i)

        def getRuleIndex(self):
            return CFGParser.RULE_nonterm

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNonterm" ):
                listener.enterNonterm(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNonterm" ):
                listener.exitNonterm(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitNonterm" ):
                return visitor.visitNonterm(self)
            else:
                return visitor.visitChildren(self)




    def nonterm(self):

        localctx = CFGParser.NontermContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_nonterm)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 95
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,13,self._ctx)
            if la_ == 1:
                self.state = 75
                self.match(CFGParser.CAPS)
                pass

            elif la_ == 2:
                self.state = 76
                self.match(CFGParser.LEFT_ANGLE)
                self.state = 78 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 77
                    self.symbol()
                    self.state = 80 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << CFGParser.UNDERSCORE) | (1 << CFGParser.TERMINAL) | (1 << CFGParser.CAPS))) != 0)):
                        break

                self.state = 82
                self.match(CFGParser.RIGHT_ANGLE)

                self.state = 86
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==CFGParser.APOSTROPHE:
                    self.state = 83
                    self.match(CFGParser.APOSTROPHE)
                    self.state = 88
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                pass

            elif la_ == 3:
                self.state = 89
                self.symbol()
                self.state = 91 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 90
                    self.match(CFGParser.APOSTROPHE)
                    self.state = 93 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not (_la==CFGParser.APOSTROPHE):
                        break

                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class SymbolContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def TERMINAL(self):
            return self.getToken(CFGParser.TERMINAL, 0)

        def CAPS(self):
            return self.getToken(CFGParser.CAPS, 0)

        def UNDERSCORE(self):
            return self.getToken(CFGParser.UNDERSCORE, 0)

        def getRuleIndex(self):
            return CFGParser.RULE_symbol

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSymbol" ):
                listener.enterSymbol(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSymbol" ):
                listener.exitSymbol(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSymbol" ):
                return visitor.visitSymbol(self)
            else:
                return visitor.visitChildren(self)




    def symbol(self):

        localctx = CFGParser.SymbolContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_symbol)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 97
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << CFGParser.UNDERSCORE) | (1 << CFGParser.TERMINAL) | (1 << CFGParser.CAPS))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class CommentContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def HASH(self):
            return self.getToken(CFGParser.HASH, 0)

        def anyvalue(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CFGParser.AnyvalueContext)
            else:
                return self.getTypedRuleContext(CFGParser.AnyvalueContext,i)


        def getRuleIndex(self):
            return CFGParser.RULE_comment

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterComment" ):
                listener.enterComment(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitComment" ):
                listener.exitComment(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitComment" ):
                return visitor.visitComment(self)
            else:
                return visitor.visitChildren(self)




    def comment(self):

        localctx = CFGParser.CommentContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_comment)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 107
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [CFGParser.HASH]:
                self.state = 99
                self.match(CFGParser.HASH)
                self.state = 103
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << CFGParser.LEFT_ANGLE) | (1 << CFGParser.RIGHT_ANGLE) | (1 << CFGParser.APOSTROPHE) | (1 << CFGParser.UNDERSCORE) | (1 << CFGParser.TERMINAL) | (1 << CFGParser.CAPS) | (1 << CFGParser.ARROW) | (1 << CFGParser.EPSILON) | (1 << CFGParser.DELIMITER) | (1 << CFGParser.HASH) | (1 << CFGParser.ANYCHAR))) != 0):
                    self.state = 100
                    self.anyvalue()
                    self.state = 105
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                pass
            elif token in [CFGParser.EOF]:
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AnyvalueContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LEFT_ANGLE(self):
            return self.getToken(CFGParser.LEFT_ANGLE, 0)

        def RIGHT_ANGLE(self):
            return self.getToken(CFGParser.RIGHT_ANGLE, 0)

        def APOSTROPHE(self):
            return self.getToken(CFGParser.APOSTROPHE, 0)

        def UNDERSCORE(self):
            return self.getToken(CFGParser.UNDERSCORE, 0)

        def TERMINAL(self):
            return self.getToken(CFGParser.TERMINAL, 0)

        def CAPS(self):
            return self.getToken(CFGParser.CAPS, 0)

        def ARROW(self):
            return self.getToken(CFGParser.ARROW, 0)

        def EPSILON(self):
            return self.getToken(CFGParser.EPSILON, 0)

        def DELIMITER(self):
            return self.getToken(CFGParser.DELIMITER, 0)

        def ANYCHAR(self):
            return self.getToken(CFGParser.ANYCHAR, 0)

        def HASH(self):
            return self.getToken(CFGParser.HASH, 0)

        def getRuleIndex(self):
            return CFGParser.RULE_anyvalue

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAnyvalue" ):
                listener.enterAnyvalue(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAnyvalue" ):
                listener.exitAnyvalue(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAnyvalue" ):
                return visitor.visitAnyvalue(self)
            else:
                return visitor.visitChildren(self)




    def anyvalue(self):

        localctx = CFGParser.AnyvalueContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_anyvalue)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 109
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << CFGParser.LEFT_ANGLE) | (1 << CFGParser.RIGHT_ANGLE) | (1 << CFGParser.APOSTROPHE) | (1 << CFGParser.UNDERSCORE) | (1 << CFGParser.TERMINAL) | (1 << CFGParser.CAPS) | (1 << CFGParser.ARROW) | (1 << CFGParser.EPSILON) | (1 << CFGParser.DELIMITER) | (1 << CFGParser.HASH) | (1 << CFGParser.ANYCHAR))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





