# Generated from lib/parser/SET.g4 by ANTLR 4.9.2
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\31")
        buf.write("n\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b")
        buf.write("\t\b\4\t\t\t\4\n\t\n\3\2\3\2\3\2\5\2\30\n\2\3\2\3\2\3")
        buf.write("\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4")
        buf.write("\3\4\3\4\3\4\3\4\3\4\5\4/\n\4\5\4\61\n\4\3\5\3\5\3\5\3")
        buf.write("\5\3\5\5\58\n\5\3\5\3\5\3\5\3\5\3\5\3\5\7\5@\n\5\f\5\16")
        buf.write("\5C\13\5\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5")
        buf.write("\7P\n\7\3\7\3\7\5\7T\n\7\3\b\3\b\3\b\3\b\6\bZ\n\b\r\b")
        buf.write("\16\b[\3\b\3\b\5\b`\n\b\3\t\3\t\7\td\n\t\f\t\16\tg\13")
        buf.write("\t\3\t\5\tj\n\t\3\n\3\n\3\n\2\3\b\13\2\4\6\b\n\f\16\20")
        buf.write("\22\2\3\4\2\3\27\31\31\2v\2\24\3\2\2\2\4\33\3\2\2\2\6")
        buf.write("\60\3\2\2\2\b\67\3\2\2\2\nD\3\2\2\2\fS\3\2\2\2\16_\3\2")
        buf.write("\2\2\20i\3\2\2\2\22k\3\2\2\2\24\27\5\6\4\2\25\30\5\4\3")
        buf.write("\2\26\30\3\2\2\2\27\25\3\2\2\2\27\26\3\2\2\2\30\31\3\2")
        buf.write("\2\2\31\32\5\20\t\2\32\3\3\2\2\2\33\34\7\23\2\2\34\35")
        buf.write("\7\22\2\2\35\36\7\24\2\2\36\37\5\f\7\2\37\5\3\2\2\2 \61")
        buf.write("\5\b\5\2!.\5\b\5\2\"#\6\4\2\3#$\7\n\2\2$/\5\6\4\2%&\6")
        buf.write("\4\3\3&\'\7\f\2\2\'/\5\6\4\2()\6\4\4\3)*\7\13\2\2*/\5")
        buf.write("\6\4\2+,\6\4\5\3,-\7\25\2\2-/\5\6\4\2.\"\3\2\2\2.%\3\2")
        buf.write("\2\2.(\3\2\2\2.+\3\2\2\2/\61\3\2\2\2\60 \3\2\2\2\60!\3")
        buf.write("\2\2\2\61\7\3\2\2\2\62\63\b\5\1\2\638\5\n\6\2\648\5\f")
        buf.write("\7\2\65\66\7\r\2\2\668\5\b\5\6\67\62\3\2\2\2\67\64\3\2")
        buf.write("\2\2\67\65\3\2\2\28A\3\2\2\29:\f\5\2\2:@\7\16\2\2;<\f")
        buf.write("\4\2\2<@\7\b\2\2=>\f\3\2\2>@\7\t\2\2?9\3\2\2\2?;\3\2\2")
        buf.write("\2?=\3\2\2\2@C\3\2\2\2A?\3\2\2\2AB\3\2\2\2B\t\3\2\2\2")
        buf.write("CA\3\2\2\2DE\7\3\2\2EF\5\6\4\2FG\7\4\2\2G\13\3\2\2\2H")
        buf.write("O\7\5\2\2IP\3\2\2\2JP\5\16\b\2KL\5\16\b\2LM\7\7\2\2MN")
        buf.write("\5\16\b\2NP\3\2\2\2OI\3\2\2\2OJ\3\2\2\2OK\3\2\2\2PQ\3")
        buf.write("\2\2\2QT\7\6\2\2RT\7\21\2\2SH\3\2\2\2SR\3\2\2\2T\r\3\2")
        buf.write("\2\2U`\7\17\2\2V`\7\20\2\2WY\7\27\2\2XZ\5\22\n\2YX\3\2")
        buf.write("\2\2Z[\3\2\2\2[Y\3\2\2\2[\\\3\2\2\2\\]\3\2\2\2]^\7\27")
        buf.write("\2\2^`\3\2\2\2_U\3\2\2\2_V\3\2\2\2_W\3\2\2\2`\17\3\2\2")
        buf.write("\2ae\7\26\2\2bd\5\22\n\2cb\3\2\2\2dg\3\2\2\2ec\3\2\2\2")
        buf.write("ef\3\2\2\2fj\3\2\2\2ge\3\2\2\2hj\3\2\2\2ia\3\2\2\2ih\3")
        buf.write("\2\2\2j\21\3\2\2\2kl\t\2\2\2l\23\3\2\2\2\16\27.\60\67")
        buf.write("?AOS[_ei")
        return buf.getvalue()


class SETParser ( Parser ):

    grammarFileName = "SET.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'('", "')'", "'{'", "'}'", "','", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "'='", "<INVALID>", 
                     "'#'", "'\"'" ]

    symbolicNames = [ "<INVALID>", "LEFT_PAR", "RIGHT_PAR", "LEFT_BR", "RIGHT_BR", 
                      "COMMA", "ITER", "POS_ITER", "CONCAT", "UNION", "INTERS", 
                      "COMPL", "POS_COMPL", "ALPHABET", "EPSILON", "EMPTYSET", 
                      "SIGMA", "SIGSEP", "EQUALS", "DIFF", "HASH", "QUOTE", 
                      "WS", "ANYCHAR" ]

    RULE_start = 0
    RULE_alph = 1
    RULE_expr = 2
    RULE_unop = 3
    RULE_parentheses = 4
    RULE_atom_set = 5
    RULE_symbol = 6
    RULE_comment = 7
    RULE_anyvalue = 8

    ruleNames =  [ "start", "alph", "expr", "unop", "parentheses", "atom_set", 
                   "symbol", "comment", "anyvalue" ]

    EOF = Token.EOF
    LEFT_PAR=1
    RIGHT_PAR=2
    LEFT_BR=3
    RIGHT_BR=4
    COMMA=5
    ITER=6
    POS_ITER=7
    CONCAT=8
    UNION=9
    INTERS=10
    COMPL=11
    POS_COMPL=12
    ALPHABET=13
    EPSILON=14
    EMPTYSET=15
    SIGMA=16
    SIGSEP=17
    EQUALS=18
    DIFF=19
    HASH=20
    QUOTE=21
    WS=22
    ANYCHAR=23

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class StartContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expr(self):
            return self.getTypedRuleContext(SETParser.ExprContext,0)


        def comment(self):
            return self.getTypedRuleContext(SETParser.CommentContext,0)


        def alph(self):
            return self.getTypedRuleContext(SETParser.AlphContext,0)


        def getRuleIndex(self):
            return SETParser.RULE_start

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStart" ):
                listener.enterStart(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStart" ):
                listener.exitStart(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStart" ):
                return visitor.visitStart(self)
            else:
                return visitor.visitChildren(self)




    def start(self):

        localctx = SETParser.StartContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_start)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 18
            self.expr(0)
            self.state = 21
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [SETParser.SIGSEP]:
                self.state = 19
                self.alph()
                pass
            elif token in [SETParser.EOF, SETParser.HASH]:
                pass
            else:
                raise NoViableAltException(self)

            self.state = 23
            self.comment()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AlphContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def SIGSEP(self):
            return self.getToken(SETParser.SIGSEP, 0)

        def SIGMA(self):
            return self.getToken(SETParser.SIGMA, 0)

        def EQUALS(self):
            return self.getToken(SETParser.EQUALS, 0)

        def atom_set(self):
            return self.getTypedRuleContext(SETParser.Atom_setContext,0)


        def getRuleIndex(self):
            return SETParser.RULE_alph

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAlph" ):
                listener.enterAlph(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAlph" ):
                listener.exitAlph(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAlph" ):
                return visitor.visitAlph(self)
            else:
                return visitor.visitChildren(self)




    def alph(self):

        localctx = SETParser.AlphContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_alph)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 25
            self.match(SETParser.SIGSEP)
            self.state = 26
            self.match(SETParser.SIGMA)
            self.state = 27
            self.match(SETParser.EQUALS)
            self.state = 28
            self.atom_set()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExprContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1, pr:int=None):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.pr = None
            self.pr = pr

        def unop(self):
            return self.getTypedRuleContext(SETParser.UnopContext,0)


        def CONCAT(self):
            return self.getToken(SETParser.CONCAT, 0)

        def expr(self):
            return self.getTypedRuleContext(SETParser.ExprContext,0)


        def INTERS(self):
            return self.getToken(SETParser.INTERS, 0)

        def UNION(self):
            return self.getToken(SETParser.UNION, 0)

        def DIFF(self):
            return self.getToken(SETParser.DIFF, 0)

        def getRuleIndex(self):
            return SETParser.RULE_expr

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpr" ):
                listener.enterExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpr" ):
                listener.exitExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpr" ):
                return visitor.visitExpr(self)
            else:
                return visitor.visitChildren(self)




    def expr(self, pr:int):

        localctx = SETParser.ExprContext(self, self._ctx, self.state, pr)
        self.enterRule(localctx, 4, self.RULE_expr)
        try:
            self.state = 46
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,2,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 30
                self.unop(0)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 31
                self.unop(0)
                self.state = 44
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,1,self._ctx)
                if la_ == 1:
                    self.state = 32
                    if not 5 >= localctx.pr:
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "5 >= $pr")
                    self.state = 33
                    self.match(SETParser.CONCAT)
                    self.state = 34
                    self.expr(6)
                    pass

                elif la_ == 2:
                    self.state = 35
                    if not 4 >= localctx.pr:
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "4 >= $pr")
                    self.state = 36
                    self.match(SETParser.INTERS)
                    self.state = 37
                    self.expr(5)
                    pass

                elif la_ == 3:
                    self.state = 38
                    if not 3 >= localctx.pr:
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "3 >= $pr")
                    self.state = 39
                    self.match(SETParser.UNION)
                    self.state = 40
                    self.expr(4)
                    pass

                elif la_ == 4:
                    self.state = 41
                    if not 2 >= localctx.pr:
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "2 >= $pr")
                    self.state = 42
                    self.match(SETParser.DIFF)
                    self.state = 43
                    self.expr(3)
                    pass


                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class UnopContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def parentheses(self):
            return self.getTypedRuleContext(SETParser.ParenthesesContext,0)


        def atom_set(self):
            return self.getTypedRuleContext(SETParser.Atom_setContext,0)


        def COMPL(self):
            return self.getToken(SETParser.COMPL, 0)

        def unop(self):
            return self.getTypedRuleContext(SETParser.UnopContext,0)


        def POS_COMPL(self):
            return self.getToken(SETParser.POS_COMPL, 0)

        def ITER(self):
            return self.getToken(SETParser.ITER, 0)

        def POS_ITER(self):
            return self.getToken(SETParser.POS_ITER, 0)

        def getRuleIndex(self):
            return SETParser.RULE_unop

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterUnop" ):
                listener.enterUnop(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitUnop" ):
                listener.exitUnop(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitUnop" ):
                return visitor.visitUnop(self)
            else:
                return visitor.visitChildren(self)



    def unop(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = SETParser.UnopContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 6
        self.enterRecursionRule(localctx, 6, self.RULE_unop, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 53
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [SETParser.LEFT_PAR]:
                self.state = 49
                self.parentheses()
                pass
            elif token in [SETParser.LEFT_BR, SETParser.EMPTYSET]:
                self.state = 50
                self.atom_set()
                pass
            elif token in [SETParser.COMPL]:
                self.state = 51
                self.match(SETParser.COMPL)
                self.state = 52
                self.unop(4)
                pass
            else:
                raise NoViableAltException(self)

            self._ctx.stop = self._input.LT(-1)
            self.state = 63
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,5,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 61
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,4,self._ctx)
                    if la_ == 1:
                        localctx = SETParser.UnopContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_unop)
                        self.state = 55
                        if not self.precpred(self._ctx, 3):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 3)")
                        self.state = 56
                        self.match(SETParser.POS_COMPL)
                        pass

                    elif la_ == 2:
                        localctx = SETParser.UnopContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_unop)
                        self.state = 57
                        if not self.precpred(self._ctx, 2):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                        self.state = 58
                        self.match(SETParser.ITER)
                        pass

                    elif la_ == 3:
                        localctx = SETParser.UnopContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_unop)
                        self.state = 59
                        if not self.precpred(self._ctx, 1):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 1)")
                        self.state = 60
                        self.match(SETParser.POS_ITER)
                        pass

             
                self.state = 65
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,5,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class ParenthesesContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LEFT_PAR(self):
            return self.getToken(SETParser.LEFT_PAR, 0)

        def expr(self):
            return self.getTypedRuleContext(SETParser.ExprContext,0)


        def RIGHT_PAR(self):
            return self.getToken(SETParser.RIGHT_PAR, 0)

        def getRuleIndex(self):
            return SETParser.RULE_parentheses

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterParentheses" ):
                listener.enterParentheses(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitParentheses" ):
                listener.exitParentheses(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitParentheses" ):
                return visitor.visitParentheses(self)
            else:
                return visitor.visitChildren(self)




    def parentheses(self):

        localctx = SETParser.ParenthesesContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_parentheses)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 66
            self.match(SETParser.LEFT_PAR)
            self.state = 67
            self.expr(0)
            self.state = 68
            self.match(SETParser.RIGHT_PAR)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Atom_setContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LEFT_BR(self):
            return self.getToken(SETParser.LEFT_BR, 0)

        def RIGHT_BR(self):
            return self.getToken(SETParser.RIGHT_BR, 0)

        def symbol(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SETParser.SymbolContext)
            else:
                return self.getTypedRuleContext(SETParser.SymbolContext,i)


        def COMMA(self):
            return self.getToken(SETParser.COMMA, 0)

        def EMPTYSET(self):
            return self.getToken(SETParser.EMPTYSET, 0)

        def getRuleIndex(self):
            return SETParser.RULE_atom_set

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAtom_set" ):
                listener.enterAtom_set(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAtom_set" ):
                listener.exitAtom_set(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAtom_set" ):
                return visitor.visitAtom_set(self)
            else:
                return visitor.visitChildren(self)




    def atom_set(self):

        localctx = SETParser.Atom_setContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_atom_set)
        try:
            self.state = 81
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [SETParser.LEFT_BR]:
                self.enterOuterAlt(localctx, 1)
                self.state = 70
                self.match(SETParser.LEFT_BR)
                self.state = 77
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,6,self._ctx)
                if la_ == 1:
                    pass

                elif la_ == 2:
                    self.state = 72
                    self.symbol()
                    pass

                elif la_ == 3:
                    self.state = 73
                    self.symbol()

                    self.state = 74
                    self.match(SETParser.COMMA)
                    self.state = 75
                    self.symbol()
                    pass


                self.state = 79
                self.match(SETParser.RIGHT_BR)
                pass
            elif token in [SETParser.EMPTYSET]:
                self.enterOuterAlt(localctx, 2)
                self.state = 80
                self.match(SETParser.EMPTYSET)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class SymbolContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ALPHABET(self):
            return self.getToken(SETParser.ALPHABET, 0)

        def EPSILON(self):
            return self.getToken(SETParser.EPSILON, 0)

        def QUOTE(self, i:int=None):
            if i is None:
                return self.getTokens(SETParser.QUOTE)
            else:
                return self.getToken(SETParser.QUOTE, i)

        def anyvalue(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SETParser.AnyvalueContext)
            else:
                return self.getTypedRuleContext(SETParser.AnyvalueContext,i)


        def getRuleIndex(self):
            return SETParser.RULE_symbol

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSymbol" ):
                listener.enterSymbol(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSymbol" ):
                listener.exitSymbol(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSymbol" ):
                return visitor.visitSymbol(self)
            else:
                return visitor.visitChildren(self)




    def symbol(self):

        localctx = SETParser.SymbolContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_symbol)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 93
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [SETParser.ALPHABET]:
                self.state = 83
                self.match(SETParser.ALPHABET)
                pass
            elif token in [SETParser.EPSILON]:
                self.state = 84
                self.match(SETParser.EPSILON)
                pass
            elif token in [SETParser.QUOTE]:
                self.state = 85
                self.match(SETParser.QUOTE)
                self.state = 87 
                self._errHandler.sync(self)
                _alt = 1
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt == 1:
                        self.state = 86
                        self.anyvalue()

                    else:
                        raise NoViableAltException(self)
                    self.state = 89 
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,8,self._ctx)

                self.state = 91
                self.match(SETParser.QUOTE)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class CommentContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def HASH(self):
            return self.getToken(SETParser.HASH, 0)

        def anyvalue(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SETParser.AnyvalueContext)
            else:
                return self.getTypedRuleContext(SETParser.AnyvalueContext,i)


        def getRuleIndex(self):
            return SETParser.RULE_comment

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterComment" ):
                listener.enterComment(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitComment" ):
                listener.exitComment(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitComment" ):
                return visitor.visitComment(self)
            else:
                return visitor.visitChildren(self)




    def comment(self):

        localctx = SETParser.CommentContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_comment)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 103
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [SETParser.HASH]:
                self.state = 95
                self.match(SETParser.HASH)
                self.state = 99
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << SETParser.LEFT_PAR) | (1 << SETParser.RIGHT_PAR) | (1 << SETParser.LEFT_BR) | (1 << SETParser.RIGHT_BR) | (1 << SETParser.COMMA) | (1 << SETParser.ITER) | (1 << SETParser.POS_ITER) | (1 << SETParser.CONCAT) | (1 << SETParser.UNION) | (1 << SETParser.INTERS) | (1 << SETParser.COMPL) | (1 << SETParser.POS_COMPL) | (1 << SETParser.ALPHABET) | (1 << SETParser.EPSILON) | (1 << SETParser.EMPTYSET) | (1 << SETParser.SIGMA) | (1 << SETParser.SIGSEP) | (1 << SETParser.EQUALS) | (1 << SETParser.DIFF) | (1 << SETParser.HASH) | (1 << SETParser.QUOTE) | (1 << SETParser.ANYCHAR))) != 0):
                    self.state = 96
                    self.anyvalue()
                    self.state = 101
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                pass
            elif token in [SETParser.EOF]:
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AnyvalueContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LEFT_PAR(self):
            return self.getToken(SETParser.LEFT_PAR, 0)

        def RIGHT_PAR(self):
            return self.getToken(SETParser.RIGHT_PAR, 0)

        def LEFT_BR(self):
            return self.getToken(SETParser.LEFT_BR, 0)

        def RIGHT_BR(self):
            return self.getToken(SETParser.RIGHT_BR, 0)

        def COMMA(self):
            return self.getToken(SETParser.COMMA, 0)

        def ITER(self):
            return self.getToken(SETParser.ITER, 0)

        def POS_ITER(self):
            return self.getToken(SETParser.POS_ITER, 0)

        def CONCAT(self):
            return self.getToken(SETParser.CONCAT, 0)

        def UNION(self):
            return self.getToken(SETParser.UNION, 0)

        def INTERS(self):
            return self.getToken(SETParser.INTERS, 0)

        def COMPL(self):
            return self.getToken(SETParser.COMPL, 0)

        def POS_COMPL(self):
            return self.getToken(SETParser.POS_COMPL, 0)

        def ALPHABET(self):
            return self.getToken(SETParser.ALPHABET, 0)

        def EPSILON(self):
            return self.getToken(SETParser.EPSILON, 0)

        def EMPTYSET(self):
            return self.getToken(SETParser.EMPTYSET, 0)

        def QUOTE(self):
            return self.getToken(SETParser.QUOTE, 0)

        def SIGMA(self):
            return self.getToken(SETParser.SIGMA, 0)

        def SIGSEP(self):
            return self.getToken(SETParser.SIGSEP, 0)

        def EQUALS(self):
            return self.getToken(SETParser.EQUALS, 0)

        def DIFF(self):
            return self.getToken(SETParser.DIFF, 0)

        def HASH(self):
            return self.getToken(SETParser.HASH, 0)

        def ANYCHAR(self):
            return self.getToken(SETParser.ANYCHAR, 0)

        def getRuleIndex(self):
            return SETParser.RULE_anyvalue

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAnyvalue" ):
                listener.enterAnyvalue(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAnyvalue" ):
                listener.exitAnyvalue(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAnyvalue" ):
                return visitor.visitAnyvalue(self)
            else:
                return visitor.visitChildren(self)




    def anyvalue(self):

        localctx = SETParser.AnyvalueContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_anyvalue)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 105
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << SETParser.LEFT_PAR) | (1 << SETParser.RIGHT_PAR) | (1 << SETParser.LEFT_BR) | (1 << SETParser.RIGHT_BR) | (1 << SETParser.COMMA) | (1 << SETParser.ITER) | (1 << SETParser.POS_ITER) | (1 << SETParser.CONCAT) | (1 << SETParser.UNION) | (1 << SETParser.INTERS) | (1 << SETParser.COMPL) | (1 << SETParser.POS_COMPL) | (1 << SETParser.ALPHABET) | (1 << SETParser.EPSILON) | (1 << SETParser.EMPTYSET) | (1 << SETParser.SIGMA) | (1 << SETParser.SIGSEP) | (1 << SETParser.EQUALS) | (1 << SETParser.DIFF) | (1 << SETParser.HASH) | (1 << SETParser.QUOTE) | (1 << SETParser.ANYCHAR))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[2] = self.expr_sempred
        self._predicates[3] = self.unop_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def expr_sempred(self, localctx:ExprContext, predIndex:int):
            if predIndex == 0:
                return 5 >= localctx.pr
         

            if predIndex == 1:
                return 4 >= localctx.pr
         

            if predIndex == 2:
                return 3 >= localctx.pr
         

            if predIndex == 3:
                return 2 >= localctx.pr
         

    def unop_sempred(self, localctx:UnopContext, predIndex:int):
            if predIndex == 4:
                return self.precpred(self._ctx, 3)
         

            if predIndex == 5:
                return self.precpred(self._ctx, 2)
         

            if predIndex == 6:
                return self.precpred(self._ctx, 1)
         




