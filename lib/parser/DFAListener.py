# Generated from lib/parser/DFA.g4 by ANTLR 4.9.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .DFAParser import DFAParser
else:
    from DFAParser import DFAParser

# This class defines a complete listener for a parse tree produced by DFAParser.
class DFAListener(ParseTreeListener):

    # Enter a parse tree produced by DFAParser#production.
    def enterProduction(self, ctx:DFAParser.ProductionContext):
        pass

    # Exit a parse tree produced by DFAParser#production.
    def exitProduction(self, ctx:DFAParser.ProductionContext):
        pass


    # Enter a parse tree produced by DFAParser#start.
    def enterStart(self, ctx:DFAParser.StartContext):
        pass

    # Exit a parse tree produced by DFAParser#start.
    def exitStart(self, ctx:DFAParser.StartContext):
        pass


    # Enter a parse tree produced by DFAParser#init.
    def enterInit(self, ctx:DFAParser.InitContext):
        pass

    # Exit a parse tree produced by DFAParser#init.
    def exitInit(self, ctx:DFAParser.InitContext):
        pass


    # Enter a parse tree produced by DFAParser#stateset.
    def enterStateset(self, ctx:DFAParser.StatesetContext):
        pass

    # Exit a parse tree produced by DFAParser#stateset.
    def exitStateset(self, ctx:DFAParser.StatesetContext):
        pass


    # Enter a parse tree produced by DFAParser#final.
    def enterFinal(self, ctx:DFAParser.FinalContext):
        pass

    # Exit a parse tree produced by DFAParser#final.
    def exitFinal(self, ctx:DFAParser.FinalContext):
        pass


    # Enter a parse tree produced by DFAParser#statename.
    def enterStatename(self, ctx:DFAParser.StatenameContext):
        pass

    # Exit a parse tree produced by DFAParser#statename.
    def exitStatename(self, ctx:DFAParser.StatenameContext):
        pass


    # Enter a parse tree produced by DFAParser#comment.
    def enterComment(self, ctx:DFAParser.CommentContext):
        pass

    # Exit a parse tree produced by DFAParser#comment.
    def exitComment(self, ctx:DFAParser.CommentContext):
        pass


    # Enter a parse tree produced by DFAParser#anyvalue.
    def enterAnyvalue(self, ctx:DFAParser.AnyvalueContext):
        pass

    # Exit a parse tree produced by DFAParser#anyvalue.
    def exitAnyvalue(self, ctx:DFAParser.AnyvalueContext):
        pass



del DFAParser