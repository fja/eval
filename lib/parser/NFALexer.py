# Generated from lib/parser/NFA.g4 by ANTLR 4.9.2
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
    from typing import TextIO
else:
    from typing.io import TextIO



def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\20")
        buf.write("M\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\4\3\4\3")
        buf.write("\5\3\5\3\6\3\6\3\7\3\7\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t")
        buf.write("\3\n\3\n\3\n\5\n:\n\n\3\13\6\13=\n\13\r\13\16\13>\3\f")
        buf.write("\3\f\3\r\3\r\3\16\6\16F\n\16\r\16\16\16G\3\16\3\16\3\17")
        buf.write("\3\17\2\2\20\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13")
        buf.write("\25\f\27\r\31\16\33\17\35\20\3\2\4\n\2))//\62;>>@@C\\")
        buf.write("aac|\5\2\13\f\17\17\"\"\2O\2\3\3\2\2\2\2\5\3\2\2\2\2\7")
        buf.write("\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2")
        buf.write("\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2")
        buf.write("\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\3\37\3\2\2\2")
        buf.write("\5$\3\2\2\2\7&\3\2\2\2\t(\3\2\2\2\13*\3\2\2\2\r,\3\2\2")
        buf.write("\2\17.\3\2\2\2\21\60\3\2\2\2\239\3\2\2\2\25<\3\2\2\2\27")
        buf.write("@\3\2\2\2\31B\3\2\2\2\33E\3\2\2\2\35K\3\2\2\2\37 \7k\2")
        buf.write("\2 !\7p\2\2!\"\7k\2\2\"#\7v\2\2#\4\3\2\2\2$%\7?\2\2%\6")
        buf.write("\3\2\2\2&\'\7*\2\2\'\b\3\2\2\2()\7+\2\2)\n\3\2\2\2*+\7")
        buf.write("}\2\2+\f\3\2\2\2,-\7\177\2\2-\16\3\2\2\2./\7.\2\2/\20")
        buf.write("\3\2\2\2\60\61\7h\2\2\61\62\7k\2\2\62\63\7p\2\2\63\64")
        buf.write("\7c\2\2\64\65\7n\2\2\65\22\3\2\2\2\66:\7\u03b7\2\2\67")
        buf.write("8\7^\2\28:\7g\2\29\66\3\2\2\29\67\3\2\2\2:\24\3\2\2\2")
        buf.write(";=\t\2\2\2<;\3\2\2\2=>\3\2\2\2><\3\2\2\2>?\3\2\2\2?\26")
        buf.write("\3\2\2\2@A\7%\2\2A\30\3\2\2\2BC\7$\2\2C\32\3\2\2\2DF\t")
        buf.write("\3\2\2ED\3\2\2\2FG\3\2\2\2GE\3\2\2\2GH\3\2\2\2HI\3\2\2")
        buf.write("\2IJ\b\16\2\2J\34\3\2\2\2KL\13\2\2\2L\36\3\2\2\2\7\29")
        buf.write("<>G\3\b\2\2")
        return buf.getvalue()


class NFALexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    INIT = 1
    EQUALS = 2
    LEFT_PARENTHESIS = 3
    RIGHT_PARENTHESIS = 4
    LEFT_BRACKET = 5
    RIGHT_BRACKET = 6
    COMMA = 7
    FINAL = 8
    EPSILON = 9
    STATE = 10
    HASH = 11
    QUOTE = 12
    WS = 13
    ANYCHAR = 14

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'init'", "'='", "'('", "')'", "'{'", "'}'", "','", "'final'", 
            "'#'", "'\"'" ]

    symbolicNames = [ "<INVALID>",
            "INIT", "EQUALS", "LEFT_PARENTHESIS", "RIGHT_PARENTHESIS", "LEFT_BRACKET", 
            "RIGHT_BRACKET", "COMMA", "FINAL", "EPSILON", "STATE", "HASH", 
            "QUOTE", "WS", "ANYCHAR" ]

    ruleNames = [ "INIT", "EQUALS", "LEFT_PARENTHESIS", "RIGHT_PARENTHESIS", 
                  "LEFT_BRACKET", "RIGHT_BRACKET", "COMMA", "FINAL", "EPSILON", 
                  "STATE", "HASH", "QUOTE", "WS", "ANYCHAR" ]

    grammarFileName = "NFA.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


