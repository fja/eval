from typing import Tuple, Union
from lib import reg, parser
from lib.parser import ParsingError

# support functions common for both fja_checker and web_checker

def get_task(string: str) -> Tuple[str, str]:  # from IS task prefix
    assert '-' in string, "Teacher solution could not be parsed correctly."
    teacher_type, task = string.split("-", 1)
    if '-' in task:
        task, _ = task.split("-", 1)
    assert len(teacher_type) != 0, "Teacher solution code must not ne empty"
    assert len(task) != 0, "Task code must not be empty"

    return teacher_type, task


def dfa_transform(string: str, automaton_type: str) -> reg.DFA:
    if automaton_type in {"DFA", "TOT", "MIN", "TOC", "CAN", "MIC"}:
        return parser.dfa(string)
    else:
        return nfa_transform(string, automaton_type).determinize()


def nfa_transform(string: str, automaton_type: str) -> reg.NFA:
    try:
        if automaton_type in {"EFA", "NFA"}:
            automaton = parser.nfa(string)
        elif automaton_type == "GRA":
            automaton = parser.reggrammar(string).eliminate_useless().reggrammar_to_nfa()
        elif automaton_type == "REG":
            automaton = parser.regex(string).regex_to_efa().eliminate_epsilon()
        elif automaton_type in {"DFA", "TOT", "MIN", "TOC", "CAN", "MIC"}:
            automaton = parser.dfa(string).dfa_to_nfa()

        return reg.NFA(automaton.states, automaton.characters, automaton.transition, automaton.init, automaton.final)

    except ParsingError as ex:
        raise ParsingError(ex.args)


def transform(string: str, automaton_type: str) -> Union[reg.DFA, reg.NFA, reg.RegGrammar, reg.RegEx]:
    try:
        if automaton_type in {"DFA", "TOT", "MIN", "TOC", "CAN", "MIC"}:
            formalism = parser.dfa(string)
        elif automaton_type in {"EFA", "NFA"}:
            formalism = parser.nfa(string)
        elif automaton_type == "GRA":
            formalism = parser.reggrammar(string)
        elif automaton_type == "REG":
            formalism = parser.regex(string)

        return formalism

    except ParsingError as ex:
        raise ParsingError(ex.args)


def check_task(automaton: Union[reg.DFA, reg.NFA], task: str) -> str:
    output = ""

    if isinstance(automaton, reg.NFA):
        if task == "NFA" and automaton.has_epsilon():
            output = "NFA obsahuje ε-kroky."
        return output

    if task not in {"TOT", "MIN", "TOC", "CAN", "MIC"}:
        return output

    if task == "TOT" and not automaton.is_total():
        output = "DFA není totální."

    if task == "MIN" and not automaton.is_minimal():
        output = "DFA není minimální."

    if task in {"CAN", "TOC", "MIC"}:
        canonical = automaton.is_canonical()
        if task == "CAN" and not canonical:
            output = "DFA není kanonický."

        if task == "TOC" and (not automaton.is_total() or not canonical):
            if not automaton.is_total():
                output = "DFA není totální. "
            if not canonical:
                output += "DFA není kanonický."

        if task == "MIC" and (not automaton.is_minimal() or not canonical):
            if not automaton.is_minimal():
                output = "DFA není minimální. "
            if not canonical:
                output += "DFA není kanonický."

    return output


def check_empty(student_solution, teacher_solution, task_solved=""):
    if teacher_solution.is_empty() and student_solution.is_empty()\
            and task_solved == "":
        exit_correct()


def check_alphabets(student_alpha, teacher_alpha, task: str) -> str:
    output = ""
    if student_alpha != teacher_alpha:
        output = "Abecedy zadaného jazyka a studentova řešení se liší"

        if task == "REG":
            output += " (abeceda je odvozena ze všech písmen objevujících se v regulárním výrazu)."
        elif task == "GRA":
            output += " (abeceda je odvozena ze všech terminálů objevujících se v pravidlech gramatiky)."
        else:
            output += " (abeceda je odvozena ze všech písmen objevujících se v přechodové funkci)."

        # if wanna also print both alphabets:
        #print("Studentova abeceda:" + reg.Parser.names_to_str(student_alpha))
        #print("Abeceda zadaného jazyka:" + reg.Parser.names_to_str(teacher_alpha))

    return output

def exit_correct():
    print("Správné řešení.")
    exit(0)


def exit_incorrect():
    print("Nesprávné řešení.")
    exit(1)
