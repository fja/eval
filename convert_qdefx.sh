#!/usr/bin/env bash
[ $1 ] || {
    cat <<EOF | sed 's/^        //' >&2
        Converts qdefx question files from IS MU to the new format. Input files must be
        in the second-generation format (the type of external evaluator is f; used 2018
        and 2019).

        Files are modified in-place.

        usage: $0 QDEFX_FILES
EOF
    exit 1
}
for i in "$@"; do
    # first regex replaces definition inside the ebmedded old QDEF format,
    # which is not really used, but should be kept in sync
    # the second one prelaces the proper definition in the new format
    # the third one removes unnecessary "-N" from task prefix
    sed -i -e 's/^:\(e[^=]*\)="f:/:\1="d:?/' \
           -e 's|\(<check><type>external</type><service>\)f\(</service><correct><!\[CDATA\[\)|\1d\2?|' \
        $i

    sed -i -E 's/([A-Z]{3}-[A-Z]{3})-N/\1/g' $i
done
