from lib.common import Nonterminal, Terminal, Eps
import lib.parser
import lib.cfl as cfl
from copy import deepcopy

S, A, B, C = tuple(Nonterminal(x) for x in "SABC")
a, b, c = tuple(Terminal(x) for x in "abc")

Rules = cfl.CFG.Rules

rules0: Rules = dict()
g0 = cfl.CFG({A}, {a, b}, rules0, A)

rules1: Rules = {A: {(a,), (a, A), (b, B), (b,)},
                 B: {(b,), (b, B)}}
g1 = cfl.CFG({A, B}, {a, b}, rules1, A)

rules2: Rules = {A: {(a,), (a, A), (b, B)},
                 B: {(b, B), Eps()}}
g2 = cfl.CFG({A, B}, {a, b}, rules2, A)


def test_empty():
    assert g0.is_empty()


def test_basics():
    assert g1.init == A
    assert g1.terminals == {a, b}
    assert g1.nonterminals == {A, B}
    assert g1.rules == rules1
    collected_prods = set(g1.productions())
    assert collected_prods == {(src, dst) for src, prods in rules1.items()
                               for dst in prods}


def test_generates():
    def go(g):
        assert g.generates("a")
        assert g.generates("b")
        assert g.generates("aa")
        assert g.generates("ab")
        assert g.generates("abbb")
        assert g.generates("aabbb")
        assert not g.generates("")
        assert not g.generates("ba")

    go(g1)
    go(g2)


def test_remove_simple() -> None:
    g = cfl.CFG({S, A, B, C}, {a},
                {S: {(A,)},
                 A: {(B, C), (B,), (C,)},
                 B: {(a, B), (a,)},
                 C: {(a, a, C), (a, a)}},
                S)
    print(g)
    assert g.has_simple_rules()
    gs = g.remove_simple_rules()
    print(gs)
    assert not gs.has_simple_rules()
    assert (B, C) in gs.rules[gs.init]
    assert (a, B) in gs.rules[gs.init]
    assert (a,) in gs.rules[gs.init]
    assert (a, a, C) in gs.rules[gs.init]
    assert (a, a) in gs.rules[gs.init]


def test_remove_simple_2() -> None:
    g = cfl.CFG({S, A, B}, {a},
                {S: {(a, A)},
                 A: {(B,), (a, a)},
                 B: {(a,)}},
                S)
    print(g)
    assert g.has_simple_rules()
    gs = g.remove_simple_rules()
    print(gs)
    assert not gs.has_simple_rules()
    assert g.generates("aa")
    assert g.generates("aaa")


def test_is_epsilon_normal():
    assert g0.is_epsilon_normal_form()
    assert g1.is_epsilon_normal_form()
    assert not g2.is_epsilon_normal_form()
    assert not cfl.CFG({A, B}, {a}, {A: {(a, B), Eps()},
                                     B: {(a, A)}}, A).is_epsilon_normal_form()


def test_remove_eps():
    g = g2.epsilon_normal_form()
    assert g.init == g1.init
    assert g.terminals == g1.terminals
    assert g.nonterminals == g1.nonterminals
    assert g.rules == g1.rules

    g = cfl.CFG({S, A, B}, {a, b},
                {S: {(A, B)},
                 A: {(a, A), Eps()},
                 B: {(b, B), Eps()}},
                S)
    ge = g.epsilon_normal_form()
    print(ge.proper())
    assert ge.init != S
    assert Eps() in ge.rules[ge.init]
    assert ge.generates("")
    print(ge.generates("aa").cnf_cfg)
    assert ge.generates("aa")
    assert ge.generates("bb")
    assert ge.generates("ab")
    assert not ge.generates("aba")
    assert not ge.generates("ba")


def test_equal():
    R = Nonterminal("R")
    P = Nonterminal("P")
    u, d, p, v, z = tuple(Terminal(x) for x in "udpvz")
    rules = {S: {(u, R, S), Eps()},
             R: {(p, P), (d,), (u, R, R), Eps()},
             P: {(z,), (v, R)}}
    g = cfl.CFG({S, R, P}, {u, d, p, v, z}, rules, S)
    assert g.generates("")
    assert g.generates("uu")
    assert g.generates("uudu")
    assert g.generates("uudpvd")
    assert g.generates("uduududd")
    assert g.generates("uupvpzudd")
    assert not g.generates("d")
    assert not g.generates("up")
    assert not g.generates("duu")
    assert not g.generates("uv")
    assert not g.generates("udz")
    assert not g.generates("upuv")

    assert cfl.CFG.is_equivalent_test(g, g, max_cmp_len=8)

    rules_e0 = {S: {(u, R), Eps()},
                R: {(p, P), (d,), (u, R, R), Eps()},
                P: {(z,), (v, R)}}
    ge0 = cfl.CFG({S, R, P}, {u, d, p, v, z}, rules_e0, S)
    r = cfl.CFG.is_equivalent_test(g, ge0, max_cmp_len=8)
    assert not r
    print(r.__dict__)


def test_parser():
    pg0 = lib.parser.cfg("S -> aA | ε | aBC | <foo>C; "
                         "A -> a; "
                         "B -> b; "
                         "C -> c\n"
                         "<foo> -> aaa")
    assert pg0.terminals == {a, b, c}
    assert pg0.nonterminals == {S, A, B, C, Nonterminal("<foo>")}
    assert pg0.generates("")
    assert pg0.generates("aa")
    assert pg0.generates("abc")
    assert pg0.generates("aaac")
    assert pg0.is_epsilon_normal_form()

