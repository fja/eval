import lib.reg as reg
import lib.cfl as cfl
from lib import parser
from lib.parser import ParsingError
from lib.checker import get_task, dfa_transform, nfa_transform, check_task, check_empty, check_alphabets, exit_correct, exit_incorrect
import sys
import signal
import traceback
from typing import Any


render_html: bool = False


def wrap(text: str, tag: str, **options: str) -> str:
    if render_html:
        front = f"<{tag}"
        front += " " if options else ""
        front += " ".join(f'{k}="{v}"' for k, v in options.items()) + ">"
        return f"{front}{text}</{tag}>"
    return text


def paragraph(*args: Any) -> str:
    return wrap(' '.join(str(a) for a in args), "p", style="margin-bottom: 0px")


def code(val: str) -> str:
    return wrap(val, "code")


def print_extra_word_ce(student_word):
    print(paragraph("Příklad slova, které je ve studentově řešení a není v zadaném jazyce:", code(student_word)))


def print_missing_word_ce(teacher_word):
    print(paragraph("Příklad slova, které chybí ve studentově řešení a je v zadaném jazyce:", code(teacher_word)))


def dfa_task(teacher_type, teacher_string, task, student_string):
    try:
        student_solution = dfa_transform(student_string, task)
        teacher_solution = dfa_transform(teacher_string, teacher_type)
    except ParsingError as ex:
        print(paragraph("Nastala chyba při parsování:", ex))
        traceback.print_exc(file=sys.stderr)
        exit_incorrect()

    task_solved = ""
    if isinstance(student_solution, reg.DFA) or isinstance(student_solution, reg.NFA):
            task_solved = check_task(student_solution, task)

    check_empty(student_solution=student_solution,
                teacher_solution=teacher_solution,
                task_solved=task_solved)

    alphabets = check_alphabets(student_alpha=student_solution.characters,
                                teacher_alpha=teacher_solution.characters, task=task)
    if alphabets != "":
        print(paragraph(alphabets))
        exit_incorrect()

    result = reg.DFA.is_equivalent(student_solution, teacher_solution)

    if task_solved == "" and result:
        exit_correct()

    print(task_solved)

    if not result:
        student_word, teacher_word = result.left_counterexample, result.right_counterexample

        if student_word is None:
            print(paragraph("Studentovo řešení je podmnožinou zadaného jazyka."))
        else:
            print_extra_word_ce(student_word)

        if teacher_word is None:
            print(paragraph("Studentovo řešení je nadmnožinou zadaného jazyka."))
        else:
            print_missing_word_ce(teacher_word)

        if result.inf is not None:
            print(paragraph("Rozdíl porovnávaných jazyků je nekonečný."))
        else:
            print(paragraph("Rozdíl porovnávaných jazyků je konečný."))

    exit_incorrect()


def exit_cfl_ok_but_invalid_constraint(msg : str) -> None:
    print(paragraph("Gramatika generuje zadaný jazyk, ale nesplňuje podmínky"
                    f"zadání: {msg}"))
    exit(1)


def cfg_task(teacher_type: str, teacher_string: str, task: str,
             student_string: str) -> None:
    assert task[:3] == "CFG", f"Invalid task prefix {task[:3]}"
    if len(task) > 3:
        assert task[3] == '+', f"Invalid task suffix {task[3:]}"
    constraints = filter(lambda x: len(x) != 0, task[4:].split(","))
    try:
        teacher_solution = parser.cfg(teacher_string)
    except ParsingError as message:
        print(paragraph(f"Error parsing teacher's solution: {message}"))
        exit(1)

    try:
        student_solution = parser.cfg(student_string)
    except ParsingError as message:
        print(paragraph(f"Error parsing student's solution: {message}"))
        exit(1)

    check_empty(student_solution=student_solution,
                teacher_solution=teacher_solution)

    alphabets = check_alphabets(student_alpha=student_solution.terminals,
                                teacher_alpha=teacher_solution.terminals, task="GRA")
    if alphabets != "":
        print(alphabets)
        exit_incorrect()

    equals = cfl.CFG.is_equivalent_test(student_solution, teacher_solution)

    if equals:
        failed = []
        for constraint in constraints:
            if constraint == "¬ε":
                if not student_solution.is_epsilon_normal_form():
                    failed.append("obsahuje nepovolené ε-kroky")
            elif constraint == "¬s":
                if student_solution.has_simple_rules():
                    failed.append("obsahuje nepovolená jednoduchá pravidla")
            elif constraint == "CNF":
                if not student_solution.is_cnf():
                    failed.append("není v CNF")
            else:
                assert False, f"Unknown constraint `{constraint}'"

        if len(failed) == 0:
            exit_correct()
        exit_cfl_ok_but_invalid_constraint(", ".join(failed))

    if equals.left_counterexample is not None:
        print_extra_word_ce(equals.left_counterexample)
    if equals.right_counterexample:
        print_missing_word_ce(equals.right_counterexample)

    exit_incorrect()


def main():
    signal.alarm(50)

    global render_html
    if sys.argv[1] == "--html":
        render_html = True

    # Get string with student's solution.
    with open(sys.argv[1 + render_html]) as student_file:
        student_string = student_file.read()

    # Get string with teacher's solution and type of the task.
    for argument in sys.argv:
        if argument[:2] == "-o":
            teacher_string = argument[2:]

    try:
        task_prefix, teacher_string = teacher_string.split(":", 1)
        teacher_type, task = get_task(task_prefix)

        # When time comes, teacher's solution can surely be also GRA or REG, see web_checker.
        if teacher_type == "DFA" or teacher_type == "NFA":
            dfa_task(teacher_type=teacher_type, teacher_string=teacher_string,
                     task=task, student_string=student_string)
        elif teacher_type == "CFG":
            cfg_task(teacher_type=teacher_type, teacher_string=teacher_string,
                     task=task, student_string=student_string)
        else:
            print(paragraph(f"Invalid question type {task_prefix}"))
            exit(1)

    except Exception as ex:
        print(paragraph("Error inside of fja_checker:", ex.args))
        traceback.print_exc(file=sys.stderr)
        exit(1)

if __name__ == "__main__":
    main()
