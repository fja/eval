PWD != pwd
PY = $(wildcard *.py)
GRMS=DFA NFA CFG SET RegG
ANTLR_VERSION != antlr4 | grep Version | sed -E 's/.*Version ([0-9]+[.][0-9]+.[0-9]+).*/\1/'

-include local.make

MYPY ?= mypy

all : test

typecheck : $(PY:%=%.mypy)

unit:
	PYTHONPATH=$$PWD pytest --color=yes

test: typecheck unit

%.mypy : %
	$(MYPY) --check-untyped-defs --warn-redundant-casts --warn-unused-ignores --warn-return-any $<

grammars : $(GRMS:%=lib/parser/%Parser.py)

$(GRMS:%=lib/parser/%Parser.py) : lib/parser/%Parser.py : lib/parser/%.g4
	antlr4 -Dlanguage=Python3 -visitor $<
	@# let's be a bit more permissive for versions
	sed -i '/self.checkVersion(".*")/d' $(@:%Parser.py=%*.py)

$(GRMS:%=build/js/%Parser.js) : build/js/%Parser.js : lib/parser/%.g4
	mkdir -p $(dir $@)
	@# note: the path must be absolute, otherwise antlr4 put files into lib/parser subdir
	antlr4 -Dlanguage=JavaScript -o $(dir $@) $(PWD)/$<

runtime-js : build/js/runtime/antlr4/Parser.js

build/js/runtime/antlr4/Parser.js : build/js/antlr-javascript-runtime-$(ANTLR_VERSION).zip
	(cd $(dir $@).. && unzip -o ../antlr-javascript-runtime-$(ANTLR_VERSION).zip)
	touch $@

FA_PARSER=DFA NFA
$(FA_PARSER:%=lib/parser/%Parser.py) : lib/parser/FA.g4
$(FA_PARSER:%=build/js/%Parser.js) : lib/parser/FA.g4

lib/parser/RegGParser.py : lib/parser/CFG.g4
build/js/RegGParser.js : lib/parser/CFG.g4

.PHONY: %.mypy typecheck test unit all grammars
